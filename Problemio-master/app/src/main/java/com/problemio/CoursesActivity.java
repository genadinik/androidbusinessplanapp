package com.problemio;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 12/18/15.
 */
public class CoursesActivity extends BaseActivity
{
    /**
     * Called when this activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.courses);

//        Button niche = (Button)findViewById(R.id.niche);
//        niche.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.udemy.com/how-to-find-your-business-niche/?couponCode=androidapp"));
//
//                startActivity(browserIntent);
//            }
//        });

        Button startbusiness = (Button)findViewById(R.id.startbusiness);
        startbusiness.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/course/how-to-start-a-business-go-from-business-idea-to-a-business/?referralCode=39682B31DCA89BD13A15"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });

//        Button appbook = (Button)findViewById(R.id.appbook);
//        appbook.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.udemy.com/how-to-create-grow-a-mobile-app-iphone-android-business/?couponCode=androidapp"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });

//        Button twitter = (Button)findViewById(R.id.twitter);
//        twitter.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.udemy.com/twitter-marketing-course/?couponCode=androidapp"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });


        Button strategiesbook = (Button)findViewById(R.id.strategiesbook);
        strategiesbook.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/course/marketing-plan-strategy-become-a-great-marketer/?referralCode=12AAD72F0DAA6471084D"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });


        Button businessplan = (Button)findViewById(R.id.businessplan);
        businessplan.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/course/how-to-write-a-business-plan/?referralCode=D66A2AFD0CC9CFC4278F"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });

        Button marketingplan = (Button)findViewById(R.id.marketingplan);
        marketingplan.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/course/how-to-write-a-marketing-plan/?referralCode=3B8ED79B3D272AB9060B"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", CoursesActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });



//
//        Button marketingplan = (Button)findViewById(R.id.marketingplan);
//        marketingplan.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.udemy.com/how-to-write-a-marketing-plan/?couponCode=androidapp"));
//
//                startActivity(browserIntent);
//            }
//        });
//


//        Button get_app = (Button)findViewById(R.id.get_app);
//        get_app.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse("market://details?id=com.okason.udemycoupons"));
//
//                try
//                {
//                    startActivity(intent);
//                }
//                catch (ActivityNotFoundException anfe)
//                {
//                    try
//                    {
//                        Uri uri = Uri.parse("market://search?q=pname:com.okason.udemycoupons");
//                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(next_intent);
//                    }
//                    catch ( Exception e)
//                    {
//                        // Now try to redirect them to the web version:
//                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.okason.udemycoupons");
//                        try
//                        {
//                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                            startActivity(webintent);
//                        }
//                        catch ( Exception except )
//                        {
//
//                        }
//                    }
//                }
//            }
//        });
    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
        // your code
    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
