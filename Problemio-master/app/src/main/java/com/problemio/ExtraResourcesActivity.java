package com.problemio;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import utils.SendEmail;



public class ExtraResourcesActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.content_extraresources);
	    
	    

	    
//	    Button give_review = (Button)findViewById(R.id.give_review);   
//	    give_review.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.marketing"));
//            	//startActivity(intent);
//            	            	 
//            	 try 
//            	 {
//            	        startActivity(intent);
//            	 } 
//            	 catch (ActivityNotFoundException anfe) 
//            	 {            		
//                     try
//                     {
//                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketing");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);  
//                     }
//                     catch ( Exception e)
//                     {
//                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//
//                         }
//                     }
//            	 }           	            	
//            }
//        });
//
//	      Button premium_marketing = (Button)findViewById(R.id.premium_marketing);
//	      premium_marketing.setOnClickListener(new Button.OnClickListener() 
//	      {  
//	          public void onClick(View v) 
//	          {		
//	          	sendEmail("Free marketing advertising -> premium marketin","");
//	        	  
//	            Intent intent = new Intent(Intent.ACTION_VIEW);
//	        	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//	        	            	 
//	        	 try 
//	        	 {
//	        	        startActivity(intent);
//	        	 } 
//	        	 catch (ActivityNotFoundException anfe) 
//	        	 {
//	                 try
//	                 {
//	                	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	                	 startActivity(next_intent);  
//	                 }
//	                 catch ( Exception e)
//	                 {
//	                     // Now try to redirect them to the web version:
//	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//	                     try
//	                     {
//	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                    	 startActivity(webintent);
//	                     }
//	                     catch ( Exception except )
//	                     {
//
//	                     }
//	                 }
//	        	 }           	            	
//	        }
//	      });	 	     	    

        
        Button prweb = (Button)findViewById(R.id.prweb);          
        prweb.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
            		  Uri.parse("http://www.anrdoezrs.net/click-7252177-10820226"));
              
              startActivity(browserIntent);
            }
        });

        
        Button bluehost = (Button)findViewById(R.id.bluehost);          
        bluehost.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
            		  Uri.parse("http://www.bluehost.com/track/genadinik"));
              
              startActivity(browserIntent);
            }
        });        
        
//        Button websitemag = (Button)findViewById(R.id.websitemag);          
//        websitemag.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {        	
//              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//            		  Uri.parse("http://www.dpbolvw.net/click-7252177-10967366"));
//              
//              startActivity(browserIntent);
//            }
//        });        

         
//        Button discussions_button = (Button)findViewById(R.id.community); 
//        discussions_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//          public void onClick(View v) 
//          {            	
//        	  // DailyDiscussionsActivity.class  CommunityQuestionsActivity
//            Intent myIntent = new Intent(ExtraResourcesActivity.this, CommunityActivity.class);
//            ExtraResourcesActivity.this.startActivity(myIntent);
//          }
//        }); 



        Button jobs = (Button)findViewById(R.id.jobs);
        jobs.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                Intent myIntent = new Intent(ExtraResourcesActivity.this, CoursesActivity.class);
                ExtraResourcesActivity.this.startActivity(myIntent);
            }
        });      
        
        Button ipoll = (Button)findViewById(R.id.ipoll);          
        ipoll.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
                Intent myIntent = new Intent(ExtraResourcesActivity.this, BookActivity.class);
                ExtraResourcesActivity.this.startActivity(myIntent);
            }
        });
        

        Button marketing = (Button)findViewById(R.id.marketing);
        marketing.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.marketing"));
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketing");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           	            	
            }
        });   		                   
        
        Button ideas = (Button)findViewById(R.id.ideas);
        ideas.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=business.ideas"));
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:business.ideas");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.ideas");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           	            	
            }
        });   		           
        
        Button money_app = (Button)findViewById(R.id.money);
        money_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=make.money"));
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:make.money");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=make.money");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           	            	
            }
        });   		                   
	        		
	}
	
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
