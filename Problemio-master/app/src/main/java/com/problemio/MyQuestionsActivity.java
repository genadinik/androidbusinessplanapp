package com.problemio;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.problemio.data.Question;
import com.problemio.inappbilling.IabBroadcastReceiver;
import com.problemio.inappbilling.IabHelper;
import com.problemio.inappbilling.IabResult;
import com.problemio.inappbilling.Inventory;
import com.problemio.inappbilling.Purchase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import utils.SendEmail;

public class MyQuestionsActivity extends BaseListActivity implements IabBroadcastReceiver.IabBroadcastListener{


    // Will the subscription auto-renew?
    boolean mAutoRenewEnabled = false;

    // The in app billing helper object
    IabHelper mHelper;

    private boolean IS_SUBSCRIBED_USER = false;

    //SKU for monthly and yearly Question subscription
    static final String SKU_PREMIUM_QUESTION_MONTHLY = "99_subscription_2015";
    static final String SKU_PREMIUM_QUESTION_YEARLY = "premium_question_yearly";


    //Request code for the purchase flow
    static final int RC_REQUEST = 10001;

    //the app private license key, will need to be changed
    private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAihihw" +
            "2Q6tdOpWUmk84zuFHZDnTduxoX25rHsRGFTcZ5BB13ZFwdPISefHMAeDyfuJdtHwkieniA4WL+AkhohvSDNpIInqWEmvU9Lte" +
            "jwy4hVrv9hVKAv1wPuA+DUyZeujIgCl7gqCHULf4fCwDaP9nQ62rdRqV9+rmw+1tFOpyWOe7Vi6m2jmJhFk4QxuVKG0IOLg8+2EI" +
            "go0Quisrc8WTwtQ7Ev2FV2UPMZp8pPMxjmCck1jmv7uPcnphTLFLE386dejOLJc0hbv6gj6OnQgzjfdX4ldNF3aMgpKOF8Y5/YHTKy2d" +
            "zSt0ZGsSqPmCTJF2yzZoLAi9BKYxCU2/BOBwIDAQAB";


    //tag for debugging
    static final String TAG = "In_App_Billing";

    // Provides purchase notification while this app is running
    IabBroadcastReceiver mBroadcastReceiver;
    Activity mActivity;




    ArrayAdapter<Question> adapter;
    ArrayList<Question> questions = new ArrayList<Question>();


    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        Log.d(TAG, "Creating IAB Helper");
        //initialize in app billing
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mActivity = this;

        //enable debugging logging, should be removed after development
        mHelper.enableDebugLogging(true);

        setContentView(R.layout.users_questions);

        //sendEmail("ProblemioMyQuestions","PageLoaded");

        // Make sure the user is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyQuestionsActivity.this);
        final String user_id = prefs.getString("user_id", null);
        final String email = prefs.getString("email", null);
        final String first_name = prefs.getString("first_name", null);

        // If the user is not logged in, send them to log in
        if (user_id == null) {
            Toast.makeText(getApplicationContext(), "Not logged in.  Please login or create an account.", Toast.LENGTH_LONG).show();

            Intent loginIntent = new Intent(MyQuestionsActivity.this, LoginActivity.class);
            MyQuestionsActivity.this.startActivity(loginIntent);
        }


//        Question q = new Question ();
//        q.setQuestion( "" );
//        
//        questions.add(q);	    

        //adapter = new ArrayAdapter<Question>(this, R.layout.user_question_list,
        //		R.id.label,  questions);

        adapter = new ArrayAdapter<Question>(this, R.layout.user_question_list, questions);

        setListAdapter(adapter);

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);

        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
                        Toast.LENGTH_SHORT).show();

                String question_name = questions.get(position).getQuestion() + "";
                String question_id = questions.get(position).getQuestionId() + "";

                // Now put this stuff into the SharedPreferences
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(
                        MyQuestionsActivity.this);

                prefs.edit().putString("recent_question_id", question_id).commit();
                prefs.edit().putString("recent_question", question_name).commit();

                // And go to problem intent
                Intent myIntent = new Intent(MyQuestionsActivity.this, QuestionActivity.class);
                MyQuestionsActivity.this.startActivity(myIntent);
            }
        });

        Button add_question = (Button) findViewById(R.id.add_question);
        add_question.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo info = cm.getActiveNetworkInfo();

                if (info != null && info.isConnected()) {
                    Intent questionIntent = new Intent(mActivity, AskQuestionActivity.class);
                    Intent subscribeIntent = new Intent(mActivity, SubscriptionActivity.class);
                    if (IS_SUBSCRIBED_USER) {
                        startActivity(questionIntent);
                    } else {
                        processSubscription();
                    }
                } else {
                    Toast.makeText(mActivity, "Internet connection needed to ask question.", Toast.LENGTH_SHORT);
                }


            }
        });


//	    Button ask_about = (Button)findViewById(R.id.ask_about);
//        ask_about.setOnClickListener(new Button.OnClickListener()
//	    {
//	 	   public void onClick(View v)
//	 	   {
//              Intent myIntent = new Intent(MyQuestionsActivity.this, AboutActivity.class);
//              MyQuestionsActivity.this.startActivity(myIntent);
//	 	   }
//	    });

        Button howworks = (Button)findViewById(R.id.howworks);
        final AlertDialog.Builder builderbutton = new AlertDialog.Builder(this);
        howworks.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {

                builderbutton.setMessage("How this works: " +
                        "\n\n1) Sign-up (no risk! If you don't like it, ask for a refund within the first 7 days)" +
                        "\n\n2) Ask any business or marketing questions you have" +
                        "\n\n3) Get an answer from an expert within 24 hours")
                        .setCancelable(false)

                        .setNegativeButton("OK", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builderbutton.create();
                alert.show();
            }
        });

//        Button howworks = (Button)findViewById(R.id.howworks);
//        howworks.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v) {
//                builder.setMessage("How this works:" +
//                        "\n1) Sign-up (no risk! If you don't like it, ask for a refund)" +
//                        "\n2) Ask questions about your business plan or execution of your business strategy" +
//                        "\n3) Get answers to all your business questions" +
//                        "\n4) Take your business from idea all the way to success with the rigth guidance")
//                        .setCancelable(false)
//                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//
//                            });
//
//                                AlertDialog alert = builder.create();
//                                alert.show();
//
//                        });




        // Now make a call to server to get the questions for user.

        if (user_id == null) {
            //sendEmail ( "MyQuestionsError NULL ID" ,  "User id is null.  here is the email: " + email + " and name: + first_name.  Lets see what happens next....migth crash :)");
        } else {
            sendFeedback(user_id);
        }

        //setup in app purchase
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    //there was a problem
                    complain("Problem setting up in -app billing: " + result);
                    return;
                }

                // If the in app purchase process has been disposed,  quit.
                if (mHelper == null) return;

                mBroadcastReceiver = new IabBroadcastReceiver(MyQuestionsActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);

                //at this point In app purchase billing has been setup,
                //get inventory of the digital products
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    public void sendFeedback(String user_id) {
        String[] params = new String[]
                {"https://www.problemio.com/problems/get_questions_by_user_mobile.php",
                        user_id};

        DownloadWebPageTask task = new DownloadWebPageTask();

        task.execute(params);
    }



    public class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        private boolean connectionError = false;

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(MyQuestionsActivity.this);

            dialog.setContentView(R.layout.please_wait);
            dialog.setTitle("Loading Your Questions");

            TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
            text.setText("Please wait while your questions load...");
            dialog.show();
        }


        @Override
        protected String doInBackground(String... theParams) {
            String myUrl = theParams[0];
            final String user_id = theParams[1];

            String charset = "UTF-8";
            String response = null;

            try {
                String query = String.format("user_id=%s",
                        URLEncoder.encode(user_id, charset));

                final URL url = new URL(myUrl + "?" + query);

                final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setDoOutput(true);
                conn.setRequestMethod("GET");

                conn.setUseCaches(false);

                conn.connect();

                final InputStream is = conn.getInputStream();
                final byte[] buffer = new byte[8196];
                int readCount;
                final StringBuilder builder = new StringBuilder();
                while ((readCount = is.read(buffer)) > -1) {
                    builder.append(new String(buffer, 0, readCount));
                }

                response = builder.toString();
            } catch (Exception e) {
                connectionError = true;
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                dialog.dismiss();
            } catch (Exception ee)
            {
                // nothing
            }

            if (connectionError == true) {
                Toast.makeText(getApplicationContext(), "Please try again. Possible Internet connection error.", Toast.LENGTH_LONG).show();
            }

            if (result != null && result.trim().equals("no_user_id")) {
                Toast.makeText(getApplicationContext(), "Could not get your questions. We are working to fix this. ", Toast.LENGTH_LONG).show();

                //sendEmail("Error in My Questions", "The questions by user did not load because on the server the validation for user id said the user id was empty." );
            } else if (result != null && result.trim().equals("database_error")) {
                Toast.makeText(getApplicationContext(), "Could not get your questions. We are working to fix this. ", Toast.LENGTH_LONG).show();

                //sendEmail("Error in My Questions", "The questions by user did not load because on the server there " +
                //	"was a database error." );
            } else if (result != null && result.trim().equals("no_questions_by_user")) {
                //Toast.makeText(getApplicationContext(), "You have not asked any questions.  Ask your business questions! ", Toast.LENGTH_LONG).show();

                //sendEmail("No Questions so far in My Questions", "User got message that they do not have questions." );

                TextView question_label = (TextView) findViewById(R.id.question_label);
                question_label.setText("You have not asked any business questions.  Ask a business question and an experienced entrepreneur will get back to you.");
            } else {
                try {
                    JSONArray obj = new JSONArray(result);

                    {
                        questions.clear();

                        for (int i = 0; i < obj.length(); i++) {
                            JSONObject o = obj.getJSONObject(i);

                            String question_id = o.getString("question_id");
                            String question = o.getString("question");
                            String questioner_id = o.getString("member_id");
                            String first_name = o.getString("first_name");
                            String last_name = o.getString("last_name");

                            Question q = new Question();
                            q.setQuestion(question);
                            q.setQuestionId(question_id);
                            q.setQuestionByMemberId(questioner_id);

                            q.setAuthorName(first_name + " " + last_name);

                            questions.add(q);
                        }
                    }

                    adapter.notifyDataSetChanged();

                    TextView question_label = (TextView) findViewById(R.id.question_label);
                    question_label.setText("The questions you asked are below. Choose one or ask a new question.");
                } catch (Exception e) {
                    //sendEmail ("Exeption in getting qestions" , "Exception: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            //adapter.notifyDataSetChanged();
        }
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM_QUESTION_MONTHLY);
            IS_SUBSCRIBED_USER = (premiumPurchase != null);

            String userStatus = IS_SUBSCRIBED_USER ? "Subscribed User" : "Un subscribed User";
            mAutoRenewEnabled = premiumPurchase != null && premiumPurchase.isAutoRenewing();
            //Log.d(TAG, "User is " + (IS_SUBSCRIBED_USER ? "PREMIUM" : "NOT PREMIUM"));

            setWaitScreen(false);
            //Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    void complain(String message) {
        //Log.e(TAG, "**** In app Purchase Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    // Enables or disables the "please wait" screen.
    void setWaitScreen(boolean set) {
        findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
        findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    }

    // During destroy event. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver);
        }

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }


    private void processSubscription()
    {
        if (!mHelper.subscriptionsSupported()) {
            complain("Subscription not supported on your device yet. Sorry!");
            startActivity(new Intent(this, MyQuestionsActivity.class));
        }

        int titleResId;
        if (!IS_SUBSCRIBED_USER) {
            titleResId = R.string.subscription_period_prompt;
        } else if (!mAutoRenewEnabled) {
            titleResId = R.string.subscription_resignup_prompt;
        } else {
            titleResId = R.string.subscription_update_prompt;
        }

        List<String> oldSkus = null;
        if (IS_SUBSCRIBED_USER) {
            // The user currently has a valid subscription, any purchase action is going to
            // replace that subscription
            oldSkus = new ArrayList<String>();
            oldSkus.add(SKU_PREMIUM_QUESTION_MONTHLY);
        }
        final String payload = getDeviceId();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final List<String> finalOldSkus = oldSkus;
        builder.setTitle(titleResId)
                .setMessage("Upgrade to premium subscription to get continuous expert help to guide you as you work on your business")
                .setPositiveButton(R.string.subscription_prompt_continue, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHelper.launchPurchaseFlow(MyQuestionsActivity.this, SKU_PREMIUM_QUESTION_MONTHLY, IabHelper.ITEM_TYPE_SUBS,
                                finalOldSkus, RC_REQUEST, mPurchaseFinishedListener, payload);
                    }
                })
                .setNegativeButton(R.string.subscription_prompt_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void receivedBroadcast() {
        // Received a broadcast notification that the inventory of items has changed
        Log.d(TAG, "Received broadcast notification. Querying inventory.");
        mHelper.queryInventoryAsync(mGotInventoryListener);
    }


    public static String getDeviceId() {

            return android.os.Build.SERIAL;

    }


    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) {
                startActivity(new Intent(getApplicationContext(), MyQuestionsActivity.class));
            }

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                setWaitScreen(false);
                startActivity(new Intent(getApplicationContext(), MyQuestionsActivity.class));
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                setWaitScreen(false);
                startActivity(new Intent(MyQuestionsActivity.this, MyQuestionsActivity.class));
            }

            Log.d(TAG, "Purchase successful.");

            try {
                if (purchase.getSku().equals(SKU_PREMIUM_QUESTION_MONTHLY)
                        || purchase.getSku().equals(SKU_PREMIUM_QUESTION_YEARLY)) {
                    // bought the question subscription
                    alert("Thank you for subscribing to Premium subscription!");
                    IS_SUBSCRIBED_USER = true;
                    String userStatus = IS_SUBSCRIBED_USER ? "Subscribed User" : "Un subscribed User";
                    mAutoRenewEnabled = purchase.isAutoRenewing();
                    setWaitScreen(false);
                    startActivity(new Intent(MyQuestionsActivity.this, AskQuestionActivity.class));
                }
            } catch (Exception e) {
                IS_SUBSCRIBED_USER = false;
                startActivity(new Intent(MyQuestionsActivity.this, MyQuestionsActivity.class));
                Log.d(TAG, "Error with purchase." + e.getMessage() + " " + e.getCause());
            }
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mHelper == null) startActivity(new Intent(getApplicationContext(), MyQuestionsActivity.class));

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {

            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
           // super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean verifyDeveloperPayload(Purchase purchase) {
        if (purchase != null) {
            String payload = purchase.getDeveloperPayload();
            return payload.equals(getDeviceId());
        } else {
            return false;
        }
    }

//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
