package com.problemio.data;

public class ContestEntry 
{
	public String personName;
	public String businessName;
	public String businessDesc;
	public String contestVotes;
	public String entryId;
	public String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessDesc() {
		return businessDesc;
	}
	public void setBusinessDesc(String businessDesc) {
		this.businessDesc = businessDesc;
	}
	public String getContestVotes() {
		return contestVotes;
	}
	public void setContestVotes(String contestVotes) {
		this.contestVotes = contestVotes;
	}
	public String getEntryId() {
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}	
	
	public String toString ( )
	{
		if ( contestVotes == null || contestVotes.equals("") || contestVotes.equals("null") )
		{
			return "Business: " + businessName + " with 0 votes.\nUrl:  " + url + "\nDescription: " + businessDesc;			
		}
		else
		{
			return "Business: " + businessName + " with " + contestVotes + " votes.";
		}
	}
	
}
