package com.problemio.data;

public class SolutionTopic 
{
	public String solutionTopicId;
	public String solutionTopicName;
	public String commentCount;
	public String displayOrder;
	
	public String getSolutionTopicId() {
		return solutionTopicId;
	}
	public void setSolutionTopicId(String solutionTopicId) {
		this.solutionTopicId = solutionTopicId;
	}
	public String getSolutionTopicName() {
		return solutionTopicName;
	}
	public void setSolutionTopicName(String solutionTopicName) {
		this.solutionTopicName = solutionTopicName;
	}

	public String getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(String commentCount) {
		this.commentCount = commentCount;
	}
	public String getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}	
	
	@Override
	public String toString()
	{
		if ( commentCount == null || commentCount.trim().length() == 0 )
		{
			return commentCount;
		}
		else
		{
			String coloredString =  solutionTopicName + " (" + commentCount +  ")";
	        //Spannable sb = new SpannableString( coloredString );
	        //sb.setSpan(new ForegroundColorSpan(Color.BLUE), coloredString.indexOf("("), coloredString.indexOf(")")+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

	        return coloredString;			
			

	        
	        
	         //mTextView.setText(sb);	        
	        

			
		}
	}
}
