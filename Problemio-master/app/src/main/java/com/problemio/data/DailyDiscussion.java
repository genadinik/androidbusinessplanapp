package com.problemio.data;

import java.util.ArrayList;


public class DailyDiscussion 
{
	public String discussionId;
	public String discussion;
	public ArrayList <DailyDiscussionComment>  discussionComments;
	public String authorName;
	public String authorId;
	public String date;
	
	
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDiscussionId() {
		return discussionId;
	}
	public void setDiscussionId(String discussionId) {
		this.discussionId = discussionId;
	}
	public String getDiscussion() {
		return discussion;
	}
	public void setDiscussion(String discussion) {
		this.discussion = discussion;
	}

	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getAuthorId() {
		return authorId;
	}
	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}	
	
	
	public String toString ( )
	{
		return  discussion; // + " " + date;
	}
}
