package com.problemio.data;

import java.util.List;

public class Discussion 
{
	public String discussionTopicId;
	public String discussionTopicName;
	public List<DiscussionMessage> discussionMessages;

	public String getDiscussionTopicId() {
		return discussionTopicId;
	}
	public void setDiscussionTopicId(String discussionTopicId) {
		this.discussionTopicId = discussionTopicId;
	}
	public String getDiscussionTopicName() {
		return discussionTopicName;
	}
	public void setDiscussionTopicName(String discussionTopicName) {
		this.discussionTopicName = discussionTopicName;
	}
	public List<DiscussionMessage> getDiscussionMessages() {
		return discussionMessages;
	}
	public void setDiscussionMessages(List<DiscussionMessage> discussionMessages) {
		this.discussionMessages = discussionMessages;
	}
	
	@Override
	public String toString ( )
	{
		String temp = null;
		
		if ( discussionMessages != null )
		{
			for ( int i = 0; i < discussionMessages.size(); i++ )
			{
				discussionMessages.get(i).getMessage();
			}
		}
		
		return discussionTopicName;
	}
}
