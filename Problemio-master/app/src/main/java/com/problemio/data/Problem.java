package com.problemio.data;

import java.util.ArrayList;

public class Problem 
{
	public String problemName;
	public String problemId;
	public String problemDescription;
	public ArrayList<SolutionTopic> solutionTopics;
	
	public String isPrivate;
	public String hasNewComments;
	
	public String getProblemName() {
		return problemName;
	}
	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}
	public String getProblemId() {
		return problemId;
	}
	public void setProblemId(String problemId) {
		this.problemId = problemId;
	}
	
	@Override
	public String toString()
	{
		if ( isPrivate != null && isPrivate.trim().equals("0") && hasNewComments != null && hasNewComments.trim().equals("1") )
		{
			return problemName + "\n(New Comments) ";
		}
		else
		{
			return problemName;
		}
	}
	
	public String getProblemDescription() {
		return problemDescription;
	}
	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}
	public ArrayList<SolutionTopic> getSolutionTopics() {
		return solutionTopics;
	}
	public void setSolutionTopics(ArrayList<SolutionTopic> solutionTopics) {
		this.solutionTopics = solutionTopics;
	}
	public String getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(String isPrivate) {
		this.isPrivate = isPrivate;
	}
	public String getHasNewComments() {
		return hasNewComments;
	}
	public void setHasNewComments(String hasNewComments) {
		this.hasNewComments = hasNewComments;
	}

}
