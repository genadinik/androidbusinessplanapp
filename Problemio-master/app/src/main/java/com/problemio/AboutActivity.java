package com.problemio;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

/**
 * Created by alexgenadinik on 12/18/15.
 */
public class AboutActivity extends BaseActivity
{
    /**
     * Called when this activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    // TODO: start here
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.about);

        Button books = (Button)findViewById(R.id.books);
        books.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.amazon.com/Alex-Genadinik/e/B00I114WEU"));

                startActivity(browserIntent);
            }
        });

        Button courses = (Button)findViewById(R.id.courses);
        courses.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/user/alexgenadinik"));

                startActivity(browserIntent);
            }
        });

        Button website = (Button)findViewById(R.id.website);
        website.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.problemio.com"));

                startActivity(browserIntent);
            }
        });



    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
        // your code
    }
}
