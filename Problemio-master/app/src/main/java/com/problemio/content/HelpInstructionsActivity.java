package com.problemio.content;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;
import com.problemio.BaseActivity;
import com.problemio.R;

public class HelpInstructionsActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_help_instructions);
	    
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( HelpInstructionsActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
	    String first_name = prefs.getString( "first_name" , null );			    
	    String last_name = prefs.getString( "last_name" , null );			    
	    String email = prefs.getString( "email" , null );			    
	    
		Button premium_app = (Button)findViewById(R.id.premium_app);
		premium_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=business.premium"));
            	
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           
            }
        });
		
		
//	    Button target_market = (Button)findViewById(R.id.target_market);   
//	    target_market.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                //sendEmail("Advertising --> Target Users", "From advertising page, user clicked on target market" );   	
//            	
//                Intent myIntent = new Intent( AdvertisingActivity.this, TargetMarketActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });
	}
	
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
