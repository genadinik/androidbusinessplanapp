package com.problemio.content;

import utils.SendEmail;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

public class ProductStrategyActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	        super.onCreate(savedInstanceState);
		    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	        
	        setContentView(R.layout.content_product_strategy);
	        
//	        // Get session variables
//	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( ProductStrategyActivity.this);
//	        
//	        String user_id = prefs.getString( "user_id", null ); 
//	        String first_name = prefs.getString( "first_name", null ); 
//	        String email = prefs.getString( "email", null ); 
	              
			//sendEmail("Product Strategy Loading", "Loading the product strategy page for this user: " + first_name + " with email: " + email + " and user_id: " + user_id );	        
			
	        final TextView explanation = (TextView) findViewById(R.id.explanation);
	        final TextView explanation_two = (TextView) findViewById(R.id.explanation_two);


//	        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//	        try
//	        {        	
//		        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//		  			  Uri.parse("http://www.problemio.com/business/product_strategy.php"));  
//		      		startActivity(browserIntent);
//	        	
////		        if ( android.os.Build.VERSION.SDK_INT >= 14 )
////		        {
////		            Button share = (Button)findViewById(R.id.share_button); 
////		    	    share.setOnClickListener(new Button.OnClickListener() 
////		    	    {  
////		    	        public void onClick(View v) 
////		    	        {		        	
////		    	        	openOptionsMenu();
////		    	        }
////		    	    });        
////		        }
////		        else
////		        {
////		        	// HIDE THE TWO PAGE ELEMENTS
////		            Button share = (Button)findViewById(R.id.share_button); 
////		            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 
////
////		            share.setVisibility(View.GONE);
////		            share_prompt.setVisibility(View.GONE);	            
////		        }
//	        }
//	        catch ( Exception e )
//	        {
//	           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//	        }	        
	        
	        Button learn_target_market = (Button)findViewById(R.id.learn_target_market);   
	        learn_target_market.setOnClickListener(new Button.OnClickListener() 
	        {  
	            public void onClick(View v) 
	            {

	                Intent myIntent = new Intent(ProductStrategyActivity.this, TargetMarketActivity.class);
	                ProductStrategyActivity.this.startActivity(myIntent);
	            }
	        });
	    	        
//	        Button extra_help = (Button)findViewById(R.id.extra_help);           
//	        extra_help.setOnClickListener(new Button.OnClickListener() 
//	        {  
//	            public void onClick(View v) 
//	            {	
//	            	sendEmail("Product Strategy -> Extra Help", "From product strategy to extea help" );
//	                
//	                Intent myIntent = new Intent(ProductStrategyActivity.this, ExtraHelpActivity.class);
//	                ProductStrategyActivity.this.startActivity(myIntent);
//	            }
//	        });	 	     
	        
	        Button planBusiness = (Button)findViewById(R.id.plan_business);   
	        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          
	        Button learn_business_model = (Button)findViewById(R.id.learn_business_model);          

	        learn_business_model.setOnClickListener(new Button.OnClickListener() 
	        {  
	            public void onClick(View v) 
	            {	            	
	              Intent myIntent = new Intent(ProductStrategyActivity.this, BusinessModelsActivity.class);
	              ProductStrategyActivity.this.startActivity(myIntent);
	            }
	        });     
	        
	        
	        
	        planBusiness.setOnClickListener(new Button.OnClickListener() 
	        {  
	            public void onClick(View v) 
	            {	            	
	              Intent myIntent = new Intent(ProductStrategyActivity.this, AddProblemActivity.class);
	              ProductStrategyActivity.this.startActivity(myIntent);
	            }
	        });     
	        
//		    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//	        {  
//	            public void onClick(View v) 
//	            {	            	
//	                Intent myIntent = new Intent(ProductStrategyActivity.this, AskQuestionActivity.class);
//	                ProductStrategyActivity.this.startActivity(myIntent);
//	            }
//	        });	        
	 
	        TextView helpful = (TextView) findViewById(R.id.helpful);
	        final TextView helpful_text = (TextView) findViewById(R.id.helpful_text);

	        final TextView explanation_2 = (TextView) findViewById(R.id.explanation_2);
	        final TextView explanation_3 = (TextView) findViewById(R.id.explanation_3);

	        final TextView heading_1 = (TextView) findViewById(R.id.heading_1);
	        final TextView heading_2 = (TextView) findViewById(R.id.heading_2);
	        final TextView heading_3 = (TextView) findViewById(R.id.heading_3);	        
	        final TextView heading_4 = (TextView) findViewById(R.id.heading_4);	        
	        
		    Button give_review = (Button)findViewById(R.id.give_review);   
		    give_review.setOnClickListener(new Button.OnClickListener() 
		    {  
		        public void onClick(View v) 
		        {		        	
//		            Intent intent = new Intent(Intent.ACTION_VIEW);
//		        	intent.setData(Uri.parse("market://details?id=com.problemio"));
//		        	startActivity(intent);                
		            
		           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
		           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		           	 
		           	 try 
		           	 {
		           	        startActivity(intent);
		           	 } 
		           	 catch (ActivityNotFoundException anfe) 
		           	 {		           		 
		                    // Now try to redirect them to the web version:
		                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
		                    try
		                    {
		                   	   Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
		                   	   startActivity(webintent);
		                    }
		                    catch ( Exception e )
		                    {

		                    }
		           	 }		            
		        }
		    });    		   
		    
	     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);

//	        Button share_button = (Button)findViewById(R.id.product_share_button);   
//	        share_button.setOnClickListener(new Button.OnClickListener() 
//	        {  
//	            public void onClick(View v) 
//	            {
//	              sendEmail("Product ~ Maybe Share", "From Product page, user clicked on share button" );   	
//	            	
//	              builder.setMessage("You will be taken to our home page from which you can share?")
//	              .setCancelable(false)
//	              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//	                  public void onClick(DialogInterface dialog, int id) {
//	                       
//	                      sendEmail("Product ~ Yes Share", "From Product page, user clicked on share button" );   	        		       
//	                  
//	                      Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	                    		  Uri.parse("http://www.problemio.com"));
//	                      startActivity(browserIntent);
//	                  }
//	              })
//	              .setNegativeButton("No", new DialogInterface.OnClickListener() 
//	              {
//	                  public void onClick(DialogInterface dialog, int id) 
//	                  {
//	                       dialog.cancel();
//	                  }
//	              });
//	       AlertDialog alert = builder.create();
//	       alert.show();
//	            }
//	        });  		    
		    
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.layout.menu, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//        
//        return false;
//    }
//    
//    private Intent createShareIntent() 
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT, 
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//    
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//    }                
}
