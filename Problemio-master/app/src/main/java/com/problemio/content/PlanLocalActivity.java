package com.problemio.content;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

//import com.flurry.android.FlurryAgent;
import com.problemio.BaseActivity;
import com.problemio.R;
import com.problemio.VideosActivity;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 12/20/15.
 */
public class PlanLocalActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.content_plan_local);

        // Get Shared Preferences.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PlanLocalActivity.this);




        Button fundraising = (Button)findViewById(R.id.fundraising);
        fundraising.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences( PlanLocalActivity.this);

                prefs.edit()
                        .putString("url_to_watch", "https://youtu.be/KGVgamPNPNY")
                        .commit();

                Intent myIntent = new Intent(PlanLocalActivity.this, VideosActivity.class);
                PlanLocalActivity.this.startActivity(myIntent);
            }
        });




        Button marketingcourse = (Button)findViewById(R.id.marketingcourse);
        marketingcourse.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/marketing-plan-strategy-become-a-great-marketer/?couponCode=androidapp"));
                startActivity(browserIntent);
            }
        });


        Button localbook = (Button)findViewById(R.id.localbook);
        localbook.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.amazon.com/Local-Small-Business-Marketing-business/dp/1519556985"));
                startActivity(browserIntent);
            }
        });

        Button businesscours = (Button)findViewById(R.id.businesscours);
        businesscours.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                //sendEmail("Problemio Web Setup -> BlueHost", "" );

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.udemy.com/how-to-start-a-business-go-from-business-idea-to-a-business/?couponCode=androidapp"));
                startActivity(browserIntent);
            }
        });






//        Button planBusiness = (Button)findViewById(R.id.plan_business);
//        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);
//
//        planBusiness.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
////                Intent myIntent = new Intent(TopMistakesActivity.this, AddProblemActivity.class);
////                TopMistakesActivity.this.startActivity(myIntent);
//            }
//        });

//	    ask_direct_question.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent myIntent = new Intent(TopMistakesActivity.this, AskQuestionActivity.class);
//                TopMistakesActivity.this.startActivity(myIntent);
//            }
//        });



    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        //FlurryAgent.onEndSession(this);
        // your code
    }
}
