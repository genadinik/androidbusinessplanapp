package com.problemio.content;

import com.problemio.BaseActivity;

import utils.SendEmail;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.R;

public class PremiumWebAdvertisingActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.content_premium_web_marketing);
 
        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( PremiumWebAdvertisingActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
	    String first_name = prefs.getString( "first_name" , null );			    
	    String last_name = prefs.getString( "last_name" , null );			    
	    String email = prefs.getString( "email" , null );			    
	    
		//sendEmail("Premium Web Advertising Loaded", "User id: " + user_id + " name: " + first_name + " " + last_name + " and email: " + email );   
	    
	
	        
        TextView heading_1 = (TextView) findViewById(R.id.heading_1);
        TextView heading_2 = (TextView) findViewById(R.id.heading_2);
        TextView heading_3 = (TextView) findViewById(R.id.heading_3);
        TextView heading_4 = (TextView) findViewById(R.id.heading_4);
        TextView heading_5 = (TextView) findViewById(R.id.heading_5);
        TextView heading_7 = (TextView) findViewById(R.id.heading_7);
        TextView heading_8 = (TextView) findViewById(R.id.heading_8);
        TextView heading_9 = (TextView) findViewById(R.id.heading_9);
        TextView heading_10 = (TextView) findViewById(R.id.heading_10);
        TextView heading_11 = (TextView) findViewById(R.id.heading_11);
        TextView heading_12 = (TextView) findViewById(R.id.heading_12);

        
        TextView explation_1 = (TextView) findViewById(R.id.explanation_12);
        TextView explation_2 = (TextView) findViewById(R.id.explanation_2);
        TextView explation_3 = (TextView) findViewById(R.id.explanation_3);
        TextView explation_4 = (TextView) findViewById(R.id.explanation_4);
        TextView explation_5 = (TextView) findViewById(R.id.explanation_5);
        TextView explation_7 = (TextView) findViewById(R.id.explanation_7);
        TextView explation_8 = (TextView) findViewById(R.id.explanation_8);
        TextView explation_9 = (TextView) findViewById(R.id.explanation_9);
        TextView explation_10 = (TextView) findViewById(R.id.explanation_10);
        TextView explation_11 = (TextView) findViewById(R.id.explanation_11);
        TextView explation_12 = (TextView) findViewById(R.id.explanation_12);
                           
        
        TextView helpful = (TextView) findViewById(R.id.helpful);
        TextView helpful_text = (TextView) findViewById(R.id.helpful_text);
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
              //sendEmail("Plan Business -> FROM LEARN", "From home page, user clicked on plan a business button" );   	
            	
              Intent myIntent = new Intent(PremiumWebAdvertisingActivity.this, AddProblemActivity.class);
              PremiumWebAdvertisingActivity.this.startActivity(myIntent);
            }
        });     
        
	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(PremiumWebAdvertisingActivity.this, AskQuestionActivity.class);
                PremiumWebAdvertisingActivity.this.startActivity(myIntent);
            }
        });     
	   
    
    Button give_review = (Button)findViewById(R.id.give_review);   
    give_review.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {        	
            Intent intent = new Intent(Intent.ACTION_VIEW);
        	intent.setData(Uri.parse("market://details?id=com.problemio"));
        	startActivity(intent);                
        }
    });    
    
}    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}
