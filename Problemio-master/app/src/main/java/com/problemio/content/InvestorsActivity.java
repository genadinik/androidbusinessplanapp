package com.problemio.content;

import utils.SendEmail;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InvestorsActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_investors);
	        	    
		Button fundraising_app = (Button)findViewById(R.id.fundraising_app);
		fundraising_app.setOnClickListener(new Button.OnClickListener() 
        { 
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.fundraising"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           
            }
        });  

	    
	    
	    Button learn_business_model = (Button)findViewById(R.id.learn_business_model);   
	    learn_business_model.setOnClickListener(new Button.OnClickListener() 
        {	    	
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent( InvestorsActivity.this, BusinessModelsActivity.class);
                InvestorsActivity.this.startActivity(myIntent);                            
            }
        });	    	    	    
	    
	    Button stage_tactics = (Button)findViewById(R.id.stage_tactics);   
	    stage_tactics.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            
                Intent myIntent = new Intent( InvestorsActivity.this, StageTacticsActivity.class);
                InvestorsActivity.this.startActivity(myIntent);                            
            }
        });	    	    

	    
//	    Button learn_marketing = (Button)findViewById(R.id.learn_marketing);   
//	    learn_marketing.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            
//                Intent myIntent = new Intent( InvestorsActivity.this, AdvertisingActivity.class);
//                InvestorsActivity.this.startActivity(myIntent);                            
//            }
//        });	    
	    

	    
	    
        final TextView explanation_one = (TextView) findViewById(R.id.explanation_one);

//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Investors -> Extra Help", "From investors page to extra help" );   	
//                
//                Intent myIntent = new Intent(InvestorsActivity.this, ExtraHelpActivity.class);
//                InvestorsActivity.this.startActivity(myIntent);
//            }
//        });	 	     
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(InvestorsActivity.this, AddProblemActivity.class);
              InvestorsActivity.this.startActivity(myIntent);
            }
        });     
        
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);   
                
           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
           	 
           	 try 
           	 {
           	        startActivity(intent);
           	 } 
           	 catch (ActivityNotFoundException anfe) 
           	 {           		 
                    // Now try to redirect them to the web version:
                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                    try
                    {
                   	   Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                   	   startActivity(webintent);
                    }
                    catch ( Exception e )
                    {

                    }
           	 }                   
            }
        });        
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(InvestorsActivity.this, AskQuestionActivity.class);
//                InvestorsActivity.this.startActivity(myIntent);
//            }
//        });	        				
		

	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}
