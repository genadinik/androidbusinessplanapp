package com.problemio.content;

import utils.SendEmail;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.LearnActivity;
import com.problemio.R;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BusinessIdeasActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.content_business_ideas);
 
        // Get session variables
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( BusinessIdeasActivity.this);
        
        String user_id = prefs.getString( "user_id", null ); 
        String first_name = prefs.getString( "first_name", null ); 
        String email = prefs.getString( "email", null ); 
         		
		Button business_idea_app = (Button)findViewById(R.id.business_idea_app);
		business_idea_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	

             
            }
        });   		
		
		
		
		
		
		
		
		
		
		
		
        Button product_strategy_button = (Button)findViewById(R.id.product_strategy_button);   
        product_strategy_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Biz Ideas --> Product Strategy", "From business ideas page, user clicked on product strategy" );   	
            	
                Intent myIntent = new Intent(BusinessIdeasActivity.this, ProductStrategyActivity.class);
                BusinessIdeasActivity.this.startActivity(myIntent);
            }
        });        
        
        Button target_users_button = (Button)findViewById(R.id.target_users_button);   
        target_users_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Biz Ideas --> Target Users", "From biz ideas page, user clicked on target market" );   	
            	
                Intent myIntent = new Intent(BusinessIdeasActivity.this, TargetMarketActivity.class);
                BusinessIdeasActivity.this.startActivity(myIntent);
            }
        });        
		
        
//        Button learn_more = (Button)findViewById(R.id.learn_more);           
//        learn_more.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	//sendEmail("Biz Ideas --> Learn More", "From business ideas, user clicked on learn more" );   	
//                
//                Intent myIntent = new Intent( BusinessIdeasActivity.this, LearnActivity.class);
//                BusinessIdeasActivity.this.startActivity(myIntent);
//            }
//        });	                
        
        
//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Biz Ideas --> Extra Help", "From business ideas, user clicked on extra help" );   	
//                
//                Intent myIntent = new Intent( BusinessIdeasActivity.this, ExtraHelpActivity.class);
//                BusinessIdeasActivity.this.startActivity(myIntent);
//            }
//        });	        
        
        Button give_review = (Button)findViewById(R.id.give_review);   
        give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);
                
           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
           	 
           	 try 
           	 {
           	        startActivity(intent);
           	 } 
           	 catch (ActivityNotFoundException anfe) 
           	 {           		 
                    // Now try to redirect them to the web version:
                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                    try
                    {
                   	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                   	 startActivity(webintent);
                    }
                    catch ( Exception e )
                    {

                    }
           	 }                   
            }
        });    	
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
              //sendEmail("Biz Ideas -> Plan Biz", "From business ideas page, user clicked on plan a business button" );   	
            	
              Intent myIntent = new Intent(BusinessIdeasActivity.this, AddProblemActivity.class);
              BusinessIdeasActivity.this.startActivity(myIntent);
            }
        });     
        
	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Ask Question -> From Biz Ideas", "From business ideas page, user clicked on ask question" );   	
            	
                Intent myIntent = new Intent(BusinessIdeasActivity.this, AskQuestionActivity.class);
                BusinessIdeasActivity.this.startActivity(myIntent);
            }
        });                     
    }
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}
