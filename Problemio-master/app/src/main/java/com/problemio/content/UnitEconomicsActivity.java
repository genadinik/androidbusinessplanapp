package com.problemio.content;

import utils.SendEmail;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.ExtraHelpActivity;
import com.problemio.R;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

public class UnitEconomicsActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.content_unit_economics);
 
//        // Get Shared Preferences.
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( UnitEconomicsActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    
	    
    
        TextView explanation = (TextView) findViewById(R.id.explanation);
        
        TextView explanation1 = (TextView) findViewById(R.id.explanation1);  
        TextView explanation_heading1 = (TextView) findViewById(R.id.explanation_heading1);  

        TextView explanation2 = (TextView) findViewById(R.id.explanation2);  
        TextView explanation_heading2 = (TextView) findViewById(R.id.explanation_heading2);        

        TextView explanation3 = (TextView) findViewById(R.id.explanation3);  
        TextView explanation_heading3 = (TextView) findViewById(R.id.explanation_heading3);    

        TextView explanation4 = (TextView) findViewById(R.id.explanation4);  
        TextView explanation_heading4 = (TextView) findViewById(R.id.explanation_heading4);   
        
        TextView explanation5 = (TextView) findViewById(R.id.explanation5);  
        TextView explanation_heading5 = (TextView) findViewById(R.id.explanation_heading5);           
        
        // Now add the button listeners.
//        Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question); 
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	
//                Intent myIntent = new Intent(UnitEconomicsActivity.this, AskQuestionActivity.class);
//                UnitEconomicsActivity.this.startActivity(myIntent);
//            }
//        });
	    
	    
	    Button planBusiness = (Button)findViewById(R.id.plan_business);   
        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
              Intent myIntent = new Intent(UnitEconomicsActivity.this, AddProblemActivity.class);
              UnitEconomicsActivity.this.startActivity(myIntent);
            }
        });     

//        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {      
//        	
//		        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//				  Uri.parse("http://www.problemio.com/business/business_economics.php"));  
//        		startActivity(browserIntent);
//        	
//        	
//        	
//        	
////	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
////	        {
////	            Button share = (Button)findViewById(R.id.share_button); 
////	    	    share.setOnClickListener(new Button.OnClickListener() 
////	    	    {  
////	    	        public void onClick(View v) 
////	    	        {		        	
////	    	        	openOptionsMenu();
////	    	        }
////	    	    });        
////	        }
////	        else
////	        {
////	        	// HIDE THE TWO PAGE ELEMENTS
////	            Button share = (Button)findViewById(R.id.share_button); 
////	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 
////
////	            share.setVisibility(View.GONE);
////	            share_prompt.setVisibility(View.GONE);	            
////	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//        }
       
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);   
                
	           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
	           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	           	 
	           	 try 
	           	 {
	           	        startActivity(intent);
	           	 } 
	           	 catch (ActivityNotFoundException anfe) 
	           	 {	           		 
	                    // Now try to redirect them to the web version:
	                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
	                    try
	                    {
	                   	   Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                   	   startActivity(webintent);
	                    }
	                    catch ( Exception e )
	                    {

	                    }
	           	 }	                
            }
        });        
        
        
    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }   
    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.layout.menu, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//        
//        return false;
//    }
//    
//    private Intent createShareIntent() 
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT, 
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//    
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//    }                
}



