package com.problemio.content;

import utils.Consts;
import utils.SendEmail;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.LearnActivity;
import com.problemio.R;

public class TopMistakesActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.content_top_mistakes);
 
        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( TopMistakesActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
	    String first_name = prefs.getString( "first_name" , null );			    
	    String last_name = prefs.getString( "last_name" , null );			    
	    String email = prefs.getString( "email" , null );			    
	    	        
        TextView heading_1 = (TextView) findViewById(R.id.heading_1);
        TextView text_1 = (TextView) findViewById(R.id.text_1);        

        TextView heading_2 = (TextView) findViewById(R.id.heading_2);
        TextView text_2 = (TextView) findViewById(R.id.text_2);     
        
        TextView heading_3 = (TextView) findViewById(R.id.heading_3);
        TextView text_3 = (TextView) findViewById(R.id.text_3);     
        
        TextView heading_4 = (TextView) findViewById(R.id.heading_4);
        TextView text_4 = (TextView) findViewById(R.id.text_4);     
        
        TextView heading_5 = (TextView) findViewById(R.id.heading_5);
        TextView text_5 = (TextView) findViewById(R.id.text_5);     
        
        TextView heading_6 = (TextView) findViewById(R.id.heading_6);
        TextView text_6 = (TextView) findViewById(R.id.text_6);     
        
        TextView heading_7 = (TextView) findViewById(R.id.heading_7);
        TextView text_7 = (TextView) findViewById(R.id.text_7);     
        
        TextView heading_8 = (TextView) findViewById(R.id.heading_8);
        TextView text_8 = (TextView) findViewById(R.id.text_8);     
        
        TextView heading_9 = (TextView) findViewById(R.id.heading_9);
        TextView text_9 = (TextView) findViewById(R.id.text_9);     
        
        TextView heading_10 = (TextView) findViewById(R.id.heading_10);
        TextView text_10 = (TextView) findViewById(R.id.text_10);     
        
        TextView heading_11 = (TextView) findViewById(R.id.heading_11);
        TextView text_11 = (TextView) findViewById(R.id.text_11);     

        TextView heading_12 = (TextView) findViewById(R.id.heading_12);
        TextView text_12 = (TextView) findViewById(R.id.text_12);        
        
        TextView helpful = (TextView) findViewById(R.id.helpful);
        
        
        
        
        
        
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          
        
        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
              Intent myIntent = new Intent(TopMistakesActivity.this, AddProblemActivity.class);
              TopMistakesActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	
//                Intent myIntent = new Intent(TopMistakesActivity.this, AskQuestionActivity.class);
//                TopMistakesActivity.this.startActivity(myIntent);
//            }
//        });     
	      
    
    Button give_review = (Button)findViewById(R.id.give_review);   
    give_review.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {        	
            Intent intent = new Intent(Intent.ACTION_VIEW);
        	intent.setData(Uri.parse("market://details?id=com.problemio"));
        	startActivity(intent);                
        }
    });    
    
}    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}
