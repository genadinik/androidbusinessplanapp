package com.problemio.content;

import utils.SendEmail;
import utils.SendResourceEmail;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.ExtraHelpActivity;
import com.problemio.LearnActivity;
import com.problemio.R;

public class WebSetupActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_web_setup);
	    
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( WebSetupActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String last_name = prefs.getString( "last_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    
	    	    
	    //Button web_content_squarespace = (Button)findViewById(R.id.web_content_squarespace);
	    
	    Button marketing_button_1 = (Button)findViewById(R.id.marketing_button_1);
	    Button marketing_button_2 = (Button)findViewById(R.id.marketing_button_2);
	    
	    
	    
	    
	    

	    
	    
	    
	    //final TextView email_ask = (TextView) findViewById(R.id.email_ask);
	    
	    final EditText email_form = (EditText) findViewById(R.id.web_email_text);
	    
	    
//	    Button next_steps = (Button)findViewById(R.id.next_steps);
//	    next_steps.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	                  	        
//	            Intent myIntent = new Intent(WebSetupActivity.this, WebsiteServiceActivity.class);
//	            WebSetupActivity.this.startActivity(myIntent);	
//            }
//        });	    
	    
	    
	    
	    
	    
	    
//	    Button send_web_resource = (Button)findViewById(R.id.send_web_resource_button);
//	    send_web_resource.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            
//      	        String email_ask_input = email_form.getText().toString().trim();
//                sendEmail("Web Setup -> Resource Email", "From web setup page, user clicked on send email" );   	
//      	        
//            	if ( email_ask_input == null )
//            	{
//                    sendEmail("Web Setup -> Email validation error", "From web setup page, user clicked on send email" );   	
//
//            		// CREATE AN ERROR MESSAGE
//      		      Toast.makeText(getApplicationContext(), "Please enter an email address.", Toast.LENGTH_LONG).show();	
//            	}
//            	else
//            	{
//        		    Toast.makeText(getApplicationContext(), "Email sent!", Toast.LENGTH_LONG).show();	
//
//                    sendEmail("Web Setup -> Sending Email", "From web setup page, sending resource email" );   	
//
//            		sendResourceEmail( email_ask_input );
//            	}
//            	
////                sendEmail("Web Setup -> Distilled", "From web setup page, user clicked on distilled" );   	
////            	
////                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
////              		  Uri.parse("http://tracking.distilled.net/SHI"));  
////                startActivity(browserIntent);
//            }
//        });
	    
	    
	    
	    
//	    Button distilled_button = (Button)findViewById(R.id.distilled_button);
//	    distilled_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                sendEmail("Web Setup -> Distilled", "From web setup page, user clicked on distilled" );   	
//            	
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//              		  Uri.parse("http://tracking.distilled.net/SHI"));  
//                startActivity(browserIntent);
//            }
//        });	    
	    
	    
//	    Button web_mindbug = (Button)findViewById(R.id.web_mindbug);
//	    web_mindbug.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                sendEmail("Web Setup -> Mindbug", "From web setup page, user clicked on mindbug button" );   	
//            	
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//              		  Uri.parse("http://www.mindbugstudios.com/?utm_source=bzcoachplan&utm_medium=cpc&utm_term=intro%2Bvideo&utm_content=textlink&utm_campaign=Business%2BCoach%2BPlan"));
//                startActivity(browserIntent);
//            }
//        });
	    
//	  web_content_squarespace.setOnClickListener(new Button.OnClickListener() 
//	  {  
//	      public void onClick(View v) 
//	      {	      	          	
//              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//            		  Uri.parse("http://squarespace.7eer.net/c/35378/38409/1291"));
//              
//              startActivity(browserIntent);
//	      }
//	  });
	  
//	  marketing_button_1.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	        	
//	            Intent myIntent = new Intent(WebSetupActivity.this, AdvertisingActivity.class);
//	            WebSetupActivity.this.startActivity(myIntent);
//	        }
//	    }); 
//	    	 
//	  marketing_button_2.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	        	
//	            Intent myIntent = new Intent(WebSetupActivity.this, PremiumWebAdvertisingActivity.class);
//	            WebSetupActivity.this.startActivity(myIntent);
//	        }
//	    });	    
	    

	  
//		final TextView web_setup_page_title = (TextView) findViewById(R.id.web_setup_page_title);
//	    final TextView web_setup_heading = (TextView) findViewById(R.id.web_setup_heading);
//
//	    final TextView web_setup_heading_1 = (TextView) findViewById(R.id.web_setup_heading_1);
//	    final TextView web_setup_text_1 = (TextView) findViewById(R.id.web_setup_text_1);
//		
//	    final TextView web_setup_heading_2 = (TextView) findViewById(R.id.web_setup_heading_2);
//	    final TextView web_setup_text_2 = (TextView) findViewById(R.id.web_setup_text_2);
//
//	    final TextView web_setup_heading_3 = (TextView) findViewById(R.id.web_setup_heading_3);
//	    final TextView web_setup_text_3 = (TextView) findViewById(R.id.web_setup_text_3);
//	    
//	    final TextView optional_heading_1 = (TextView) findViewById(R.id.optional_heading_1);
//	    final TextView optional_text_1 = (TextView) findViewById(R.id.optional_text_1);
//
//	    final TextView conclusion = (TextView) findViewById(R.id.conclusion);

	    
//	    Button web_content_squarespace = (Button)findViewById(R.id.web_content_squarespace);
//	    
//	    Button marketing_button_1 = (Button)findViewById(R.id.marketing_button_1);
//	    Button marketing_button_2 = (Button)findViewById(R.id.marketing_button_2);
	    
	    
	    
	    
	    
//	    final TextView download_heading = (TextView) findViewById(R.id.download_heading);
//	    final TextView download_explain = (TextView) findViewById(R.id.download_explain);
//	    final TextView email_ask = (TextView) findViewById(R.id.email_ask);
//	    final EditText email_form = (EditText) findViewById(R.id.web_email_text);
	    
	    
 
	    
	    
	    
	    
	    
	    
	    Button send_web_resource = (Button)findViewById(R.id.send_web_resource_button);
	    send_web_resource.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            
      	        String email_ask_input = email_form.getText().toString().trim();
      	        
            	if ( email_ask_input == null )
            	{
            		// CREATE AN ERROR MESSAGE
      		      Toast.makeText(getApplicationContext(), "Please enter an email address.", Toast.LENGTH_LONG).show();	
            	}
            	else
            	{
        		    Toast.makeText(getApplicationContext(), "Email sent!", Toast.LENGTH_LONG).show();	

            		sendResourceEmail( email_ask_input );
            	}
            	
//                sendEmail("Web Setup -> Distilled", "From web setup page, user clicked on distilled" );   	
//            	
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//              		  Uri.parse("http://tracking.distilled.net/SHI"));  
//                startActivity(browserIntent);
            }
        });
	    
	    
	    
	    
//	    Button distilled_button = (Button)findViewById(R.id.distilled_button);
//	    distilled_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                sendEmail("Web Setup -> Distilled", "From web setup page, user clicked on distilled" );   	
//            	
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//              		  Uri.parse("http://tracking.distilled.net/SHI"));  
//                startActivity(browserIntent);
//            }
//        });	    
	    
	    
//	    Button web_mindbug = (Button)findViewById(R.id.web_mindbug);
//	    web_mindbug.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//              		  Uri.parse("http://www.mindbugstudios.com/?utm_source=bzcoachplan&utm_medium=cpc&utm_term=intro%2Bvideo&utm_content=textlink&utm_campaign=Business%2BCoach%2BPlan"));
//                startActivity(browserIntent);
//            }
//        });
	    
	  
	  marketing_button_1.setOnClickListener(new Button.OnClickListener() 
	    {  
	        public void onClick(View v) 
	        {	        	
	            Intent myIntent = new Intent(WebSetupActivity.this, AdvertisingActivity.class);
	            WebSetupActivity.this.startActivity(myIntent);
	        }
	    }); 
	    	 
	  marketing_button_2.setOnClickListener(new Button.OnClickListener() 
	    {  
	        public void onClick(View v) 
	        {	        	
	            Intent myIntent = new Intent(WebSetupActivity.this, PremiumWebAdvertisingActivity.class);
	            WebSetupActivity.this.startActivity(myIntent);
	        }
	    });	    
	    	  
	  
	    
	  
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.problemio"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }
            }
        });
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);        
        //Button web_share_button = (Button)findViewById(R.id.web_share_button);   


        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(WebSetupActivity.this, AddProblemActivity.class);
              WebSetupActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(WebSetupActivity.this, AskQuestionActivity.class);
//                WebSetupActivity.this.startActivity(myIntent);
//            }
//        });	  
	    
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	    web_share_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//            	sendEmail("Web ~ Maybe Share", "From web page, user clicked on share button" );   	
//            	
//                builder.setMessage("Like or Tweet from our home page to share with your friends? It helps the app grow so thank you!")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                         
//                        sendEmail("Web ~ Yes Share", "From web page, user clicked on share button" );   	        		       
//                    
//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//                      		  Uri.parse("http://www.problemio.com"));
//                        startActivity(browserIntent);
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() 
//                {
//                    public void onClick(DialogInterface dialog, int id) 
//                    {
//                         dialog.cancel();
//                    }
//                });
//         AlertDialog alert = builder.create();
//         alert.show();
//            }
//        });	  
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    public void sendResourceEmail( String to_email )
    {
        String[] params = new String[] { "https://www.problemio.com/problems/send_resource_email_mobile.php" , to_email };

        SendResourceEmail task = new SendResourceEmail();
        task.execute(params);      	
    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }      
}
