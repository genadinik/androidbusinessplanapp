package com.problemio.content;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

public class AdvertisingActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_advertising);
	    
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AdvertisingActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String last_name = prefs.getString( "last_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    
	    
		Button marketing_app = (Button)findViewById(R.id.marketing_app);
		marketing_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           
            }
        });
		
		
//	    Button target_market = (Button)findViewById(R.id.target_market);   
//	    target_market.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                //sendEmail("Advertising --> Target Users", "From advertising page, user clicked on target market" );   	
//            	
//                Intent myIntent = new Intent( AdvertisingActivity.this, TargetMarketActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });
	    	      
	    
//	    Button stage_tactics = (Button)findViewById(R.id.stage_tactics);   
//	    stage_tactics.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                //sendEmail("Advertising --> Stage Tactics", "From advertising page, user clicked on stage tactics" );   	
//            	
//                Intent myIntent = new Intent( AdvertisingActivity.this, StageTacticsActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });	    
	    
	
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Advertising --> Give Review", "From advertising page, user clicked on give review" );   	
            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.problemio"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           	
            }
        });

	    
//	    Button business_models = (Button)findViewById(R.id.business_models);   
//	    business_models.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                sendEmail("Advertising --> Biz models", "From advertising page, user clicked on business models" );   	
//            
//                Intent myIntent = new Intent(AdvertisingActivity.this, BusinessModelsActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//                            
//            }
//        });	    
	    
	    
        final TextView explanation_two = (TextView) findViewById(R.id.explanation_two);

		
//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Advertising Activity -> Extra Help", "From marketing page, " +
//            			"user clicked on extra help" );   	
//                
//                Intent myIntent = new Intent(AdvertisingActivity.this, ExtraHelpActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });	 	     
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(AdvertisingActivity.this, AddProblemActivity.class);
              AdvertisingActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(AdvertisingActivity.this, AskQuestionActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });	        		

	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
//
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}