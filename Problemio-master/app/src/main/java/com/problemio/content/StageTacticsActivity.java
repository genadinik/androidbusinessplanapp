package com.problemio.content;

import utils.SendEmail;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

public class StageTacticsActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_stage_tactics);
	       
		Button fundraising_app = (Button)findViewById(R.id.fundraising_app);
		fundraising_app.setOnClickListener(new Button.OnClickListener() 
        { 
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.fundraising"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           
            }
        });  
	    
	    Button learn_product = (Button)findViewById(R.id.learn_product);   
	    learn_product.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent( StageTacticsActivity.this, ProductStrategyActivity.class);
                StageTacticsActivity.this.startActivity(myIntent);
            }
        });	    
	    
	    
	    
	    Button target_market = (Button)findViewById(R.id.target_market);   
	    target_market.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent( StageTacticsActivity.this, TargetMarketActivity.class);
                StageTacticsActivity.this.startActivity(myIntent);
            }
        });
	    	        

//        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {      
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//			  Uri.parse("http://www.problemio.com/business/company_stages.php"));  
//    		startActivity(browserIntent);
//        	
////	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
////	        {
////	            Button share = (Button)findViewById(R.id.share_button); 
////	    	    share.setOnClickListener(new Button.OnClickListener() 
////	    	    {  
////	    	        public void onClick(View v) 
////	    	        {		        	
////	    	        	openOptionsMenu();
////	    	        }
////	    	    });        
////	        }
////	        else
////	        {
////	        	// HIDE THE TWO PAGE ELEMENTS
////	            Button share = (Button)findViewById(R.id.share_button); 
////	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 
////
////	            share.setVisibility(View.GONE);
////	            share_prompt.setVisibility(View.GONE);	            
////	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//        }	    
	    
//	    Button give_review = (Button)findViewById(R.id.give_review);   
//	    give_review.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                sendEmail("Advertising --> Give Review", "From advertising page, user clicked on give review" );   	
//            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);                
//            }
//        });

	    
	    Button learn_marketing = (Button)findViewById(R.id.learn_marketing);   
	    learn_marketing.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Stage Tactics --> Learn Marketing", "From stage tactics page, user clicked on marketing" );   	
            
                Intent myIntent = new Intent( StageTacticsActivity.this, AdvertisingActivity.class);
                StageTacticsActivity.this.startActivity(myIntent);
                            
            }
        });	    
	    
	    Button learn_investors = (Button)findViewById(R.id.learn_investors);   
	    learn_investors.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Stage Tactics --> Learn Investors", "From stage tactics page, user clicked on investors" );   	
            
                Intent myIntent = new Intent( StageTacticsActivity.this, InvestorsActivity.class);
                StageTacticsActivity.this.startActivity(myIntent);
                            
            }
        });	 
	    
	    	    
//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Stage Tactics -> Extra Help", "From stage tactics, user clicked on extra help" );   	
//                
//                Intent myIntent = new Intent( StageTacticsActivity.this, ExtraHelpActivity.class);
//                StageTacticsActivity.this.startActivity(myIntent);
//            }
//        });	 	     
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(StageTacticsActivity.this, AddProblemActivity.class);
              StageTacticsActivity.this.startActivity(myIntent);
            }
        });     
        
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);  
                
	           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
	           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	           	 
	           	 try 
	           	 {
	           	        startActivity(intent);
	           	 } 
	           	 catch (ActivityNotFoundException anfe) 
	           	 {	           		 
	                    // Now try to redirect them to the web version:
	                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
	                    try
	                    {
	                   	   Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                   	   startActivity(webintent);
	                    }
	                    catch ( Exception e )
	                    {

	                    }
	           	 }	                
            }
        });        
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(StageTacticsActivity.this, AskQuestionActivity.class);
//                StageTacticsActivity.this.startActivity(myIntent);
//            }
//        });	 	   
	    
     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);

//        Button share_button = (Button)findViewById(R.id.tactics_share_button);   
//        share_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//              sendEmail("Stage Tactics ~ Maybe Share", "From Stage Tactics page, user clicked on share button" );   	
//            	
//              builder.setMessage("You will be taken to our home page from which you can share?")
//              .setCancelable(false)
//              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                  public void onClick(DialogInterface dialog, int id) {
//                       
//                      sendEmail("Stage Tactics ~ Yes Share", "From Stage Tactics page, user clicked on share button" );   	        		       
//                  
//                      Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//                    		  Uri.parse("http://www.problemio.com"));
//                      startActivity(browserIntent);
//                  }
//              })
//              .setNegativeButton("No", new DialogInterface.OnClickListener() 
//              {
//                  public void onClick(DialogInterface dialog, int id) 
//                  {
//                       dialog.cancel();
//                  }
//              });
//       AlertDialog alert = builder.create();
//       alert.show();
//            }
//        });  
	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.layout.menu, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//        
//        return false;
//    }
//    
//    private Intent createShareIntent() 
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT, 
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//    
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//    }                
}
