package com.problemio.content;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.HowToGetInvestmentActivity;
import com.problemio.R;

public class FundraisingActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.content_fundraising);
	    
//	    // Get Shared Preferences.
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( FundraisingActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String last_name = prefs.getString( "last_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    
	    
        Button job_button = (Button)findViewById(R.id.job_button);          
        Button fundraising_app_button = (Button)findViewById(R.id.fundraising_app_button);
        
        
//        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {  
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//			  Uri.parse("http://www.problemio.com/business/fundraising_ideas_and_ways_to_fundraise.php"));  
//    		startActivity(browserIntent);
//        	
////	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
////	        {
////	            Button share = (Button)findViewById(R.id.share_button); 
////	    	    share.setOnClickListener(new Button.OnClickListener() 
////	    	    {  
////	    	        public void onClick(View v) 
////	    	        {		        	
////	    	        	openOptionsMenu();
////	    	        }
////	    	    });        
////	        }
////	        else
////	        {
////	        	// HIDE THE TWO PAGE ELEMENTS
////	            Button share = (Button)findViewById(R.id.share_button); 
////	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 
////
////	            share.setVisibility(View.GONE);
////	            share_prompt.setVisibility(View.GONE);	            
////	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//        }        

        Button business_ideas_app = (Button)findViewById(R.id.business_ideas_app);
        Button marketing_app = (Button)findViewById(R.id.marketing_app);
        
//        Button fundme = (Button)findViewById(R.id.fundme);
//        fundme.setOnClickListener(new Button.OnClickListener() 
//        {  
//          public void onClick(View v) 
//          {	
//              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//            		  Uri.parse("http://www.gofundme.com/sign-up/?aff=pmio"));
//              startActivity(browserIntent);
//          }
//        });
	    
		//Button ledio_button = (Button)findViewById(R.id.ledio_button);

        Button 	moneyapp = (Button)findViewById(R.id.moneyapp);
        moneyapp.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {	
        	  Intent intent = new Intent(Intent.ACTION_VIEW);
          	intent.setData(Uri.parse("market://details?id=make.money"));
          	            	 
          	 try 
          	 {
          	        startActivity(intent);
          	 } 
          	 catch (ActivityNotFoundException anfe) 
          	 {
                   try
                   {
                  	 Uri uri = Uri.parse("market://search?q=pname:make.money");
                  	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                  	 startActivity(next_intent);  
                   }
                   catch ( Exception e)
                   {
                       // Now try to redirect them to the web version:
                       Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=make.money");
                       try
                       {
                      	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                      	 startActivity(webintent);
                       }
                       catch ( Exception except )
                       {

                       }
                   }
          	 }           	            	              
          }
        });        
        

        
//        ledio_button.setOnClickListener(new Button.OnClickListener()
//        {
//          public void onClick(View v)
//          {
//
//              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//            		  Uri.parse("http://prosper.evyy.net/c/225224/27098/994"));
//
//			  boolean isChromeInstalled = isPackageInstalled("com.android.chrome", FundraisingActivity.this);
//			  if (isChromeInstalled) {
//				  browserIntent.setPackage("com.android.chrome");
//				  startActivity(browserIntent);
//			  }else{
//				  Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//				  startActivity(chooserIntent);
//			  }
//
//              startActivity(browserIntent);
//          }
//        });
        
        job_button.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {	            	          	
        	  Intent myIntent = new Intent(FundraisingActivity.this, JobActivity.class);
        	  FundraisingActivity.this.startActivity(myIntent);

          }
        });        
        
        

	      marketing_app.setOnClickListener(new Button.OnClickListener() 
	      {  
	          public void onClick(View v) 
	          {	 		              
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	
	        }
	      });	 	     	    	            
        
	    business_ideas_app.setOnClickListener(new Button.OnClickListener() 
	      {  
	          public void onClick(View v) 
	          {		              
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	
	        }
	      });	 	     	    	            
        
        
        fundraising_app_button.setOnClickListener(new Button.OnClickListener() 
	    {  
	        public void onClick(View v) 
	        {		        			        
	        	Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }
	        }
	    }); 
//
//        loans_button.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {		        			        	
//	            Intent myIntent = new Intent(ContentLearnActivity.this, ContentLoansActivity.class);
//	            ContentLearnActivity.this.startActivity(myIntent);
//	        }
//	    }); 
//        
//        investments_button.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {		        			        	
//	            Intent myIntent = new Intent(ContentLearnActivity.this, ContentInvestmentActivity.class);
//	            ContentLearnActivity.this.startActivity(myIntent);
//	        }
//	    }); 
//        
//        donations_button.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {		        			        	
//	            Intent myIntent = new Intent(ContentLearnActivity.this, ContentDonationActivity.class);
//	            ContentLearnActivity.this.startActivity(myIntent);
//	        }
//	    });         
        
        
        
//    TextView squarespace_text = (TextView)findViewById(R.id.squarespace_text);           
//    Button squarespace_button = (Button)findViewById(R.id.squarespace_button);          
//    squarespace_button.setOnClickListener(new Button.OnClickListener() 
//    {  
//        public void onClick(View v) 
//        {
//          sendEmail("Marketing Learn -> Squarespace", "From learn page, user clicked on squarespace button" );   	
//        	
//          Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//        		  Uri.parse("http://squarespace.7eer.net/c/35378/38409/1291"));
//          
//          startActivity(browserIntent);
//        }
//    });            
       
//    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//    {  
//        public void onClick(View v)         
//        {
//        	
//            Intent myIntent = new Intent(FundraisingActivity.this, AskQuestionActivity.class);
//            FundraisingActivity.this.startActivity(myIntent);
//        }
//    });     
    
    


	Button give_review = (Button)findViewById(R.id.give_review);   
	give_review.setOnClickListener(new Button.OnClickListener() 
	{  
	    public void onClick(View v) 
	    {	    	
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	    	intent.setData(Uri.parse("market://details?id=com.problemio"));
	    	startActivity(intent);                
	    }
	});
} 


	// Subject , body
//	public void sendEmail( String subject , String body )
//	{
//	    String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php",
//	    		subject, body };
//
//	    SendEmail task = new SendEmail();
//	    task.execute(params);
//	}

	private boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}
}
