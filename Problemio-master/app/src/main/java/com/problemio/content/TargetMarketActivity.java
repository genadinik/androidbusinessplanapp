package com.problemio.content;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

public class TargetMarketActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_target_market);
	        
	    final TextView heading_0 = (TextView) findViewById(R.id.heading_0);
	    final TextView explanation_0 = (TextView) findViewById(R.id.explanation_0);   		
   		
	    final TextView explanation = (TextView) findViewById(R.id.explanation);

	    final TextView heading_01 = (TextView) findViewById(R.id.heading_01);
	    final TextView explanation_01 = (TextView) findViewById(R.id.explanation_01);  
	    
	    final TextView target_users_heading = (TextView) findViewById(R.id.target_users_heading);
	    final TextView explanation1 = (TextView) findViewById(R.id.explanation1);

	        
	    final TextView helpful = (TextView) findViewById(R.id.helpful);
	    final TextView helpful_text = (TextView) findViewById(R.id.helpful_text);
	    	    
	    
	        
	        
	        
	    Button learn_advertising = (Button)findViewById(R.id.learn_advertising);   
	    learn_advertising.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                //sendEmail("Product Strategy --> Marketing", "From product strategy page, user clicked on advertising" );   	
            	
                Intent myIntent = new Intent( TargetMarketActivity.this, AdvertisingActivity.class);
                TargetMarketActivity.this.startActivity(myIntent);
            }
        });
	    	        
	
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);   
                
	           	 try 
	           	 {
	           	        startActivity(intent);
	           	 } 
	           	 catch (ActivityNotFoundException anfe) 
	           	 {
	           		Uri uri = Uri.parse("market://search?q=pname:com.problemio");
		           	Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
		           	 
	           		 try
	           		 {
	           			 startActivity(next_intent); 
	           		 }
	           		 catch ( Exception nextExc )
	           		 {			           		 
		                    // Now try to redirect them to the web version:
		                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
		                    try
		                    {
		                   	   Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
		                   	   startActivity(webintent);
		                    }
		                    catch ( Exception e )
		                    {

		                    } 
	           		 }
	           }       
            }
        });
	    
//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Target Market -> Extra Help", "From target market page, " +
//            			"user clicked on extra help" );   	
//                
//                Intent myIntent = new Intent(TargetMarketActivity.this, ExtraHelpActivity.class);
//                TargetMarketActivity.this.startActivity(myIntent);
//            }
//        });	 	     
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(TargetMarketActivity.this, AddProblemActivity.class);
              TargetMarketActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(TargetMarketActivity.this, AskQuestionActivity.class);
//                TargetMarketActivity.this.startActivity(myIntent);
//            }
//        });	 
		
		
	    
	    
	    
	    

	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }        
}
