package com.problemio.content;

import utils.SendEmail;

//import com.flurry.android.FlurryAgent;
import com.problemio.AddProblemActivity;
import com.problemio.AskQuestionActivity;
import com.problemio.BaseActivity;
import com.problemio.R;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

public class BusinessModelsActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_business_models);
	        
//	    Button target_market = (Button)findViewById(R.id.target_market);   
//	    target_market.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                sendEmail("Business Models --> Target Market", "From business models, user clicked on target market" );   	
//            	
//                Intent myIntent = new Intent( BusinessModelsActivity.this, TargetMarketActivity.class);
//                BusinessModelsActivity.this.startActivity(myIntent);
//            }
//        });


//        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {        	
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//			  Uri.parse("http://www.problemio.com/business/business_model.php"));  
//    		startActivity(browserIntent);
//        	
////	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
////	        {
////	            Button share = (Button)findViewById(R.id.share_button); 
////	    	    share.setOnClickListener(new Button.OnClickListener() 
////	    	    {  
////	    	        public void onClick(View v) 
////	    	        {		        	
////	    	        	openOptionsMenu();
////	    	        }
////	    	    });        
////	        }
////	        else
////	        {
////	        	// HIDE THE TWO PAGE ELEMENTS
////	            Button share = (Button)findViewById(R.id.share_button); 
////	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 
////
////	            share.setVisibility(View.GONE);
////	            share_prompt.setVisibility(View.GONE);	            
////	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//        }	    
	    
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	startActivity(intent);    
                
           	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
           	 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
           	 
           	 try 
           	 {
           	        startActivity(intent);
           	 } 
           	 catch (ActivityNotFoundException anfe) 
           	 {           		 
                    // Now try to redirect them to the web version:
                    Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                    try
                    {
                   	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                   	 startActivity(webintent);
                    }
                    catch ( Exception e )
                    {

                    }
           	 }                   
            }
        });

//	    Button extra_help = (Button)findViewById(R.id.extra_help);           
//	    extra_help.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	
//	        	sendEmail("Business Models -> Extra Help", "From business models, user clicked on extra help" );   	
//	            
//	            Intent myIntent = new Intent(BusinessModelsActivity.this, ExtraHelpActivity.class);
//	            BusinessModelsActivity.this.startActivity(myIntent);
//	        }
//	    });	     	    
	    
        final TextView explanation_one = (TextView) findViewById(R.id.explanation_one);
        final TextView explanation_two = (TextView) findViewById(R.id.explanation_two);

        TextView helpful = (TextView) findViewById(R.id.helpful);
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
              Intent myIntent = new Intent(BusinessModelsActivity.this, AddProblemActivity.class);
              BusinessModelsActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//                Intent myIntent = new Intent(BusinessModelsActivity.this, AskQuestionActivity.class);
//                BusinessModelsActivity.this.startActivity(myIntent);
//            }
//        });  		
		
	    
	      final TextView contact = (TextView) findViewById(R.id.contact);

	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }    
    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.layout.menu, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//        	
//        }
//        
//        return false;
//    }
//    
//    private Intent createShareIntent() 
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT, 
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//    
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) 
//    {
//        try
//        {        	
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch (Exception e )
//        {
//        	
//        }
//    }                
}
