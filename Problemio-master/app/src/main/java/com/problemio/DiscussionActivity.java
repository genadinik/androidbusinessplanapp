package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;
import com.problemio.QuestionActivity.UpdateRedId;
import com.problemio.data.DailyDiscussionComment;

import utils.SendEmail;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DiscussionActivity extends BaseListActivity
{
	ArrayAdapter<DailyDiscussionComment> adapter;			
	ArrayList<DailyDiscussionComment> discussion = new ArrayList <DailyDiscussionComment>( );	
	Dialog dialog;
	
	//EditText email;
	//TextView email_ask;
	EditText name;
	TextView name_ask;	
	
	static final String SENDER_ID = "566530471892";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.daily_discussion);
        
        DailyDiscussionComment d = new DailyDiscussionComment ();
        d.setComment( "Please wait while the discussion loads..." );
        
        discussion.add(d);
        adapter = new ArrayAdapter<DailyDiscussionComment>( 
        		this,R.layout.discussion_comments, 
        		discussion);

        setListAdapter(adapter);
      
        ListView lv = getListView();
        lv.setTextFilterEnabled(true);

        // Check if person is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( DiscussionActivity.this);
        final String user_id = prefs.getString( "user_id" , null ); 
        final String discussion_id = prefs.getString( "recent_discussion_id" , null ); 
        final String recent_discussion = prefs.getString( "recent_discussion" , null ); 
        final String first_name = prefs.getString( "first_name" , null );	 	         

        // If the user is not logged in, send them to log in
        if ( user_id == null )
        { 	
	        Intent loginIntent = new Intent( DiscussionActivity.this, LoginActivity.class);
	        DiscussionActivity.this.startActivity(loginIntent);        	
        }    
        
        
//        if ( android.os.Build.VERSION.SDK_INT >= 8 )
//        {
//	        GCMRegistrar.checkDevice(this);	
//	        GCMRegistrar.checkManifest(this);
//	    
//	        final String regId = GCMRegistrar.getRegistrationId(this);
//	        if (regId.equals("")) 
//	        {
//	        	// Automatically registers application on startup. 
//	        	GCMRegistrar.register(getApplicationContext(), SENDER_ID); 
//	        } 
//	        else 
//	        {	     
//	        	// Device is already registered on GCM, check server. 
//	        	if (GCMRegistrar.isRegisteredOnServer(getApplicationContext())) 
//	        	{ 
//	        		// Not sure what to do here :)
//	        	} 
//	        	else 
//	        	{
//			    	if ( user_id != null )
//			    	{	
//				        GCMRegistrar.register(this, SENDER_ID); // google register 
//			    		//GCMRegistrar.setRegisteredOnServer(this, true); //Tell GCM: this device is registered on my server	        
//				    	setRegistrationId ( user_id , regId );
//			    	}
//	        	}
//	        }	        
//        }        
        
        
        
//	    email_ask = (TextView) findViewById(R.id.email_ask);
//	    email = (EditText) findViewById(R.id.email);
	    name_ask = (TextView) findViewById(R.id.name_ask);
	    name = (EditText) findViewById(R.id.name); 
	    
	    
        if ( first_name != null )
        {
    	    //email_ask.setVisibility(View.GONE);
    	    name.setVisibility(View.GONE);
    	    name_ask.setVisibility(View.GONE);        	
    	    //email.setVisibility(View.GONE);
        }	
        else
        {
        	name.setText(first_name);
        }
        
        // Show field for making a comment  
    	final EditText comment = (EditText) findViewById(R.id.discussion_comment);  
    	        
        Button back = (Button)findViewById(R.id.back);
        back.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {      		               
		        Intent loginIntent = new Intent( DiscussionActivity.this, DailyDiscussionsActivity.class);
		        DiscussionActivity.this.startActivity(loginIntent);                  	
     		}
        });        
        
        
        
        

        Button invite_friend_discussion = (Button)findViewById(R.id.invite_friend_discussion);
        invite_friend_discussion.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
		        Intent loginIntent = new Intent( DiscussionActivity.this, InviteFriendsToDiscussionActivity.class);
		        DiscussionActivity.this.startActivity(loginIntent);     		   
     		   
     	   }
        }); 
        
        
        
        
	    //dialog = new Dialog(this);
        Button submit = (Button)findViewById(R.id.submit_comment);   
        submit.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {     		   
     	      String c = comment.getText().toString();
     	      
			  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( DiscussionActivity.this);
		      String saved_name = prefs.getString( "first_name" , null );	 	         
//		      String saved_email = prefs.getString( "email" , null );				  
			  
              if ( saved_name == null )	
              {
    			  saved_name = name.getText().toString(); 	      	  
              }     	      
     	      
//     	      if ( saved_email == null )
//     	      {
//            	  saved_email = email.getText().toString(); 				       	    	  
//     	      }
     	      
     	      
     	      if ( saved_name == null || saved_name.length( ) < 3  )
     	      {
      	         Toast.makeText(getApplicationContext(), "Error: please make sure your name is in the system.", Toast.LENGTH_LONG).show();			     		             	    	  
     	      }
     	      else
              if ( c == null || c.length() == 0 )
              {				    
     	         Toast.makeText(getApplicationContext(), "Please make sure the comment is not empty.", Toast.LENGTH_LONG).show();			     		        
     	         //sendEmail( "Someone tried to add an empty comment" , "This was the comment: " + c + " and problem_id: " + problem_id );
              }
              else
              {
        	      prefs.edit()        
  		        .putString("first_name", saved_name)
  		        .commit();	
            	  
    	      	  sendFeedback( c , user_id , discussion_id , saved_name );   
              }
     	    
     		// To add a new comment, I need these fields:
     	    // problem_id , suggested_solution_id , commenter_id , comment , solution_section
     	   }
        });
        
        
        TextView discussion_message = (TextView) findViewById(R.id.discussion_message);
        discussion_message.setText( "Discuss: " + recent_discussion );
        
	    sendFeedback( discussion_id  );   
    }		
    
    
    
    public void sendFeedback( String problem_id ) 
    {  

        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/get_community_discussion_comments.php",
        		problem_id  };
        

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }                  
    
    public void sendFeedback( String c , String user_id , String problem_id , 
    		  String saved_name )
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/add_community_discussion_comment.php",
        		 c , user_id, problem_id , saved_name };

        AddComment task = new AddComment();
        task.execute(params);        
    }   
    
//    public void setRegistrationId(String user_id , String regId )
//    {
//        String[] params = new String[] { "https://www.problemio.com/problems/update_user_reg_mobile.php", user_id , regId };
//
//        UpdateRedId task = new UpdateRedId();
//        task.execute(params);
//    }
    
    public class UpdateRedId extends AsyncTask<String, Void, String>
    {
		@Override
		protected String doInBackground(String... theParams)
		{
	        String myUrl = theParams[0];
	        final String user_id = theParams[1];
	        final String reg_id = theParams[2];

	        String charset = "UTF-8";
	        String response = null;

			try
			{
		        String query = String.format("user_id=%s&reg_id=%s",
		        	     URLEncoder.encode(user_id, charset) ,
		        	     URLEncoder.encode(reg_id, charset)
		        		);

		        final URL url = new URL( myUrl + "?" + query );

		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		        conn.setDoOutput(true);
		        conn.setRequestMethod("POST");

		        conn.setDoOutput(true);
		        conn.setUseCaches(false);

		        conn.connect();

		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1)
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();
			}
			catch (Exception e)
			{
					e.printStackTrace();
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	        
	        if ( result == null )
	        {
		        // Show the user a message that they did not enter the right login
		        
	        }
	        else
	        if ( result.equals("error_updating_user") )
	        {
	        	
	        }
	        else
	        {		        
		        // TODO:
//		        if ( android.os.Build.VERSION.SDK_INT >= 8 )
//		        {
//		        	GCMRegistrar.setRegisteredOnServer(getApplicationContext(), true);
//		        }

		    }
		}        
    }                
    
    public void onItemClick(AdapterView<?> items, View v, int x, long y)
    {
        //Log.d( "onItemClick: " , "In the onItemClick method of ViewSolutionsActivity" );
    }
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(DiscussionActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		      dialog.setTitle("Getting current comments");

		      TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
		      text.setText("Please wait while comments load... ");
		      dialog.show();
		 }     	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String problem_id = theParams[1];
	        	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        String query = String.format("discussion_id=%s", 
		        	     URLEncoder.encode( problem_id, charset)
		        	     );

		        final URL url = new URL( myUrl + "?" + query );
		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
				connectionError = true;
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{				
			try {
		        dialog.dismiss();
		    } catch (Exception ee) {
		        // nothing
		    }			
			
			if ( connectionError == true )
			{
				Toast.makeText(getApplicationContext(), "Please try again. Possible Internet connection error.", Toast.LENGTH_LONG).show();	 
			}
						
			if ( result == null )
			{					
		        Toast.makeText(getApplicationContext(), "Could not get the discussion. Please try again in a few minutes.", Toast.LENGTH_LONG).show();			     		        
			}
			else
	        if ( result.equals("no_discussion_id") || result.equals("error_getting_comments" ) 
	        	)
	        {	        			        
		        Toast.makeText(getApplicationContext(), "Could not get the discussion. Please try again in a few minutes.", Toast.LENGTH_LONG).show();			     		        
	        }
	        else
	        { 
		        if ( result.length() == 0 || result.trim().equals("no_comments") )
		        {
			        discussion.clear();
			        			    	
			        DailyDiscussionComment message = new DailyDiscussionComment ( );
		            message.setComment("This section is empty.");
		            
		            discussion.add( message );			        
		        }
		        else
		        {
			        try
			        {	
			        	JSONArray obj = new JSONArray(result);
				        			        
			        	if ( obj != null )
			        	{
					        discussion.clear();
			        
			        		for ( int i = 0; i < obj.length(); i++ )
			        		{
			        			JSONObject o = obj.getJSONObject(i);
			    		    	//HashMap<String, String> map = new HashMap<String, String>();
						            
					            String comment = o.getString("comment");
					            String commenter_id = o.getString("user_id");
					            String comment_id = o.getString("discussion_comment_id");
					            String first_name = o.getString("first_name");
					            
					            DailyDiscussionComment d = new DailyDiscussionComment ( );
					            d.setComment(comment);
					            d.setCommentBy(commenter_id);
					            d.setCommentId(comment_id);
					            
					            if ( first_name != null )
					            {					            	
					            	d.setAuthorName( first_name.trim( ) );
					            }
					            else
					            {
					            	d.setAuthorName( "User" );
					            }
					          
					            discussion.add( d );
					        }
			    		} 
					    adapter.notifyDataSetChanged();		        		
			        }			        
			        catch ( Exception e )
			        {
				        e.printStackTrace();
				        
			        }
		        }
		        
		        adapter.notifyDataSetChanged();		        		
		    }
		}        
    }    


    
    
    
    public class AddComment extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(DiscussionActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		      dialog.setTitle("Submitting your comment");

		      TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
		      text.setText("Please wait while your comment is processed... ");
		      dialog.show();
		 }    	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String comment = theParams[1];
	        final String user_id = theParams[2];
	        final String problem_id = theParams[3];
	        final String saved_name = theParams[4];
	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        String query = String.format("comment=%s&user_id=%s&discussion_id=%s&sent_name=%s", 
		        	     URLEncoder.encode( comment, charset), 
		        	     URLEncoder.encode( user_id, charset), 
		        	     URLEncoder.encode( problem_id, charset) , 
		        	     URLEncoder.encode( saved_name, charset)
		        	     );		        
		        
		        final URL url = new URL( myUrl + "?" + query );
		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        InputStream stream = conn.getInputStream();
		        byte[] stream_buffer = new byte[8196];
		        int readCount;
		        StringBuilder stream_builder = new StringBuilder();
		        while ((readCount = stream.read(stream_buffer)) > -1) 
		        {
		            stream_builder.append(new String(stream_buffer, 0, readCount));
		        }

		        response = stream_builder.toString();		
			} 
			catch (Exception e) 
			{
				connectionError = true;
			}
			
			return response;
		}
		
		// TODO: 
//		 protected void onPreExecute(Long result) 
//		 {
//		     dialog = new Dialog(TopicActivity.this);
//		     //your initiation
//		     dialog.show();
//		 }

		@Override
		protected void onPostExecute(String result) 
		{
		    try {
		        dialog.dismiss();
		    } catch (Exception ee) {
		        // nothing
		    }			
			
			if ( connectionError == true )		 		      
			{
				Toast.makeText(getApplicationContext(), "Possible Internet connection error. Please try again. ", Toast.LENGTH_LONG).show();	 
			}
			
	        if ( result == null )
	        {			    
		        Toast.makeText(getApplicationContext(), "We experienced a server error. Please try again, or in a few minutes.", Toast.LENGTH_LONG).show();			     		        

	        }
	        else
	        if ( result.equals("no_problem_id") || result.equals("error_adding_discussion_comment") || 
	        		result.equals("no_discussion_id") || result.equals( "no_comment") || 
	        		result.equals("no_member_id") || result.equals( "error_duplicate_problem")
	        		|| result.equals( "error_getting_discussion_comments")
	        		
	        		)
	        {		        
		        Toast.makeText(getApplicationContext(), "Could not get the current discussion.", Toast.LENGTH_LONG).show();			     		        
		        //sendEmail("Add business comment error response" , "Error response from server. Response: " + result);   
	        }
	        else
	        {		        
		        try
		        {
		        	JSONArray obj = new JSONArray(result);
		        	
		        	if ( obj != null )
		        	{
				        discussion.clear();
		    	        
				        if ( obj.length() == 0 )
		        		{
				        	DailyDiscussionComment message = new DailyDiscussionComment ( );
				            message.setComment("No messages in this discussion.");
				            
				            discussion.add( message );
		        		}
		        		else
		        		{
			        		for ( int i = 0; i < obj.length(); i++ )
			        		{
			        			JSONObject o = obj.getJSONObject(i);
	
					            //String suggested_solution_id = o.getString("suggested_solution_id");
			        			String comment = o.getString("comment");
					            String commenter_id = o.getString("user_id");
					            String comment_id = o.getString("discussion_comment_id");
					            String first_name = o.getString("first_name");
					            
					            DailyDiscussionComment d = new DailyDiscussionComment ( );
					            d.setComment(comment);
					            d.setCommentBy(commenter_id);
					            d.setCommentId(comment_id);	
					            d.setAuthorName(first_name);
					            					            
					            discussion.add( d );
					        }
		        		}
				        
				        adapter.notifyDataSetChanged();	
				        
				        // Now clear the text area of text.
				    	EditText comment = (EditText) findViewById(R.id.discussion_comment);  
				    	comment.setText( "" );  
		        	}
		        }
		        catch ( Exception e )
		        {   
//		        	sendEmail ( "Exception 2 adding business comment" , 
//		        			"Exception: " + e.getMessage() + " , and result was: " + result );
		        	
		        	// End of input at character 0 of
		        }
		    }
		}        
    }        
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
