package com.problemio;

import utils.SendEmail;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

public class HowToGetPressActivity extends BaseActivity
{
	//private ShareActionProvider myShareActionProvider;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.get_press);
 		    
	    	    
        Button prweb = (Button)findViewById(R.id.prweb);          
        prweb.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
            		  Uri.parse("http://www.anrdoezrs.net/click-7252177-10820226"));
              
              startActivity(browserIntent);
            }
        });            
        
        
//        Button premium_button = (Button)findViewById(R.id.premium_button);
//        premium_button.setOnClickListener(new Button.OnClickListener() 
//        {      	
//            public void onClick(View v) 
//            {
//            	sendEmail("Free marketing mistake -> premium marketing","");
//            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//          	    intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//          	            	 
//    	      	 try 
//    	      	 {
//    	      	        startActivity(intent);
//    	      	 } 
//    	      	 catch (ActivityNotFoundException anfe) 
//    	      	 {
//    	               try
//    	               {
//    	              	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//    	              	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//    	              	 startActivity(next_intent);  
//    	               }
//    	               catch ( Exception e)
//    	               {
//    	                   // Now try to redirect them to the web version:
//    	                   Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//    	                   try
//    	                   {
//    	                  	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//    	                  	 startActivity(webintent);
//    	                   }
//    	                   catch ( Exception except )
//    	                   {
    //	
//    	                   }
//    	               }
//    	      	 }           	            	
//          }
//        });	 	     	      
        
        

    	
    	
    	
    	
    	
    	
    	Button give_review = (Button)findViewById(R.id.give_review);   
    	give_review.setOnClickListener(new Button.OnClickListener() 
    	{  
    	    public void onClick(View v) 
    	    {	    	
    	        Intent intent = new Intent(Intent.ACTION_VIEW);
    	    	intent.setData(Uri.parse("market://details?id=com.problemio"));
    	    	startActivity(intent);                
    	    }
    	});

    	
    	Button web_setup_button = (Button)findViewById(R.id.web_setup_button);   
    	web_setup_button.setOnClickListener(new Button.OnClickListener() 
    	{  
    	    public void onClick(View v) 
    	    {	    	
    	        Intent myIntent = new Intent(HowToGetPressActivity.this, WebsiteSetupActivity.class);
    	        HowToGetPressActivity.this.startActivity(myIntent);
    	    }
    	});	

    	
        Button support_app = (Button)findViewById(R.id.support_app); 
        support_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {		        	     
    	        Intent myIntent = new Intent(HowToGetPressActivity.this, GiveBackActivity.class);
    	        HowToGetPressActivity.this.startActivity(myIntent);        
            }
        });
                
        
        
        
    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }
}
