package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.Consts;
import utils.SendEmail;
import utils.Consts.PurchaseState;
import utils.Consts.ResponseCode;

import com.flurry.android.FlurryAgent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class EnterInviteCodeActivity extends BaseActivity
{
	EditText invite_code;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.enter_invite_code);

        // Check if person is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( EnterInviteCodeActivity.this);
        final String user_id = prefs.getString( "user_id" , null ); // First arg is name and second is if not found.
        final String session_name = prefs.getString( "first_name" , null );
        final String session_email = prefs.getString( "email" , null );        
                
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {        	
	        Intent loginIntent = new Intent( EnterInviteCodeActivity.this, LoginActivity.class);
	        EnterInviteCodeActivity.this.startActivity(loginIntent);        	
        }
           
        TextView prompt_to_enter_code = (TextView) findViewById(R.id.prompt_to_enter_code);
	    invite_code = (EditText) findViewById(R.id.invite_code);
    	
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	Button submit_invite_code = (Button)findViewById(R.id.submit_invite_code);
    	submit_invite_code.setOnClickListener(new Button.OnClickListener()
    	{  
    	   public void onClick(View v) 
    	   {
 		      Toast.makeText(getApplicationContext(), "Submitting...", Toast.LENGTH_LONG).show();	 
    		   
              // Get the code.
    	      String invite_code_input = invite_code.getText().toString().trim();

           	 // sendEmail ("Submitting invite code (Problemio)" , "User: " + user_id + " and invite code: " + invite_code_input);
    	      
    	      if ( invite_code_input == null )
    	      {
    	    	  // Show error message
     		      Toast.makeText(getApplicationContext(), "Please enter an invite code.", Toast.LENGTH_LONG).show();	 
    	      }
    	      else
    	      {
    	    	  // Send to check in the database.
    	    	  matchInviteCode ( invite_code_input.trim() , user_id );
    	      }
    	    }
    	});
    }
    


    public void matchInviteCode(String invite_code_input , String user_id) 
    {   
	        String[] params = new String[] 
	        		{ "https://www.problemio.com/problems/enter_invite_code_mobile.php",
	        		invite_code_input , user_id };
	             
	        DownloadWebPageTask task = new DownloadWebPageTask();
	        task.execute(params); 	        	        
        
    }      
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        final String invite_code = theParams[1];
	        final String user_id = theParams[2];
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("invite_code=%s&user_id=%s", 
		        	     URLEncoder.encode( invite_code , charset)  ,
		        	     URLEncoder.encode( user_id , charset) 
		        		);

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
	 		     // sendEmail ( "EnterInviteCode Network Error" , "Error: " + e.getMessage() );			
	 		}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	        
			if ( result == null )
			{
				Toast.makeText(getApplicationContext(), "Could not connect to the server. Please try again in a few minutes.", 
			    		  Toast.LENGTH_LONG).show();
			}
			else
	        if ( result.equals( "no_matching_problem" ) )
	        {		        
//                sendEmail("Add Problem Submitted Error", "From add-problem page, after submitted a business. " +
//                		"There was no problem name, no matching business" );
                
  		        Toast.makeText(getApplicationContext(), "No matching businesses found. Are you sure your code is entered correcty?", 
		    		  Toast.LENGTH_LONG).show();                
	        }
	        else
	        if ( result.equals( "no_invite_code" ) )
	        {		                      
  		        Toast.makeText(getApplicationContext(), "Error submitting the business.  " +
  		        		"We are aware working to fix this.", 
		    		  Toast.LENGTH_LONG).show();                		        		        
	        }
	        else
	        if ( result.equals( "not_logged_in" ) )
	        {		                      
  		        Toast.makeText(getApplicationContext(), "Could not get your account. Please log in first." 
  		        		,  Toast.LENGTH_LONG).show();                		        		        
	        }			
	        else
	        {                
  		        Toast.makeText(getApplicationContext(), "Success, redirecting...  " , 
		    		  Toast.LENGTH_LONG).show();
  		        
  		        //sendEmail ("Invite Code entered correctly (Problemio)","Redirecting to business id: " + result);
		       
		        // 1) First, write to whatever local session file that the person is logged in
		        // - I just really need user id and name and email. And store that.
		        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( EnterInviteCodeActivity.this);
		        prefs.edit().putString("recent_problem_id", result ).commit();
		        
		        // 2) Make an intent to go to the home screen
	            Intent myIntent = new Intent(EnterInviteCodeActivity.this, ProblemActivity.class);
	            EnterInviteCodeActivity.this.startActivity(myIntent);
	        }
		}    
    }    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }     
}
