package com.problemio;

import utils.SendEmail;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

public class WePromoteActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.content_promote);

        Button content_promote_idea_app = (Button)findViewById(R.id.content_promote_idea_app);
        content_promote_idea_app.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=business.ideas"));

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    try {
                        Uri uri = Uri.parse("market://search?q=pname:business.ideas");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    } catch (Exception e) {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.ideas");
                        try {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        } catch (Exception except) {

                        }
                    }
                }
            }
        });

        Button content_promote_fund_app = (Button)findViewById(R.id.content_promote_fund_app);
        content_promote_fund_app.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=make.money"));

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    try {
                        Uri uri = Uri.parse("market://search?q=pname:make.money");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    } catch (Exception e) {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=make.money");
                        try {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        } catch (Exception except) {

                        }
                    }
                }
            }
        });

        



	    Button marketing_app = (Button)findViewById(R.id.content_promote_marketing_app);
 		      marketing_app.setOnClickListener(new Button.OnClickListener() 
 		      {  
 		          public void onClick(View v) 
 		          {	 		              
 		            Intent intent = new Intent(Intent.ACTION_VIEW);
 		        	intent.setData(Uri.parse("market://details?id=com.marketing"));
 		        	            	 
 		        	 try 
 		        	 {
 		        	        startActivity(intent);
 		        	 } 
 		        	 catch (ActivityNotFoundException anfe) 
 		        	 {
 		                 try
 		                 {
 		                	 Uri uri = Uri.parse("market://search?q=pname:com.marketing");
 		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
 		                	 startActivity(next_intent);  
 		                 }
 		                 catch ( Exception e)
 		                 {
 		                     // Now try to redirect them to the web version:
 		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
 		                     try
 		                     {
 		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
 		                    	 startActivity(webintent);
 		                     }
 		                     catch ( Exception except )
 		                     {

 		                     }
 		                 }
 		        	 }           	            	
 		        }
 		      });	 	     	    	    
	    
     	final AlertDialog.Builder t_builder = new AlertDialog.Builder(this);
	    Button follow_on_twitter = (Button)findViewById(R.id.follow_on_twitter);
	    follow_on_twitter.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {          	
            t_builder.setMessage("You will be taken to our Twitter page from which you can follow us, say hello, and ask for retweet any time. ")
            .setCancelable(false)
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                                     
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                  		  Uri.parse("http://www.twitter.com/problemio"));
                    startActivity(browserIntent);
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() 
            {
                public void onClick(DialogInterface dialog, int id) 
                {
                     dialog.cancel();
                }
            });
     AlertDialog alert = t_builder.create();
     alert.show();

          }
      });  	    	    

     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    Button press_like = (Button)findViewById(R.id.press_like);
	    press_like.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {          	
            builder.setMessage("You will be taken to our Facebook page from which you can press the like button. Thank you in advance!")
            .setCancelable(false)
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                                     
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                  		  Uri.parse("https://www.facebook.com/groups/problemio/"));
                    startActivity(browserIntent);
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() 
            {
                public void onClick(DialogInterface dialog, int id) 
                {
                     dialog.cancel();
                }
            });
     AlertDialog alert = builder.create();
     alert.show();

          }
      });
	

	    
	    
	    
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	  see_ads.setOnClickListener(new Button.OnClickListener() 
//      {  
//          public void onClick(View v) 
//          {	
//          	
//          	// Here show instructions.
//		       builder.setMessage("Instructions: a screen with ads will appear. Please consider signing up for one of their interesting free offerings.  To get out of the takeover screen, just press the back button. Try it?")
//	              .setCancelable(false)
//	              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//	                  public void onClick(DialogInterface dialog, int id) {
//	       
//	                      Intent myIntent = new Intent(GiveBackActivity.this, AdsActivity.class);
//	                      GiveBackActivity.this.startActivity(myIntent);
//	                      
//	                  }
//	              })
//	              .setNegativeButton("No", new DialogInterface.OnClickListener() 
//	              {
//	                  public void onClick(DialogInterface dialog, int id) 
//	                  {
//	                       dialog.cancel();
//	                       
//		                      sendEmail( "See ads NO" , ":)");
//	                  }
//	              });
//	       AlertDialog alert = builder.create();
//	       alert.show();		       		         
//          }
//      });	 	     
	    
	    
//      paid_app.setOnClickListener(new Button.OnClickListener() 
//      {  
//          public void onClick(View v) 
//          {	
//          	sendEmail("Give Back To Paid App", "" );   	
//              
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//        	intent.setData(Uri.parse("market://details?id=business.premium"));
//        	            	 
//        	 try 
//        	 {
//        	        startActivity(intent);
//        	 } 
//        	 catch (ActivityNotFoundException anfe) 
//        	 {
//                 try
//                 {
//                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
//                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                	 startActivity(next_intent);  
//                 }
//                 catch ( Exception e)
//                 {
//                     // Now try to redirect them to the web version:
//                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
//                     try
//                     {
//                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                    	 startActivity(webintent);
//                     }
//                     catch ( Exception except )
//                     {
//                         sendEmail("Give Review (2) ERROR", "From advertising page, user clicked on web version of give review and this is the exception: " + except.getMessage() );   	                    	 
//                     }
//                 }
//        	 }           	            	
//        }
//      });	 	     	    











		
        Button additional_products = (Button)findViewById(R.id.additional_products);
        additional_products.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(WePromoteActivity.this, ExtraHelpActivity.class);
                WePromoteActivity.this.startActivity(myIntent);
            }
        });

        Button content_promote_coaching = (Button)findViewById(R.id.content_promote_coaching);
        content_promote_coaching.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(WePromoteActivity.this, AskQuestionActivity.class);
                WePromoteActivity.this.startActivity(myIntent);
            }
        });
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);            	
//    }   
    
    @Override
	public void onStop()
    {
       super.onStop();
       // your code
    }
}
