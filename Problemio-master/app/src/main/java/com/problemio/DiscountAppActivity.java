package com.problemio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.content.ActivityNotFoundException;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 1/17/16.
 */
public class DiscountAppActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.discount_app);


        Button get_app = (Button)findViewById(R.id.get_app);
        get_app.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences(DiscountAppActivity.this);

                String user_id = prefs.getString( "user_id", null ); // First arg is name and second is if not found.
                String first_name = prefs.getString( "first_name" , null );

                //sendEmail ("Dis-->Download" , "User_id: " + user_id + " and name: " + first_name);

                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.okason.udemycoupons"));

            	 try
            	 {
            	        startActivity(intent);
            	 }
            	 catch (ActivityNotFoundException anfe)
            	 {
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.okason.udemycoupons");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.okason.udemycoupons");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }


            }
        });
//
//
//        Button restaurantplan = (Button)findViewById(R.id.restaurantplan);
//        restaurantplan.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                SharedPreferences prefs =
//                        PreferenceManager.getDefaultSharedPreferences( PlanRestaurantActivity.this);
//
//                prefs.edit()
//                        .putString("url_to_watch", "https://www.youtube.com/watch?v=M1Gs3vOGoVg")
//                        .commit();
//
//                Intent myIntent = new Intent(PlanRestaurantActivity.this, VideosActivity.class);
//                PlanRestaurantActivity.this.startActivity(myIntent);
//            }
//        });


//        Button localbook = (Button)findViewById(R.id.localbook);
//        localbook.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                //sendEmail("Problemio Web Setup -> BlueHost", "" );
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Local-Small-Business-Marketing-business/dp/1519556985"));
//                startActivity(browserIntent);
//            }
//        });
//
//        Button businesscours = (Button)findViewById(R.id.businesscours);
//        businesscours.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                //sendEmail("Problemio Web Setup -> BlueHost", "" );
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.udemy.com/how-to-start-a-business-go-from-business-idea-to-a-business/?couponCode=androidapp"));
//                startActivity(browserIntent);
//            }
//        });






//        Button planBusiness = (Button)findViewById(R.id.plan_business);
//        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);
//
//        planBusiness.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
////                Intent myIntent = new Intent(TopMistakesActivity.this, AddProblemActivity.class);
////                TopMistakesActivity.this.startActivity(myIntent);
//            }
//        });

//	    ask_direct_question.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent myIntent = new Intent(TopMistakesActivity.this, AskQuestionActivity.class);
//                TopMistakesActivity.this.startActivity(myIntent);
//            }
//        });



    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
        // your code
    }
}
