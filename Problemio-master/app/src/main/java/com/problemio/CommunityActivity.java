package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utils.Consts;
import utils.SendEmail;
import utils.Consts.PurchaseState;
import utils.Consts.ResponseCode;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.problemio.content.AdvertisingActivity;
import com.problemio.content.BusinessModelsActivity;
import com.problemio.content.InvestorsActivity;
import com.problemio.content.PremiumWebAdvertisingActivity;
import com.problemio.content.ProductStrategyActivity;
import com.problemio.content.StageTacticsActivity;
import com.problemio.content.TargetMarketActivity;
import com.problemio.content.TopMistakesActivity;
import com.problemio.content.UnitEconomicsActivity;
import com.problemio.data.ChatMessage;
import com.problemio.data.DiscussionMessage;
import com.problemio.data.SolutionTopic;

public class CommunityActivity extends BaseActivity //implements ServiceConnection
{	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.community);



        Button fb = (Button)findViewById(R.id.fb);
        fb.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                //

                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/groups/problemio/"));
                startActivity(browserIntent);

            }
        });

        Button twitter = (Button)findViewById(R.id.twitter);
        twitter.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.twitter.com/problemio"));
                startActivity(browserIntent);

            }
        });

        Button newsletter = (Button)findViewById(R.id.newsletter);
        newsletter.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://glowingstart.com/email-subscribe/"));
                startActivity(browserIntent);

            }
        });

        Button youtube = (Button)findViewById(R.id.youtube);
        youtube.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/user/Okudjavavich"));
                startActivity(browserIntent);

            }
        });


//    Button appscourse = (Button)findViewById(R.id.appscourse);
//    appscourse.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//    	   //sendEmail("Problemio Motivation 1 -> buy sub clicked!" , "");
//
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-create-grow-a-mobile-app-iphone-android-business/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button startbusinesscourse = (Button)findViewById(R.id.startbusinesscourse);
//    startbusinesscourse.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-start-a-business-go-from-business-idea-to-a-business/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//    Button bigmarketing = (Button)findViewById(R.id.bigmarketing);
//    bigmarketing.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/marketing-plan-strategy-become-a-great-marketer/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
    
    
//    Button businessplancourse = (Button)findViewById(R.id.businessplancourse);
//    businessplancourse.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//    	   //sendEmail("Problemio Motivation 1 -> buy sub clicked!" , "");
//
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-write-a-business-plan/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });

        Button jobs = (Button)findViewById(R.id.jobs);
        jobs.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(CommunityActivity.this, CoursesActivity.class);
                CommunityActivity.this.startActivity(myIntent);
            }
        });

        Button ipoll = (Button)findViewById(R.id.ipoll);
        ipoll.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(CommunityActivity.this, BooksActivity.class);
                CommunityActivity.this.startActivity(myIntent);
            }
        });


        Button marketing = (Button)findViewById(R.id.marketing);
        marketing.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.marketing"));

                try
                {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException anfe)
                {
                    try
                    {
                        Uri uri = Uri.parse("market://search?q=pname:com.marketing");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    }
                    catch ( Exception e)
                    {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
                        try
                        {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        }
                        catch ( Exception except )
                        {

                        }
                    }
                }
            }
        });

        Button ideas = (Button)findViewById(R.id.ideas);
        ideas.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=business.ideas"));

                try
                {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException anfe)
                {
                    try
                    {
                        Uri uri = Uri.parse("market://search?q=pname:business.ideas");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    }
                    catch ( Exception e)
                    {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.ideas");
                        try
                        {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        }
                        catch ( Exception except )
                        {

                        }
                    }
                }
            }
        });

        Button money_app = (Button)findViewById(R.id.money);
        money_app.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=make.money"));

                try
                {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException anfe)
                {
                    try
                    {
                        Uri uri = Uri.parse("market://search?q=pname:make.money");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    }
                    catch ( Exception e)
                    {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=make.money");
                        try
                        {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        }
                        catch ( Exception except )
                        {

                        }
                    }
                }
            }
        });


//    Button affiliatecourse = (Button)findViewById(R.id.affiliatecourse);
//    affiliatecourse.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/make-money-with-affiliate-marketing-earn-passive-income/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//    Button seo = (Button)findViewById(R.id.seo);
//    seo.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/seo-with-google-other-large-platforms-to-get-great-scale/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button socialmedia = (Button)findViewById(R.id.socialmedia);
//    socialmedia.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/advanced-social-media-marketing-with-practical-strategies/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button realestate = (Button)findViewById(R.id.realestate);
//    realestate.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-start-a-career-in-real-estate/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button publicity = (Button)findViewById(R.id.publicity);
//    publicity.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-get-publicity-and-press-coverage-for-your-business/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//    Button makemoney = (Button)findViewById(R.id.makemoney);
//    makemoney.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-make-money/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button local = (Button)findViewById(R.id.local);
//    local.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//    	   //sendEmail("Problemio Motivation 1 -> buy sub clicked!" , "");
//
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/marketing-how-to-market-and-promote-a-local-small-business/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//    Button marketingintro = (Button)findViewById(R.id.marketingintro);
//    marketingintro.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//    	   //sendEmail("Problemio Motivation 1 -> buy sub clicked!" , "");
//
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/introduction-to-marketing-101-basics-and-fundamentals/"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//
//
//
//
//
//
////fb
////twitter
//
//    Button sell = (Button)findViewById(R.id.sell);
//    sell.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/how-to-sell-any-product-generate-sales/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
    
    
  
    
    
    
    
    
    
    
    
//    Button proj = (Button)findViewById(R.id.proj);
//    proj.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//    	   //sendEmail("Problemio Motivation 1 -> buy sub clicked!" , "");
//
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/software-project-management-for-start-ups/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//
//        }
//    });
//
//    Button events = (Button)findViewById(R.id.events);
//    events.setOnClickListener(new Button.OnClickListener()
//    {
//        public void onClick(View v)
//        {
//               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//             		  Uri.parse("https://www.udemy.com/event-marketing-how-to-create-a-successful-event-series/?couponCode=freeforappusers"));
//
//               startActivity(browserIntent);
//        }
//    });


}    
    

    
    @Override
    public void onDestroy() 
    {
        super.onDestroy();
    }
    
    
    
    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }   
}
