package com.problemio;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ShareActionProvider;

import com.flurry.android.FlurryAgent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import utils.SendEmail;

// (1)

// (2)

// (3) Legl and business insurance AND printing AND logo

// (4)

// (5)


public class ProblemioActivity extends BaseActivity
{
//	private ShareActionProvider myShareActionProvider;
	
	static final String SENDER_ID="566530471892";
	Dialog dialog;
	
	private ShareActionProvider myShareActionProvider;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");        

//        String user_id = prefs.getString( "user_id", null ); // First arg is name and second is if not found.
//        String first_time_cookie = prefs.getString( "first_time_cookie" , null );			    
//        String first_name = prefs.getString( "first_name" , null );			    
//        String email = prefs.getString( "email" , null );	


//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 8 )
//	        {
//		        GCMRegistrar.checkDevice(this);	
//		        GCMRegistrar.checkManifest(this);
//	        
//		        final String regId = GCMRegistrar.getRegistrationId(this);
//		        if (regId.equals("")) 
//		        {
//		        	GCMRegistrar.register(this, SENDER_ID); 
//		        }        
//	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
//        }
        
        // For checking if the person is logged in.        
        first_time_check();
        
        setContentView(R.layout.main);
 
        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( ProblemioActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
	   	    
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button help_app_button = (Button)findViewById(R.id.help_app_button);
        
        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//	            Button share = (Button)findViewById(R.id.share);
//	    	    share.setOnClickListener(new Button.OnClickListener()
//	    	    {
//	    	        public void onClick(View v)
//	    	        {
//	    	        	openOptionsMenu();
//	    	        }
//	    	    });
//	        }
//	        else
//	        {
//	        	// HIDE THE TWO PAGE ELEMENTS
//	            Button share = (Button)findViewById(R.id.share_button);
//	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt);
//
//	            share.setVisibility(View.GONE);
//	            share_prompt.setVisibility(View.GONE);
//	        }
//        }
//        catch ( Exception ee )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");
//        }
        
//        Button book = (Button)findViewById(R.id.book);
//        book.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//              Intent myIntent = new Intent(ProblemioActivity.this, BookActivity.class);
//              ProblemioActivity.this.startActivity(myIntent);
//            }
//        });





        Button bookscourses = (Button)findViewById(R.id.bookscourses);
        bookscourses.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ProblemioActivity.this, ExtraHelpActivity.class);
                ProblemioActivity.this.startActivity(myIntent);
            }
        });


//        Button questions_coacing = (Button)findViewById(R.id.questions_coacing);
//        questions_coacing.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent myIntent = new Intent(ProblemioActivity.this, ExtraHelpActivity.class);
//                ProblemioActivity.this.startActivity(myIntent);
//            }
//        });



        Button myBusinessesButton = (Button)findViewById(R.id.my_problems_button);  
        Button community_button = (Button)findViewById(R.id.community_button);  
        Button learn = (Button)findViewById(R.id.learn); 
        //Button fundraising_button = (Button)findViewById(R.id.fundraising_button);
        Button website_button = (Button)findViewById(R.id.website_button);

        Button examples_button = (Button)findViewById(R.id.examples_button);

        Button timeline_button = (Button)findViewById(R.id.timeline_button);
        //Button joinfb = (Button)findViewById(R.id.joinfb);

        examples_button.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(ProblemioActivity.this, PlanExamplesActivity.class);
                ProblemioActivity.this.startActivity(myIntent);
            }
        });

//        joinfb.setOnClickListener(new Button.OnClickListener()
//        {
//          public void onClick(View v)
//          {
//            Intent myIntent = new Intent(ProblemioActivity.this, SocialActivity.class);
//            ProblemioActivity.this.startActivity(myIntent);
//          }
//        });
        
        community_button.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {            	
            Intent myIntent = new Intent(ProblemioActivity.this, CommunityActivity.class);
            ProblemioActivity.this.startActivity(myIntent);
          }
        }); 
        
//        help_app_button.setOnClickListener(new Button.OnClickListener()
//        {
//          public void onClick(View v)
//          {
//            Intent myIntent = new Intent(ProblemioActivity.this, GiveBackActivity.class);
//            ProblemioActivity.this.startActivity(myIntent);
//          }
//        });
        
        
        
        Button youtube_button = (Button)findViewById(R.id.youtube_button);
        youtube_button.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {            	
            Intent myIntent = new Intent(ProblemioActivity.this, VideoListActivity.class);
            ProblemioActivity.this.startActivity(myIntent);
          }
        });        
        
//        Button dis = (Button)findViewById(R.id.dis);
//        dis.setOnClickListener(new Button.OnClickListener()
//        {
//          public void onClick(View v)
//          {
//
//            Intent myIntent = new Intent(ProblemioActivity.this, DiscountAppActivity.class);
//            ProblemioActivity.this.startActivity(myIntent);
//          }
//        });

     	//final AlertDialog.Builder linkedin_builder = new AlertDialog.Builder(this);
//        Button podcast_button = (Button)findViewById(R.id.podcast_button);
//        podcast_button.setOnClickListener(new Button.OnClickListener()
//        {
//          public void onClick(View v)
//          {
//        	  linkedin_builder.setMessage("Instructions: One of the top business podcast shows (new episode daily) will open in a web view. One warning is that this may not play on all Android devices.")
//	            .setCancelable(false)
//	            .setNegativeButton("No", new DialogInterface.OnClickListener()
//	            {
//	                public void onClick(DialogInterface dialog, int id)
//	                {
//	                     dialog.cancel();
//	                }
//	            })
//	            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//	                public void onClick(DialogInterface dialog, int id) {
//
//	                    Intent myIntent = new Intent(ProblemioActivity.this, PodcastActivity.class);
//	                    ProblemioActivity.this.startActivity(myIntent);
//	                }
//	            })
//;
//	     AlertDialog alert = linkedin_builder.create();
//	     alert.show();
//
//
//
//
//
//          }
//        });

        
        Button motivation_button = (Button)findViewById(R.id.motivation_button); 
        motivation_button.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {            	
            Intent myIntent = new Intent(ProblemioActivity.this, MotivationActivity.class);
            ProblemioActivity.this.startActivity(myIntent);
          }
        });        

        website_button.setOnClickListener(new Button.OnClickListener()
        {  
          public void onClick(View v) 
          {            	
            Intent myIntent = new Intent(ProblemioActivity.this, WebsiteSetupActivity.class);
            ProblemioActivity.this.startActivity(myIntent);
          }
        });

//        fundraising_button_real.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent myIntent = new Intent(ProblemioActivity.this, FundraisingActivity.class);
//                ProblemioActivity.this.startActivity(myIntent);
//            }
//        });


        
//        Button more_articles_button = (Button)findViewById(R.id.more_articles_button);
//        more_articles_button.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//              Intent myIntent = new Intent(ProblemioActivity.this, MoreArticlesActivity.class);
//              ProblemioActivity.this.startActivity(myIntent);
//            }
//        });


        
//        questions_i_asked.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	
//              Intent myIntent = new Intent(ProblemioActivity.this, MyQuestionsActivity.class);
//              ProblemioActivity.this.startActivity(myIntent);
//            }
//        });                





        timeline_button.setOnClickListener(new Button.OnClickListener()
        {  
            public void onClick(View v) 
            {            	
              Intent myIntent = new Intent(ProblemioActivity.this, TimelineActivity.class);
              ProblemioActivity.this.startActivity(myIntent);
            }
        });        
        

      
        
        
        
        if ( user_id != null && !user_id.equals("1") )
        {
        	// That means this person is logged in and just loaded the app.
        	// Lets send a call to the server to update his last login time and tell us who he is.
        	
            //String firstName = prefs.getString( "first_name", null); // First arg is name and second is if not found.
            //String lastName = prefs.getString( "last_name", null); // First arg is name and second is if not found.
            //String email = prefs.getString( "email", null); // First arg is name and second is if not found.

            sendFeedback ( user_id );
        }
        
        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
              Intent myIntent = new Intent(ProblemioActivity.this, AddProblemActivity.class);
              ProblemioActivity.this.startActivity(myIntent);
            }
        });        
        
        myBusinessesButton.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	            	
                Intent myIntent = new Intent(ProblemioActivity.this, MyProblemsActivity.class);
                ProblemioActivity.this.startActivity(myIntent);
            }
        });
    
	    learn.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	                
                Intent myIntent = new Intent(ProblemioActivity.this, LearnActivity.class);
                ProblemioActivity.this.startActivity(myIntent);
            }
        });   	    
        
	    
	    Button settings = (Button)findViewById(R.id.settings); 
	    settings.setOnClickListener(new Button.OnClickListener() 
	    {  
	        public void onClick(View v) 
	        {		        			        	
	            Intent myIntent = new Intent(ProblemioActivity.this, SettingsActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);
	        }
	    });	    
	    
//	    Button home_share = (Button)findViewById(R.id.home_share); 
//	    home_share.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	
//	              //sendEmail("Home ~ Premium", "From home page, user clicked on premium button" );   	
//	            	
//		            Intent myIntent = new Intent(ProblemioActivity.this, ExtraHelpActivity.class);
//		            ProblemioActivity.this.startActivity(myIntent);
//	              
////	              builder.setMessage("Like or Tweet from our home page to share with your friends?")
////	              .setCancelable(false)
////	              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
////	                  public void onClick(DialogInterface dialog, int id) {
////	                       
////	                      sendEmail("Home ~ Yes Share", "From home page, user clicked on share button" );   	        		       
////	                  
////	                      Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
////	                    		  Uri.parse("http://www.problemio.com"));
////	                      startActivity(browserIntent);
////	                  }
////	              })
////	              .setNegativeButton("No", new DialogInterface.OnClickListener() 
////	              {
////	                  public void onClick(DialogInterface dialog, int id) 
////	                  {
////	                       dialog.cancel();
////	                  }
////	              });
////	       AlertDialog alert = builder.create();
////	       alert.show();	        	
//	        	
//	        }
//	    });	    
	  
	    
	    
	    
	    
	    
	    
	    
	    

}
    
    public void first_time_check()
    {	
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( ProblemioActivity.this);
        
        String user_id = prefs.getString( "user_id", null ); // First arg is name and second is if not found.
        String first_time_cookie = prefs.getString( "first_time_cookie" , null );			    
        String first_name = prefs.getString( "first_name" , null );			    
        String email = prefs.getString( "email" , null );
        
        
        //sendEmail ( "In FirstTimeCheck" , "Name: " + first_name + " email: " + email + " and user_id: " + user_id + " and cookie: " + first_time_cookie );
        
	    // About setting cookie.  Check for first time cookie
	    if ( first_time_cookie == null )
	    {	        
	    	// This user is a first-time visitor, 1) Create a cookie for them
	    	first_time_cookie = "1";
	    	
	    	// 2) Set it in the application session.
	    	prefs.edit().putString("first_time_cookie", first_time_cookie ).commit(); 
	    
	    	// Make a remote call to the database to increment downloads number
	    	// AND send me an email that it was a new user.
	    	sendFeedback ( ); 		    	
	    }        
	    else
	    {
	    	// If not first time, get their id.
	    	// If user_id is empty, make them an account.  
	    	// If id not empty, call the update login date and be done.	    	    	
	    	if ( user_id == null )
	    	{
	    		// User id is null so they must have logged out.	    		
//	            sendEmail("Not new, user NULL (3)", "Name: " + first_name + " email: " + 
//	            email + " and user_id: " + user_id + "....redirecting them to login activity.  " );   	
	    		
	            if ( email == null || email.trim().equals(""))
	            {
	            	//sendEmail ("3, email null" , "Doing nothing");
		    	    //Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
		            //ProblemioActivity.this.startActivity(myIntent);
	            }
	            else
	            {	            	
//	            	sendEmail ("User id empty, email NOT null" , "Logging person in. Email is this: " + email);

	            	// Send it to get the user id from the email.
	                logUserIn( email );
	            }
	        }
	    	else
	    	{	    		
		    	// Make a remote call to the database to increment downloads number
		    	sendFeedback ( user_id );	    	    		
	    	}
	    }
       
    	return;
    }
    
    // Subject , body
    public void sendEmail( String subject , String body )
    {
        String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };

        SendEmail task = new SendEmail();
        task.execute(params);            	
    }    
    
    public void sendFeedback( String user_id ) 
    {  
        String[] params = new String[] { "https://www.problemio.com/auth/update_last_login_mobile.php",
        		 user_id };

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }         
    
    public void sendFeedback( ) 
    {  
        String[] params = new String[] { "https://www.problemio.com/problems/increment_downloads.php" };

        DownloadsPageTask task = new DownloadsPageTask();
        task.execute(params);        
    }       

    public void logUserIn( String email ) 
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/auth/log_user_in_from_email_mobile.php" , email };

        LogUserFromEmailTask task = new LogUserFromEmailTask();
        task.execute(params);        
    }    
    
    public class LogUserFromEmailTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String email = theParams[1];
	      
	        String charset = "UTF-8";	        
	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("email=%s", 
		        	     URLEncoder.encode(email, charset)  
		        		);

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");

		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
			        Log.d( "ProblemioActivity" , "Exception:  " + e);
					e.printStackTrace();
			}
			
			return response;
		}
    
		@Override
		protected void onPostExecute(String result) 
		{	     
			  SharedPreferences prefs = 
		        		PreferenceManager.getDefaultSharedPreferences( ProblemioActivity.this);

			  String temp_email = prefs.getString("email", null);
      	
						
        	if ( result == null )
        	{
        		//sendEmail ("Error in problemio" , "Null was returned after logging in user from email.");
        	
	    	    Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);        		
        	}
        	else
            if ( result.equals ( "no_email ") )
            {
	        	//sendEmail ( "Eror in problemio - no email" , "Error getting the email after logging in user from email, redirecting to log in." );
	    	    Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);
            }
        	else
	        if ( result.equals( "error_getting_user") )
	        {	        
	        	//sendEmail ( "Eror in problemio" , "Error getting the user after logging in user from email" );
	        		
	    	    Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);
	        }	        
	        else
	        if ( result.equals( "email_error") )
	        {
	        	//sendEmail ( "Error in problemio" , "Error, no email sent....after logging in user from email");
	        
	    	    Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);
	        }
	        else
	        if ( result.equals( "no_user" ) )
	        {
	        	//sendEmail( "Error in problemio" , "no_user for email: " + temp_email );
	        		
	    	    Intent myIntent = new Intent(ProblemioActivity.this, LoginActivity.class);
	            ProblemioActivity.this.startActivity(myIntent);
	        }
	        else
	        {
	        	// Unwrap the stuff from the JSON string
		        try
		        {
		        	JSONArray obj = new JSONArray(result);
		        	
		        	if ( obj != null )
		        	{		        		
		        		for ( int i = 0; i < obj.length(); i++ )
		        		{
		        			JSONObject o = obj.getJSONObject(i);
		        	
				            String user_id = o.getString("user_id");
				        		
				        		// If ok, put this id into the user's SharedPrefs
					
						        prefs.edit()        
						        .putString("user_id", user_id)
						        .commit();
						        
						        //sendEmail ( "Logged them in" , "With this user id: " + user_id );
		        		}		        		
		        	}			        
		        }
		        catch ( Exception e )
		        {
		        	//sendEmail ( "ProblemioError *" , "error logging in existing user from email.  Email was " + temp_email + ", and Message: " + e.getMessage() );
		        }
	        }
		}    
    }
       
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String user_id = theParams[1];
//	        final String firstName = theParams[2];
//	        final String lastName = theParams[3];
//	        final String email = theParams[4];
	      
	        String charset = "UTF-8";	        
	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("user_id=%s", 
		        	     URLEncoder.encode(user_id, charset)  
		        		);

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");

		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					e.printStackTrace();
			}
			
			return response;
		}
    
		@Override
		protected void onPostExecute(String result) 
		{	        
        	Log.d( "ProblemioActivity" , "On return 1, result: " + result );
			
	        if ( result != null && result.equals( "error_updating") )
	        {	        
	        	Log.d( "ProblemioActivity" , "NOOOT  OKKKK - error updating logged in database" );	
	        }	        
	        else
	        if ( result != null && result.equals( "no_member_id") )
	        {
	        	Log.d( "ProblemioActivity" , "NOOOT  OKKKK - no member id" );	
	        }
		}    
    }

    public class DownloadsPageTask extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(ProblemioActivity.this);

		        dialog.setContentView(R.layout.please_wait);
		        dialog.setTitle("Setting Up App Configurations...");
		        dialog.show();
		 }    	    	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	      
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		     
		        final URL url = new URL( myUrl );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");

		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					e.printStackTrace();
					
					//dialog.dismiss();
			}
			
			return response;
		}
    
		@Override
		protected void onPostExecute(String result_id) 
		{		
	        //sendEmail( "ProblemioActivity New Member" , "Returned result: " + result_id );
			
            try 
            {
                dialog.dismiss();
            } 
            catch (Exception e) 
            {
                // nothing
            }
	        
	        // Need to get the person's new user id that was just returned from the db and store it
	        if ( result_id != null )
	        {	        
	        	try
	        	{
	        		Integer i = Integer.valueOf( result_id );
	        		
	        		// If ok, put this id into the user's SharedPrefs
			        SharedPreferences prefs = 
			        		PreferenceManager.getDefaultSharedPreferences( ProblemioActivity.this);

			        prefs.edit()        
			        .putString("user_id", result_id)
			        .commit();	        			        		
	        	}
	        	catch ( Exception e )
	        	{
	        		//sendEmail ( "Error creating user id " ,  e.toString() ) ;        		
	        	}
	        }		        
	        else
	        {
		        //Log.d( "ProblemioActivity" , "on post exectute - not created the user id :(" );
		        
		        // Email me this problem.
        		//sendEmail ( "Error creating new user" ,  "Here is the result: " + result_id) ;
	        }
		}    
    }		  
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }    
    
//    @Override
//    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    public boolean onCreateOptionsMenu(Menu menu)
//    {
//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.menu.menu_main, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//
//        }
//
//        return false;
//    }
//
//    private Intent createShareIntent()
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT,
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
    
    // Somewhere in the application.
//    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    public void doShare(Intent shareIntent)
//    {
//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch ( Exception e )
//        {
//
//        }
//    }
}