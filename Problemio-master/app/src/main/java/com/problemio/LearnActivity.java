package com.problemio;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.problemio.content.BusinessModelsActivity;
import com.problemio.content.FundraisingActivity;
import com.problemio.content.InvestorsActivity;
import com.problemio.content.ProductStrategyActivity;
import com.problemio.content.StageTacticsActivity;
import com.problemio.content.TargetMarketActivity;
import com.problemio.content.UnitEconomicsActivity;

import utils.SendEmail;

public class LearnActivity extends BaseActivity 
{	
	private ShareActionProvider myShareActionProvider;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.learn);
 
        // Get Shared Preferences.
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( LearnActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String last_name = prefs.getString( "last_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    
	    	    
        Button planBusiness = (Button)findViewById(R.id.plan_business); 
        Button website_setup = (Button)findViewById(R.id.website_setup); 

        website_setup.setOnClickListener(new Button.OnClickListener() 
      {  
          public void onClick(View v) 
          {            	
              Intent myIntent = new Intent(LearnActivity.this, WebsiteSetupActivity.class);
              LearnActivity.this.startActivity(myIntent);
          }
      });
        
        Button pr_button = (Button)findViewById(R.id.pr_button); 
        pr_button.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {            	
              Intent myIntent = new Intent(LearnActivity.this, HowToGetPressActivity.class);
              LearnActivity.this.startActivity(myIntent);
          }
        });
        
        

        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
        try
        {    
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//			  Uri.parse("http://www.problemio.com/business/how-to-plan-a-business.php"));  
//    		startActivity(browserIntent);
        	
	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
	        {
	            Button share = (Button)findViewById(R.id.share_button); 
	    	    share.setOnClickListener(new Button.OnClickListener() 
	    	    {  
	    	        public void onClick(View v) 
	    	        {		        	
	    	        	openOptionsMenu();
	    	        }
	    	    });        
	        }
	        else
	        {
	        	// HIDE THE TWO PAGE ELEMENTS
	            Button share = (Button)findViewById(R.id.share_button); 
	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt); 

	            share.setVisibility(View.GONE);
	            share_prompt.setVisibility(View.GONE);	            
	        }
        }
        catch ( Exception e )
        {
           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");	
        }

        
        
        Button business_plan_examples = (Button)findViewById(R.id.business_plan_examples);  
     	final AlertDialog.Builder premium_builder = new AlertDialog.Builder(this);
        business_plan_examples.setOnClickListener(new Button.OnClickListener() 
	    {  
	 	   public void onClick(View v) 
	 	   { 

          	premium_builder.setMessage("Premium articles are free to unlock. " +
          			"All you have to do is learn about how to give back to the app to help keep it free.")
           .setCancelable(false)
           .setPositiveButton("Learn How To Give Back", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
    
                   Intent myIntent = new Intent(LearnActivity.this, GiveBackActivity.class);
                   LearnActivity.this.startActivity(myIntent);                   
               }
           })
           .setNegativeButton("No", new DialogInterface.OnClickListener() 
           {
               public void onClick(DialogInterface dialog, int id) 
               {
                    dialog.cancel();
                    

               }
           });
 	 	     
		      AlertDialog alert = premium_builder.create();
		      alert.show();		       		         	 	      	 	      
	 	   }
	    });	           		
   		
   		
   		
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//   		business_starting_mistakes.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	 	   public void onClick(View v) 
//	 	   { 
//	 		   // Send me an email that a comment was submitted on a question. 
//	 	      sendEmail("Timeline, Top mistakes Clicked", "Someone clicked top mistakes." );   
//	 	
//	 	     builder.setMessage("We made premium features simple to unlock. " +
//	 	     		"All we ask is your good will in giving back to the app in a show of appreciation since the app is free.")
//             .setCancelable(false)
//             .setPositiveButton("Show Thanks", new DialogInterface.OnClickListener() {
//                 public void onClick(DialogInterface dialog, int id) {
//      
//                     Intent myIntent = new Intent(TimelineActivity.this, GiveBackActivity.class);
//                     TimelineActivity.this.startActivity(myIntent);
//                     
//                     sendEmail( "Show Thanks YES" , ":)");
//                 }
//             })
//             .setNegativeButton("No", new DialogInterface.OnClickListener() 
//             {
//                 public void onClick(DialogInterface dialog, int id) 
//                 {
//                      dialog.cancel();
//                      
//	              }
//          	 });
//       		   AlertDialog alert = builder.create();
//               alert.show();	    
//	 	   }
	   // });	   		
        

        
        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
              Intent myIntent = new Intent(LearnActivity.this, AddProblemActivity.class);
              LearnActivity.this.startActivity(myIntent);
            }
        });
	    
        Button unit_economics_button = (Button)findViewById(R.id.unit_economics_button);   
        unit_economics_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
                Intent myIntent = new Intent(LearnActivity.this, UnitEconomicsActivity.class);
                LearnActivity.this.startActivity(myIntent);
            }
        });
	    
        Button product_strategy_button = (Button)findViewById(R.id.product_strategy_button);   
        product_strategy_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(LearnActivity.this, ProductStrategyActivity.class);
                LearnActivity.this.startActivity(myIntent);
            }
        });
    
        
        Button business_models_button = (Button)findViewById(R.id.business_models_button);   
        business_models_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(LearnActivity.this, BusinessModelsActivity.class);
                LearnActivity.this.startActivity(myIntent);
            }
        });        
   
        
//        Button fundraising_app = (Button)findViewById(R.id.fundraising_app);   
//		fundraising_app.setOnClickListener(new Button.OnClickListener() 
//        { 
//            public void onClick(View v) 
//            {            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.fundraising"));
//            	//startActivity(intent);
//            	            	 
//            	 try 
//            	 {
//            	        startActivity(intent);
//            	 } 
//            	 catch (ActivityNotFoundException anfe) 
//            	 {            		
//                     try
//                     {
//                    	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);  
//                     }
//                     catch ( Exception e)
//                     {
//                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//
//                         }
//                     }
//            	 }           
//            }
//        });          
        
        Button investors_button = (Button)findViewById(R.id.investors_button);   
        investors_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(LearnActivity.this, InvestorsActivity.class);
                LearnActivity.this.startActivity(myIntent);
            }
        });  
        
        Button fundraising_button = (Button)findViewById(R.id.fundraising_button);   
        fundraising_button.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(LearnActivity.this, FundraisingActivity.class);
                LearnActivity.this.startActivity(myIntent);
            }
        });          
        
  
        
        
        
        
    Button target_users_button = (Button)findViewById(R.id.target_users_button);   
    target_users_button.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {        	
            Intent myIntent = new Intent(LearnActivity.this, TargetMarketActivity.class);
            LearnActivity.this.startActivity(myIntent);
        }
    });
    
    Button company_stage_button = (Button)findViewById(R.id.company_stage_button);   
    company_stage_button.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {        	
            Intent myIntent = new Intent(LearnActivity.this, StageTacticsActivity.class);
            LearnActivity.this.startActivity(myIntent);
        }
    });        
    
    
    
    Button marketing_button = (Button)findViewById(R.id.marketing_button);   
    marketing_button.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {  	        	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                         Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                 Uri.parse("https://www.udemy.com/course/marketing-plan-strategy-become-a-great-marketer/?referralCode=12AAD72F0DAA6471084D"));

                         boolean isChromeInstalled = isPackageInstalled("com.android.chrome", LearnActivity.this);
                         if (isChromeInstalled) {
                             browserIntent.setPackage("com.android.chrome");
                             startActivity(browserIntent);
                         }else{
                             Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                             startActivity(chooserIntent);
                         }

                         startActivity(browserIntent);

//                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//
//                         }
                     }
            	 }           
        }
    });  
    
//    Button complete_web_marketing_button = (Button)findViewById(R.id.complete_web_marketing_button);   
//    complete_web_marketing_button.setOnClickListener(new Button.OnClickListener() 
//    {  
//        public void onClick(View v) 
//        {
//            sendEmail("From Learn --> Super Strategies", "From learn page, user clicked on super marketing strategies" );   	
//        	
////	 	     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
////	            		LearnActivity.this);
////	 	     
////	 	     String super_marketing = prefs.getString( "super_marketing" , null );
////	 	    
////	 	     // If already bought this item, send them to the page.
////	 	     if ( super_marketing != null )
////	 	     {
////	 	    	 Intent myIntent = new Intent(LearnActivity.this, PremiumWebAdvertisingActivity.class);
////		         LearnActivity.this.startActivity(myIntent);	
////	 	     }
////	 	     else
////	 	     {
////		 	     if(mBillingService.checkBillingSupported(Consts.ITEM_TYPE_INAPP))
////		         { 
////					  try
////					  {   
//////						  if (mBillingService.requestPurchase("android.test.purchased", 
//////								  Consts.ITEM_TYPE_INAPP ,  null)) 
////						  
////						  if (mBillingService.requestPurchase(issueProductIdWebMarketing , 
////								  Consts.ITEM_TYPE_INAPP ,  null)) 
////						  {
////					          sendEmail("Super marketing After Billing", "Seemed to have worked fine" );   					          
////				          } 
////						  else 
////						  {
////				                //showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
////						        sendEmail("Super marketing NOT ok", "Broke somewhere along the way :)" );   	
////				          }
////					  }
////					  catch ( Exception e )
////					  {
////						  String error_message = e.getMessage();  
////						  sendEmail( "Excepion in super marketing" , "Here is the exception: " + error_message );
////					  }			         
////		         } 
////		         else 
////		         {
////		        	  // If billing is not supported, email me that, and give them the item for free
////		        	 sendEmail("Billing Not supported", "Giving top items article for free." );    
////           
////			          // Send them to the screen of the article.
////			          Intent myIntent = new Intent(LearnActivity.this, PremiumWebAdvertisingActivity.class);
////			          LearnActivity.this.startActivity(myIntent);	
////		          }		 	    	 
////	 	     }            
//           
//            
//            Intent myIntent = new Intent(LearnActivity.this, PremiumWebAdvertisingActivity.class);
//            LearnActivity.this.startActivity(myIntent);
//        }
//    });     
    
        
    
    Button give_review = (Button)findViewById(R.id.give_review);   
    give_review.setOnClickListener(new Button.OnClickListener() 
    {  
        public void onClick(View v) 
        {        	
            Intent intent = new Intent(Intent.ACTION_VIEW);
        	intent.setData(Uri.parse("market://details?id=com.problemio"));
        	startActivity(intent);                
        }
    });    
    
}    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
//
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }   
    
    @Override
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        try
        {        	
	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
	        {
		        getMenuInflater().inflate(R.menu.menu_main, menu);
		        MenuItem item = menu.findItem(R.id.menu_item_share);
		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
		        myShareActionProvider.setShareHistoryFileName(
		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
		        myShareActionProvider.setShareIntent(createShareIntent());
		        return true;
	        }
        }
        catch ( Exception e )
        {
        	
        }
        
        return false;
    }
    
    private Intent createShareIntent() 
    {
           Intent shareIntent = new Intent(Intent.ACTION_SEND);
           shareIntent.setType("text/plain");
           shareIntent.putExtra(Intent.EXTRA_TEXT, 
             "I am using mobile apps for starting a business from http://www.problemio.com");
           return shareIntent;
    }
    
    // Somewhere in the application.
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void doShare(Intent shareIntent) 
    {
        try
        {        	
	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
	        {
		        // When you want to share set the share intent.
		        myShareActionProvider.setShareIntent(shareIntent);
	        }
        }
        catch ( Exception e )
        {
        	
        }
    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
