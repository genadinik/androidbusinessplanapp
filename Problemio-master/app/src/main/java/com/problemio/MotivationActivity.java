package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.SendEmail;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gcm.GCMRegistrar;
import com.problemio.content.HelpInstructionsActivity;

public class MotivationActivity extends BaseActivity
{	
//	private RadioGroup radioGroup;
//	RadioButton daily;
//	RadioButton weekly;	
//	RadioButton monthly;
//	RadioButton never;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");        

        setContentView(R.layout.motivation);
 
        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( MotivationActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
//        String first_name = prefs.getString( "first_name" , null );			    
//        String email = prefs.getString( "email" , null );	
        String r = prefs.getString( "reminder" , null );
        String saved_todo = prefs.getString( "todo" , null );
        
        
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {
        	  Toast.makeText(getApplicationContext(), "Please log in.", Toast.LENGTH_LONG).show();	            	              
            
	        Intent loginIntent = new Intent( MotivationActivity.this, LoginActivity.class);
	        MotivationActivity.this.startActivity(loginIntent);        	
        }        
        
    	final EditText todo_text = (EditText) findViewById(R.id.to_do_text);  

	    
//    	radioGroup = (RadioGroup) findViewById(R.id.radioButtons);
//    	
//    	daily = (RadioButton)findViewById(R.id.daily);
//    	weekly = (RadioButton)findViewById(R.id.weekly);
//    	monthly = (RadioButton)findViewById(R.id.monthly);
//    	never = (RadioButton)findViewById(R.id.never);
    	
    	
//    	if ( r != null )
//    	{
//    		if ( r.equals( "1") )
//    		   daily.setChecked(true);
//    		if ( r.equals( "2") )
//     		   weekly.setChecked(true); 
//    		if ( r.equals( "3") )
//    		{
//    			sendEmail("3" , "??");
//     		   monthly.setChecked(true);
//    		}
//    		if ( r.equals( "4") )
//     		   never.setChecked(true);    		
//    	}
    	
    	if ( saved_todo != null )
    	{
    		todo_text.setText(saved_todo); 
    	}
    	
//        Button motivation_promote = (Button)findViewById(R.id.motivation_promote);
//        motivation_promote.setOnClickListener(new Button.OnClickListener()
//	    {
//	 	   public void onClick(View v)
//	 	   {
//	 		   // Send me an email that a comment was submitted on a question.
//	 	      //sendEmail("My Questions -> Add Question", "Someone clicked on add-question from my questions.  User id: " + user_id  );
//
//              Intent myIntent = new Intent(MotivationActivity.this, WePromoteActivity.class);
//              MotivationActivity.this.startActivity(myIntent);
//	 	   }
//	    });
    	
    	
        Button save_todo_button = (Button)findViewById(R.id.save_todo_button);
        save_todo_button.setOnClickListener(new Button.OnClickListener()
    	{  
    	   public void onClick(View v) 
    	   {
		      Toast.makeText(getApplicationContext(), "Saving to-do list. Please wait...", Toast.LENGTH_LONG).show();	

    	      String text  = todo_text.getText().toString().trim();
    	      
    	      // Put the persons SHarepPrefs email in there.
    	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
    	    		  MotivationActivity.this);
    		  String user_id = prefs.getString( "user_id" , null );		
    		  String first_name = prefs.getString( "first_name" , null );		    		  
    		  String email = prefs.getString( "email" , null );		
    		  
//    	      daily = (RadioButton)findViewById(R.id.daily);
//    	      weekly = (RadioButton)findViewById(R.id.weekly);
//    	      monthly = (RadioButton)findViewById(R.id.monthly);
//    	      never = (RadioButton)findViewById(R.id.never);
    	      
    	      boolean continueForward = true;
    	      String reminder = "4";

    	      
//    	      if( daily.isChecked() )
//    	      {
//    	    	  reminder = "1";    	    	  
//    		      //Toast.makeText(getApplicationContext(), "Please choose whether you want help or privacy.", Toast.LENGTH_LONG).show();	
//    	      }  
//    	      else
//    	      if( weekly.isChecked() ) 	  
//    	      {
//    	    	  reminder = "2";
//    	      }
//    	      else
//    	      if( monthly.isChecked() ) 
//    	      {
//    	    	  reminder = "3";
//    	      }
//    	      else
//    	      if( never.isChecked() ) 
//    	      {
//    	    	  reminder = "4";
//    	      }    	    	  
//    	      else
//    	      {
//    	    	  continueForward = false;
//    	    	  
//    		      Toast.makeText(getApplicationContext(), "Error: Please select how often you want to be reminded.", Toast.LENGTH_LONG).show();	
//    	      }
    	      
    	        
    	      if ( text == null || text.length() < 1 )
    	      {
    	    	  //sendEmail ( "Add Biz Validation (Description null) Error" , "User: " + user_id + " With name: " + session_name );
    	    	  
    		      Toast.makeText(getApplicationContext(), "The todo section should not be empty. Please try again.", 
    		    		  Toast.LENGTH_LONG).show();	
    		      
    		      continueForward = false;
    	      }

    	       	      
    	      if ( continueForward == true )
    	      {    	  
        	      prefs.edit()        
  		           .putString("todo", text )
  		           .putString("reminder",  reminder )
  		           .commit();	 
    	    	  
    	    	  //setUserReminder( user_id , reminder );    	    	  
    	      }
    	    }
    	});        
    }
    
    public void setUserReminder( String user_id , String reminder_schedule ) 
    {          
        // Now that I have these items lets put them into the database
        
	
	        String[] params = new String[] 
	        		{ "http://www.problemio.com/problems/set_reminder_mobile.php?platform=android_probemio&", 
	        		user_id , reminder_schedule };
	             
	        DownloadWebPageTask task = new DownloadWebPageTask();
	        task.execute(params); 	        	        
    }    
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		private boolean connectionError = false;
		 
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        final String user_id = theParams[1];
	        final String schedule = theParams[2];
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("user_id=%s&schedule=%s", 
		        	     URLEncoder.encode(user_id, charset) ,
		        	     URLEncoder.encode(schedule + "", charset)  
		        		);

		        final URL url = new URL( myUrl + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					//.printStackTrace();
					// sendEmail("Exception adding business" , "Exception: " + e.getMessage());
					connectionError = true;					
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	   
			if ( connectionError == true )
			{
		        Toast.makeText(getApplicationContext(), "Error: Internet conection problem. Please try again and make sure your phone is connected to the Internet." , Toast.LENGTH_LONG).show();
			}
			else
	        if ( result != null && result.equals( "error_getting_reminder" ) || result.equals( "error_updating_reminder" ) || result.equals( "error_updating_reminder" ) )
	        {		        
//                sendEmail("Add Problem Submitted Error", "From add-problem page, after submitted a business. " +
//                		"There was no problem name" );
//                
//  		        Toast.makeText(getApplicationContext(), "Error submitting the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
//		    		  Toast.LENGTH_LONG).show();                
	        }
	        else
	        {                
  		        Toast.makeText(getApplicationContext(), "Successfully saved the todo list." , 
		    		  Toast.LENGTH_LONG).show();                		        		        		        
		       
		        // 1) First, write to whatever local session file that the person is logged in
		        // - I just really need user id and name and email. And store that.
		        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( MotivationActivity.this);
		        boolean iscommited = prefs.edit().putString("recent_problem_id", result ).commit();
	        }
		}    
    }    

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
