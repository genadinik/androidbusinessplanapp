package com.problemio;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//import com.flurry.android.FlurryAgent;
import com.problemio.content.MarketIdeaValidationActivity;
import com.problemio.content.PitchBusinessActivity;
import com.problemio.content.StageTacticsActivity;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 1/12/16.
 */
public class PreventMistakesActivity extends BaseActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.content_preventmistakes);

        //        Button timeline_marketing_app = (Button)findViewById(R.id.timeline_marketing_app);
//        timeline_marketing_app.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                //sendEmail("timeline --> marketing" , "");
//
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//
//                try
//                {
//                    startActivity(intent);
//                }
//                catch (ActivityNotFoundException anfe)
//                {
//                    try
//                    {
//                        Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(next_intent);
//                    }
//                    catch ( Exception e)
//                    {
//                        // Now try to redirect them to the web version:
//                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//                        try
//                        {
//                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                            startActivity(webintent);
//                        }
//                        catch ( Exception except )
//                        {
//
//                        }
//                    }
//                }
//            }
//        });
//
//
//        Button free_business_idea_app = (Button)findViewById(R.id.free_business_idea_app);
//        free_business_idea_app.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse("market://details?id=business.ideas"));
//
//                try
//                {
//                    startActivity(intent);
//                }
//                catch (ActivityNotFoundException anfe)
//                {
//                    try
//                    {
//                        Uri uri = Uri.parse("market://search?q=pname:business.ideas");
//                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(next_intent);
//                    }
//                    catch ( Exception e)
//                    {
//                        // Now try to redirect them to the web version:
//                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.ideas");
//                        try
//                        {
//                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                            startActivity(webintent);
//                        }
//                        catch ( Exception except )
//                        {
//
//                        }
//                    }
//                }
//            }
//        });
//
//
//        Button business_idea_app = (Button)findViewById(R.id.business_idea_app);
//        business_idea_app.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse("market://details?id=com.businessideas"));
//
//                try
//                {
//                    startActivity(intent);
//                }
//                catch (ActivityNotFoundException anfe)
//                {
//                    try
//                    {
//                        Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
//                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(next_intent);
//                    }
//                    catch ( Exception e)
//                    {
//                        // Now try to redirect them to the web version:
//                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
//                        try
//                        {
//                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                            startActivity(webintent);
//                        }
//                        catch ( Exception except )
//                        {
//
//                        }
//                    }
//                }
//            }
//        });



//	    Button fundraising = (Button)findViewById(R.id.fundraising);
//
//
//
//		    fundraising.setOnClickListener(new Button.OnClickListener()
//		      {
//		          public void onClick(View v)
//		          {
//		            Intent intent = new Intent(Intent.ACTION_VIEW);
//		        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
//
//		        	 try
//		        	 {
//		        	        startActivity(intent);
//		        	 }
//		        	 catch (ActivityNotFoundException anfe)
//		        	 {
//		                 try
//		                 {
//		                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
//		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//		                	 startActivity(next_intent);
//		                 }
//		                 catch ( Exception e)
//		                 {
//		                     // Now try to redirect them to the web version:
//		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
//		                     try
//		                     {
//		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//		                    	 startActivity(webintent);
//		                     }
//		                     catch ( Exception except )
//		                     {
//
//		                     }
//		                 }
//		        	 }
//		        }
//		      });


//        Button website = (Button)findViewById(R.id.website);
//        Button pitch_business = (Button)findViewById(R.id.pitch_business);
        Button courses = (Button)findViewById(R.id.courses);
        Button books = (Button)findViewById(R.id.books);



        Button questions_button = (Button)findViewById(R.id.questions_button);
        questions_button.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {

                Intent myIntent = new Intent(PreventMistakesActivity.this, MyQuestionsActivity.class);
                PreventMistakesActivity.this.startActivity(myIntent);
            }
        });



        books.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(PreventMistakesActivity.this, BooksActivity.class);
                PreventMistakesActivity.this.startActivity(myIntent);
            }
        });

        courses.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(PreventMistakesActivity.this, CoursesActivity.class);
                PreventMistakesActivity.this.startActivity(myIntent);
            }
        });

//        button_validate_idea_cheaply.setOnClickListener(new Button.OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                Intent myIntent = new Intent(TimelineActivity.this, MarketIdeaValidationActivity.class);
//                TimelineActivity.this.startActivity(myIntent);
//            }
//        });

//	  extra_help_button.setOnClickListener(new Button.OnClickListener()
//	  {
//	      public void onClick(View v)
//	      {
//	        Intent myIntent = new Intent(TimelineActivity.this, ExtraHelpActivity.class);
//	        TimelineActivity.this.startActivity(myIntent);
//	      }
//	  });








        Button give_review = (Button)findViewById(R.id.give_review);
        give_review.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.problemio"));
                //startActivity(intent);

                try
                {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException anfe)
                {
                    try
                    {
                        Uri uri = Uri.parse("market://search?q=pname:com.problemio");
                        Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(next_intent);
                    }
                    catch ( Exception e)
                    {
                        // Now try to redirect them to the web version:
                        Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                        try
                        {
                            Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                            startActivity(webintent);
                        }
                        catch ( Exception except )
                        {

                        }
                    }
                }
            }
        });
    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        //FlurryAgent.onEndSession(this);
        // your code
    }
}
