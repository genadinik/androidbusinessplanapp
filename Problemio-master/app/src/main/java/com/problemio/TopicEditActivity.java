package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.SendEmail;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.flurry.android.FlurryAgent;
import com.problemio.TopicActivity.AddComment;
import com.problemio.TopicActivity.DownloadWebPageTask;
import com.problemio.content.AdvertisingActivity;
import com.problemio.content.BusinessModelsActivity;
import com.problemio.content.InvestorsActivity;
import com.problemio.content.ProductStrategyActivity;
import com.problemio.content.StageTacticsActivity;
import com.problemio.content.TargetMarketActivity;
import com.problemio.content.UnitEconomicsActivity;
import com.problemio.data.DiscussionMessage;

public class TopicEditActivity extends BaseActivity
{
	Dialog dialog;
	Dialog submit_dialog;
	
	EditText comment;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.topic_edit);
                
        // Check if person is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( TopicEditActivity.this);
        final String user_id = prefs.getString( "user_id" , null ); 
        final String problem_id = prefs.getString( "recent_problem_id" , null ); 
        final String recent_topic_id = prefs.getString( "recent_topic_id" , null ); 
        final String recent_topic_name = prefs.getString( "recent_topic_name" , null ); 
        final String recent_topic_message_id = prefs.getString( "recent_topic_message_id" , null ); 

        //TextView page_explanation = (TextView) findViewById(R.id.page_explanation);
        
        // Show field for making a comment  
    	comment = (EditText) findViewById(R.id.discussion_comment);  
        
    	
    	if ( recent_topic_message_id != null )
    	{
    		getMessage ( recent_topic_message_id );
    	}
    	else
    	{
    		//sendEmail("TopicEditActivity Error" , "recent_topic_message_id: " + recent_topic_message_id + " and recent problem id: " + problem_id);
    	}
        
        
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        
        // If the user is not logged in, send them to log in
//        if ( user_id == null )
//        { 	
//	        Intent loginIntent = new Intent( TopicActivity.this, LoginActivity.class);
//	        TopicActivity.this.startActivity(loginIntent);        	
//        }        
        

        Button back = (Button)findViewById(R.id.back);
        back.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {      		               
		        Intent loginIntent = new Intent( TopicEditActivity.this, TopicActivity.class);
		        TopicEditActivity.this.startActivity(loginIntent);                  	     	    
     		}
        });
    	
        Button delete_message = (Button)findViewById(R.id.delete_message);
        delete_message.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {      		               
     		   // First show dialog 
     		  builder.setMessage("Are you sure you want to delete?")
              .setCancelable(false)
              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int id) 
                  {    
                	  if ( recent_topic_message_id != null )
                	  {
                		   deleteMessage( recent_topic_message_id , user_id ); 
                	  }
                	  else
                	  {
                		  // Show Error Message
                	  }
                  }
              })
              .setNegativeButton("No", new DialogInterface.OnClickListener() 
              {
                  public void onClick(DialogInterface dialog, int id) 
                  {
                       //dialog.cancel();
                  }
              });
       
     		  AlertDialog alert = builder.create();
              alert.show();
     		}
        }); 
                
        
        
        
	    submit_dialog = new Dialog(this);
	    
        Button submit = (Button)findViewById(R.id.submit_comment);   
        submit.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {   
     	      String c = comment.getText().toString();   
            
              if ( c == null || c.length() == 0 )
              {	    
     	         Toast.makeText(getApplicationContext(), "Please make sure the comment is not empty.", Toast.LENGTH_LONG).show();			     		        
              }
              else
              {    	    
    	      	  editMessage( c , user_id , recent_topic_message_id );   
              }
     	    
     		// To add a new comment, I need these fields:
     	    // problem_id , suggested_solution_id , commenter_id , comment , solution_section
     	   }
        });
        
        // Go to the database and display a list of problems, all clickable.
	    //sendFeedback( problem_id , recent_topic_id );   
    }		
    
    
    
    public void getMessage( String recent_topic_message_id ) 
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/get_topic_message_mobile.php",
        		recent_topic_message_id  };
        
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }                  
    
    public void deleteMessage( String recent_topic_message_id , String user_id )
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/delete_topic_comment_mobile.php",
        		 recent_topic_message_id , user_id  };

        DeleteComment task = new DeleteComment();
        task.execute(params);        
    }   
    

    
    
    public void editMessage( String c , String user_id , String recent_topic_message_id )
    {
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/edit_topic_comment_mobile.php",
        		 c , user_id , recent_topic_message_id };

        EditComment task = new EditComment();
        task.execute(params);     	
    }
    
    
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(TopicEditActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		      dialog.setTitle("Loading The Comment");

		      TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
		      text.setText("Please wait while the comment loads... ");
		      dialog.show();
		 }    	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String recent_topic_message_id = theParams[1];
	        	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        String query = String.format("recent_topic_message_id=%s", 
		        	     URLEncoder.encode( recent_topic_message_id, charset) );
		        
		        final URL url = new URL( myUrl + "?" + query );
		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{				
				connectionError = true;
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	  
			if ( connectionError == true )
			{
				Toast.makeText(getApplicationContext(), "Possible Internet connection error.", Toast.LENGTH_LONG).show();	 				
			}

		    try {
		        dialog.dismiss();
		    } catch (Exception e) {
		        // nothing
		    }			
			
			if ( result == null )
			{
				Toast.makeText(getApplicationContext(), "The server encountered an error. Please try again in a few minutes", Toast.LENGTH_LONG).show();			     		        
			       	
				//sendEmail("Toped Edit Result Null" , "result was returned as null in TopicEditActivity");
			}
			else
	        if ( result.equals("no_topic_message") )
	        {
				//sendEmail("Toped Edit Result No Message" , "Could not get the message to edit in TopicEditActivity");
		        
		        Toast.makeText(getApplicationContext(), "Could not get the message. Please let us know about this problem.", Toast.LENGTH_LONG).show();			     		        
	        }
	        else
	        {   
			    try
			    {
		        	JSONArray obj = new JSONArray(result);
			    	
			        if ( obj != null )
			        {
		        		for ( int i = 0; i < obj.length(); i++ )
		        		{
		        			JSONObject o = obj.getJSONObject(i);

				            String db_comment = o.getString("comment");
				        	comment.setText(db_comment);
				        }	
		        	}					        
			    }			        
			    catch ( Exception e )
			    {
				       //sendEmail ( "TopicEditError" , "Error: " + e.getMessage() + " and result: " + result );
			    }
	        }  // End of else
		}    
    }    


    
    
    
    public class DeleteComment extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String recent_topic_message_id = theParams[1];
	        final String user_id = theParams[2];
	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        String query = String.format("recent_topic_message_id=%s&user_id=%s", 
		        	     URLEncoder.encode( recent_topic_message_id, charset) ,
		        	     URLEncoder.encode( user_id, charset)
		        	     );		        
		        
		        final URL url = new URL( myUrl + "?" + query );
		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        InputStream stream = conn.getInputStream();
		        byte[] stream_buffer = new byte[8196];
		        int readCount;
		        StringBuilder stream_builder = new StringBuilder();
		        while ((readCount = stream.read(stream_buffer)) > -1) 
		        {
		            stream_builder.append(new String(stream_buffer, 0, readCount));
		        }

		        response = stream_builder.toString();		
			} 
			catch (Exception e) 
			{
				//sendEmail("Exception deleting business comment" , "Exception: " + e.getMessage());
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{
	        if ( result == null )
	        {
		        Toast.makeText(getApplicationContext(), "We experienced a server error. Please try again, or in a few minutes.", Toast.LENGTH_LONG).show();			     		        

		        //sendEmail("edit comment null response" , "Null response from server. Need to investigate.");
	        }
	        else
	        if ( result.equals("update_error") || result.equals( "no_topic_message") 
	        		|| result.equals("no_recent_topic_message_id") || result.equals("no_user_id") || result.equals("no_comment") )
	        {   
		        Toast.makeText(getApplicationContext(), "Could not get the current discussion. Please try again in a few minutes.", Toast.LENGTH_LONG).show();			     		        
		        //sendEmail("Edit topic comment error response" , "Error response from server. Response: " + result);
		    }
	        else
	        if ( result.equals("user_ids_dont_match"))
	        {
		        // Show the user a message that they did not enter the right login
		        
		        Toast.makeText(getApplicationContext(), "Could not confirm the author of the message. Please let us know about this", Toast.LENGTH_LONG).show();			     		        
		       // sendEmail("Edit topic comment error response" , "Error that user_id not matching author id");
	        }	
	        if ( result.equals("200") )
	        {
		        Toast.makeText(getApplicationContext(), "Success deleting!", Toast.LENGTH_LONG).show();			     		        	        
		        
	        	// Now redirect back to topic.
		        Intent loginIntent = new Intent( TopicEditActivity.this, TopicActivity.class);
		        TopicEditActivity.this.startActivity(loginIntent);	        
		    }
	        else
	        {
	        	//sendEmail ("Topic Edit ?? ", "On delete, it went to the last else...why? Result: " + result);
	        }
		}        
    }        
    
    
    
    
    
    
    
    public class EditComment extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(TopicEditActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		      dialog.setTitle("Submitting Your Edit");

		      dialog.show();
		 }    	
    	    	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String comment = theParams[1];
	        final String user_id = theParams[2];
	        final String recent_topic_message_id = theParams[3];
	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        String query = String.format("recent_topic_message_id=%s&comment=%s&user_id=%s", 
		        	     URLEncoder.encode( recent_topic_message_id, charset) ,
		        	     URLEncoder.encode( comment, charset) ,
		        	     URLEncoder.encode( user_id, charset)
		        		);
		        
		        final URL url = new URL( myUrl + "?" + query );
		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{				  
				connectionError = true;				
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	  
			try {
		        submit_dialog.dismiss();
		    } catch (Exception ee) {
		        // nothing
		    }

			if ( connectionError == true )
			{
				Toast.makeText(getApplicationContext(), "Please try again. Possible Internet connection error.", Toast.LENGTH_LONG).show();	 	
			}
			
			if ( result == null )
			{				
				Toast.makeText(getApplicationContext(), "The server encountered an error. Please try again in a few minutes", Toast.LENGTH_LONG).show();			     		        
			       	
				//sendEmail("Toped Edit Result Null" , "result was returned as null in TopicEditActivity");
			}
			else
	        if ( result.equals("no_topic_message") )
	        {
				//sendEmail("Toped Edit Result No Message" , "Could not get the message to edit in TopicEditActivity");
		        
		        Toast.makeText(getApplicationContext(), "Could not get the message. Please let us know about this problem.", Toast.LENGTH_LONG).show();			     		        
	        }
	        else
	        {		        
		        Toast.makeText(getApplicationContext(), "Success editing.", Toast.LENGTH_LONG).show();			     		        
			    
	        	// Now redirect back to topic.
		        Intent loginIntent = new Intent( TopicEditActivity.this, TopicActivity.class);
		        TopicEditActivity.this.startActivity(loginIntent);	    
	        }  // End of else
		}    
    }
    
    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }            
}
