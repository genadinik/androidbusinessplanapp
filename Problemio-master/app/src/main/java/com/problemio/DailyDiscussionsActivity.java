package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.problemio.data.DailyDiscussion;

import utils.SendEmail;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class DailyDiscussionsActivity extends BaseListActivity
{
	private Dialog dialog;

	ArrayAdapter<DailyDiscussion> adapter;			
	ArrayList<DailyDiscussion> discussions = new ArrayList <DailyDiscussion>( );			
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.daily_discussions);
	    
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( DailyDiscussionsActivity.this);
//	    String user_id = prefs.getString( "user_id" , null );		
//	    String first_name = prefs.getString( "first_name" , null );			    
//	    String last_name = prefs.getString( "last_name" , null );			    
//	    String email = prefs.getString( "email" , null );			    

	    //Button web_content_squarespace = (Button)findViewById(R.id.web_content_squarespace);
	    
//	    Button start_discussion = (Button)findViewById(R.id.start_discussion);
//	    start_discussion.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	        	
//	            Intent myIntent = new Intent(DailyDiscussionsActivity.this, AskCommunityActivity.class);
//	            DailyDiscussionsActivity.this.startActivity(myIntent);
//	        }
//	    }); 	    
	    
	    
	    
	    adapter = new ArrayAdapter<DailyDiscussion>(this, R.layout.discussions_list, discussions);
        
        setListAdapter ( adapter );
                
        ListView lv = getListView();
        lv.setTextFilterEnabled(true);
        
        lv.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) 
            {
              // When clicked, show a toast with the TextView text
              Toast.makeText(getApplicationContext(), (( TextView ) view).getText(),
                  Toast.LENGTH_SHORT).show();
              
              String question_name = discussions.get( position ).getDiscussion() + "";
              String question_id = discussions.get( position ).getDiscussionId() + "";
              
              // Now put this stuff into the SharedPreferences
		      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
		    		  DailyDiscussionsActivity.this);
		      		      
		      prefs.edit().putString( "recent_discussion_id" , question_id ).commit();
		      prefs.edit().putString( "recent_discussion" , question_name ).commit();
		      		      
              // And go to problem intent
	          Intent myIntent = new Intent(DailyDiscussionsActivity.this, DiscussionActivity.class);
	          DailyDiscussionsActivity.this.startActivity(myIntent);
            }
          });                
	    
	    
	    

	  

//	    	 
//	  marketing_button_2.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {	        	
//	            Intent myIntent = new Intent(WebSetupActivity.this, PremiumWebAdvertisingActivity.class);
//	            WebSetupActivity.this.startActivity(myIntent);
//	        }
//	    });	    
	    
	    
	    
	  
//	    Button give_review = (Button)findViewById(R.id.give_review);   
//	    give_review.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                sendEmail("Timeline --> Give Review", "From advertising page, user clicked on give review" );   	
//            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.problemio"));
//            	//startActivity(intent);
//            	            	 
//            	 try 
//            	 {
//            	        startActivity(intent);
//            	 } 
//            	 catch (ActivityNotFoundException anfe) 
//            	 {
//                     sendEmail("Give Review ERROR", "From advertising page, user clicked on give review and this is the exception: " + anfe.getMessage() );   	
//            		
//                     try
//                     {
//                    	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);  
//                     }
//                     catch ( Exception e)
//                     {
//                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//                             sendEmail("Give Review (2) ERROR", "From advertising page, user clicked on web version of give review and this is the exception: " + except.getMessage() );   	                    	 
//                         }
//                     }
//            	 }
//            }
//        });
        
//	    Button premium_marketing = (Button)findViewById(R.id.premium_marketing);
//	    premium_marketing.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	        public void onClick(View v) 
//	        {		              
//	          Intent intent = new Intent(Intent.ACTION_VIEW);
//	      	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//	      	            	 
//	      	 try 
//	      	 {
//	      	        startActivity(intent);
//	      	 } 
//	      	 catch (ActivityNotFoundException anfe) 
//	      	 {
//	               try
//	               {
//	              	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//	              	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	              	 startActivity(next_intent);  
//	               }
//	               catch ( Exception e)
//	               {
//	                   // Now try to redirect them to the web version:
//	                   Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//	                   try
//	                   {
//	                  	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                  	 startActivity(webintent);
//	                   }
//	                   catch ( Exception except )
//	                   {
//
//	                   }
//	               }
//	      	 }           	            	
//	      }
//	    });	 	     	      	      
	    
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	    web_share_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//            	sendEmail("Web ~ Maybe Share", "From web page, user clicked on share button" );   	
//            	
//                builder.setMessage("Like or Tweet from our home page to share with your friends? It helps the app grow so thank you!")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                         
//                        sendEmail("Web ~ Yes Share", "From web page, user clicked on share button" );   	        		       
//                    
//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//                      		  Uri.parse("http://www.problemio.com"));
//                        startActivity(browserIntent);
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() 
//                {
//                    public void onClick(DialogInterface dialog, int id) 
//                    {
//                         dialog.cancel();
//                    }
//                });
//         AlertDialog alert = builder.create();
//         alert.show();
//            }
//        });	  
	    
	    
	    
	    getLatestDiscussions( );
	}

    public void getLatestDiscussions(  ) 
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/get_latest_discussions.php" };
        
        DownloadWebPageTask task = new DownloadWebPageTask();
        
        task.execute(params);        
    }  	
	
	
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(DailyDiscussionsActivity.this);

		        dialog.setContentView(R.layout.please_wait);
		        dialog.setTitle("Getting latest discussions...");
		        dialog.show();
		 }    	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        final URL url = new URL( myUrl  );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
		        	//sendEmail ("Exeption in getting discussions" , "Exception: " + e.getMessage());
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	     			
			try 
			{
		        dialog.dismiss();
		    } 
			catch (Exception ee) 
			{
		        // nothing
		    }
			
			if ( result == null )
	        {
		        Toast.makeText(getApplicationContext(), "Could not get discussions. Please try again." , Toast.LENGTH_LONG).show();			     		        				

	        }
			else
			if ( result.trim().equals( "no_plans_for_user" ) )
	        {
		        Toast.makeText(getApplicationContext(), "Could not get discussions. Please try again." , Toast.LENGTH_LONG).show();			     		        
	        
//                sendEmail("Error in My Questions", "The questions by user did not load because on the server the validation for user id said the user id was empty." );   	
	        }
	        else
	        {
		            try
			        {	
			        	JSONArray obj = new JSONArray(result);				        			        

			    		ArrayList<DailyDiscussion> chats = new ArrayList <DailyDiscussion>( );
			        	
				        discussions.clear();

		        		for ( int i = 0; i < obj.length(); i++ )
		        		{
		        			JSONObject o = obj.getJSONObject(i);

				            String discussion_id = o.getString("discussion_id");
				            String discussion = o.getString("discussion");
				            String author_id = o.getString("author_id");
				            
//				            //String month = o.getString("MONTH(date)");
//				            String day = o.getString("DAYOFMONTH(date)");
//				            String year = o.getString("YEAR(date)");
//				            String month_name = o.getString("MONTHNAME(date)");
				            
				            DailyDiscussion d = new DailyDiscussion ( );
				            d.setDiscussion(discussion);
				            d.setDiscussionId(discussion_id);
				            d.setAuthorId(author_id);

				            chats.add( d );
				        }		                
		        		discussions.addAll(chats);
   
					    adapter.notifyDataSetChanged();
					    
//				        TextView question_label = (TextView) findViewById(R.id.question_label);
//				        question_label.setText("Please participate in the discussions below.");
			        }			        
			        catch ( Exception e )
			        {
				        e.printStackTrace();
			        }
		        }		        
		    }
		}        	
	
	
	
	
	
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
