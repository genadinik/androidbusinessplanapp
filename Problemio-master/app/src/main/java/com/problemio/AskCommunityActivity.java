package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class AskCommunityActivity extends BaseActivity
{

	EditText question;
	EditText email;
	EditText name;
    Button submit;

    @Override
	public void onCreate(Bundle savedInstanceState) 
    {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.ask_question);        
    
	    // Make sure the user is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AskCommunityActivity.this);
        final String user_id = prefs.getString( "user_id" , null );
        final String session_name = prefs.getString( "first_name" , null );
        final String session_email = prefs.getString( "email" , null );          
	    
        
    	Button premium_button = (Button)findViewById(R.id.premium_button);
	    premium_button.setOnClickListener(new Button.OnClickListener() 
	      {  
	          public void onClick(View v) 
	          {	   		              
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=business.premium"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	
	        }
	      });	 	     	    	            
        
	    TextView question_ask = (TextView) findViewById(R.id.question_ask);
	    question = (EditText) findViewById(R.id.question);

	    TextView email_ask = (TextView) findViewById(R.id.email_ask);
	    email = (EditText) findViewById(R.id.email);
	    TextView name_ask = (TextView) findViewById(R.id.name_ask);
	    name = (EditText) findViewById(R.id.name);	    

	    submit = (Button)findViewById(R.id.submit);    
	    
	    
	    if ( user_id == null )
	    {
			Toast.makeText(getApplicationContext(), "Please log in", Toast.LENGTH_LONG).show();	

	        Intent myIntent = new Intent(AskCommunityActivity.this, LoginActivity.class);
	        AskCommunityActivity.this.startActivity(myIntent);	
	    }
	    
	    // Now pre-fill the entries
	    if ( session_email != null )
	    {	
	    	email.setText(session_email);
	    	email.setEnabled(false);
	    	
	    	email_ask.setText("This is the email associated with your account.  If you wish to use a different email, please update your profile.");
	    }
	    
	    if ( session_name != null )
	    {
	    	name.setText(session_name);
	    }
	    
	    submit.setOnClickListener(new Button.OnClickListener() 
	    {  
	 	   public void onClick(View v) 
	 	   {
			  Toast.makeText(getApplicationContext(), "Submitting your question. Please wait...", Toast.LENGTH_LONG).show();	
	 		  
			  String q = question.getText().toString(); 			  
			  String e = email.getText().toString(); 			  
			  String n = name.getText().toString(); 
	
			  if ( e != null )
			  {
				  e = e.trim();
			  }
			  
    	      // Put the persons SHarepPrefs email in there.
    	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
    	    		  AskCommunityActivity.this);

    	      String existing_email = prefs.getString( "email" , null );
    	      
    	      if ( existing_email == null || existing_email.trim().length() < 2 )
    	      {
        	     prefs.edit()        
  		        .putString("email", e)
  		        .putString("recent_question", q )
  		        .putString( "first_name", n )
  		        .commit();    	    	  
    	      }
    	      else
    	      {
        	     prefs.edit()        
  		        .putString("recent_question", q )
  		        .putString( "first_name", n )
  		        .commit();     	    	  
    	      }
				  
			  
			  
    	 	  //Set the email pattern string
    		  Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
    		  //Match the given string with the pattern
    		  Matcher m = pattern.matcher(e);
    		  //check whether match is found
    		  boolean matchFound = m.matches();
    		 
	    	
			  if ( user_id == null )
			  {				  
            	  Toast.makeText(getApplicationContext(), "Something went wrong. Please make sure you are logged in.", Toast.LENGTH_LONG).show();	            	  
			  }
			  else
			  if ( n == null || n.length() < 2 )
              {
            	  Toast.makeText(getApplicationContext(), "Please enter your name.", Toast.LENGTH_LONG).show();	            	                  
              }
              else
              if ( e == null || !matchFound )
              { 
    	 		   Toast.makeText(getApplicationContext(), "The email: " + e + " has invalid format.  Please try again." , Toast.LENGTH_LONG).show();     	 		        
    		  }				  
              else
              if ( q == null || q.length() < 5 )
              {
    			  Toast.makeText(getApplicationContext(), "Please enter a real business question that you need help with.", Toast.LENGTH_LONG).show();	            	                
              }
              else
              {
            	  // Add the question to prefs
    		      //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AskQuestionActivity.this);            	  
    		      
            	  sendFeedback( q , user_id , e , n , "2");   
              }
	 	   }
	    });        
    }

    public void sendFeedback( String question , String user_id , String email , String name , 
    		String privacy) 
    {  
        String[] params = new String[] { "https://www.problemio.com/problems/add_question_mobile.php?platform=free_marketing",
        		question , user_id , email , name , privacy};

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }          
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String question = theParams[1];
	        final String user_id = theParams[2];
	        final String email = theParams[3];	        
	        final String name = theParams[4];	      
	        final String privacy = theParams[5];	      
	        
	        // TODO: ADD THE PERSONS EMAIL AND NAME TO APPROPRIATE OBJECTS
	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AskCommunityActivity.this);
	        prefs.edit().putString("email", email ).commit();
	        prefs.edit().putString("last_name", name ).commit();
	        
	        String charset = "UTF-8";	        
	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("question=%s&user_id=%s&email=%s&name=%s&privacy=%s", 
		        	     URLEncoder.encode(question, charset) ,
		        	     URLEncoder.encode(user_id, charset) ,
		        	     URLEncoder.encode(email, charset) ,
		        	     URLEncoder.encode(name, charset) , 
		        	     URLEncoder.encode(privacy, charset)
		        		);

		        final URL url = new URL( myUrl + "&" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");

		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					e.printStackTrace();
			}
			
			return response;
		}
    
		@Override
		protected void onPostExecute(String result) 
		{	        
	        if ( result != null && result.equals( "error_adding_question") )
	        {	        	        	
		        Toast.makeText(getApplicationContext(), "We encountered an error while adding your question.  We are aware of the error and it will be fixed in the next update of the app.", Toast.LENGTH_LONG).show();		        
	        }	        
	        else
	        if ( result != null && result.equals( "no_question" ) )
	        {	        	        	
		        Toast.makeText(getApplicationContext(), "We encountered an error while adding your question.  We are aware of the error and it will be fixed in the next update of the app.", Toast.LENGTH_LONG).show();		        
	        }
	        else
	        if ( result != null && result.equals( "no_member_id" ) )
	        {		        
		        Toast.makeText(getApplicationContext(), "We encountered an error while adding your question.  We are aware of the error and it will be fixed in the next update of the app.", Toast.LENGTH_LONG).show();	
		    }
	        else
	        {		        
		        // Add recent question id to prefs.
  		        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AskCommunityActivity.this);
  		        prefs.edit().putString("recent_question_id", result ).commit();         		        
		        
		        Toast.makeText(getApplicationContext(), "We have successfully asked your question.  Please wait while we redirect you.", Toast.LENGTH_LONG).show();	
	        
		        // Now go to question page
		        Intent myIntent = new Intent(AskCommunityActivity.this, QuestionActivity.class);
		        AskCommunityActivity.this.startActivity(myIntent);		        
	        }
		}    
    }		    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    
    @Override
	public void onStop()
    {
       super.onStop();

       // your code
    }        	
}
