package com.problemio;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.problemio.content.MarketIdeaValidationActivity;
import com.problemio.content.PitchBusinessActivity;
import com.problemio.content.StageTacticsActivity;

import utils.SendEmail;

//import android.widget.ShareActionProvider;

public class TimelineActivity extends BaseActivity 
{	
	private ShareActionProvider myShareActionProvider;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.content_timeline);
	    
        // SHARING ONLY ENABLED in SDK 14 which is Ice Cream Sandwich
//        try
//        {
////	        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
////			  Uri.parse("http://www.problemio.com/business/how_to_build_a_business.php"));
////    		startActivity(browserIntent);
//
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//	            Button share = (Button)findViewById(R.id.share_button);
//	    	    share.setOnClickListener(new Button.OnClickListener()
//	    	    {
//	    	        public void onClick(View v)
//	    	        {
//	    	        	openOptionsMenu();
//	    	        }
//	    	    });
//	        }
//	        else
//	        {
//	        	// HIDE THE TWO PAGE ELEMENTS
//	            Button share = (Button)findViewById(R.id.share_button);
//	            TextView share_prompt = (TextView)findViewById(R.id.share_prompt);
//
//	            share.setVisibility(View.GONE);
//	            share_prompt.setVisibility(View.GONE);
//	        }
//        }
//        catch ( Exception e )
//        {
//           //sendEmail ("Problemio Home Reg Exception" , e.getMessage() + "");
//        }
	    
	    Button timeline_marketing_app = (Button)findViewById(R.id.timeline_marketing_app);
	    timeline_marketing_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
            	//sendEmail("timeline --> marketing" , "");
            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           
            }
        });
	    
	    
	    Button free_business_idea_app = (Button)findViewById(R.id.free_business_idea_app);
	    free_business_idea_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=business.ideas"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:business.ideas");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.ideas");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	
            }
        });   		
	    
	    
	    Button business_idea_app = (Button)findViewById(R.id.business_idea_app);
		business_idea_app.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {                   	
	            Intent intent = new Intent(Intent.ACTION_VIEW);
	        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
	        	            	 
	        	 try 
	        	 {
	        	        startActivity(intent);
	        	 } 
	        	 catch (ActivityNotFoundException anfe) 
	        	 {
	                 try
	                 {
	                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                	 startActivity(next_intent);  
	                 }
	                 catch ( Exception e)
	                 {
	                     // Now try to redirect them to the web version:
	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
	                     try
	                     {
	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                    	 startActivity(webintent);
	                     }
	                     catch ( Exception except )
	                     {

	                     }
	                 }
	        	 }           	            	
            }
        });   		
	    
	    
	    
//	    Button fundraising = (Button)findViewById(R.id.fundraising);
//		    
//		    
//		    
//		    fundraising.setOnClickListener(new Button.OnClickListener() 
//		      {  
//		          public void onClick(View v) 
//		          {			              
//		            Intent intent = new Intent(Intent.ACTION_VIEW);
//		        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
//		        	            	 
//		        	 try 
//		        	 {
//		        	        startActivity(intent);
//		        	 } 
//		        	 catch (ActivityNotFoundException anfe) 
//		        	 {
//		                 try
//		                 {
//		                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
//		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//		                	 startActivity(next_intent);  
//		                 }
//		                 catch ( Exception e)
//		                 {
//		                     // Now try to redirect them to the web version:
//		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
//		                     try
//		                     {
//		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//		                    	 startActivity(webintent);
//		                     }
//		                     catch ( Exception except )
//		                     {
//
//		                     }
//		                 }
//		        	 }           	            	
//		        }
//		      });	 	     	    	    
	    
	    
	    Button website = (Button)findViewById(R.id.website);	    
	    Button pitch_business = (Button)findViewById(R.id.pitch_business);
	    Button button_validate_idea_cheaply = (Button)findViewById(R.id.button_validate_idea_cheaply);
//	    Button prevent_mistakes = (Button)findViewById(R.id.business_starting_mistakes);
	    Button button_early_stages = (Button)findViewById(R.id.button_early_stages);
		Button legal_training = (Button)findViewById(R.id.legal_training);
		//Button insurance_button = (Button)findViewById(R.id.insurance_button);
		//Button freshbooks_button = (Button)findViewById(R.id.freshbooks_button);

//		freshbooks_button.setOnClickListener(new Button.OnClickListener()
//		{
//			public void onClick(View v)
//			{
//				//sendEmail("Timeline-->Freshbooks", "");
//
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//						Uri.parse("http://www.anrdoezrs.net/click-7252177-12392965-1446654026000"));
//
//				boolean isChromeInstalled = isPackageInstalled("com.android.chrome", TimelineActivity.this);
//				if (isChromeInstalled) {
//					browserIntent.setPackage("com.android.chrome");
//					startActivity(browserIntent);
//				}else{
//					Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//					startActivity(chooserIntent);
//				}
//
//				startActivity(browserIntent);
//
//			}
//		});

//		insurance_button.setOnClickListener(new Button.OnClickListener()
//		{
//			public void onClick(View v)
//			{
//				//sendEmail ("Timeline-->Hiscox" , "");
//
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//						Uri.parse("http://www.anrdoezrs.net/click-7252177-11747241-1395762953000"));
//
//				boolean isChromeInstalled = isPackageInstalled("com.android.chrome", TimelineActivity.this);
//				if (isChromeInstalled) {
//					browserIntent.setPackage("com.android.chrome");
//					startActivity(browserIntent);
//				}else{
//					Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//					startActivity(chooserIntent);
//				}
//
//				startActivity(browserIntent);
//			}
//		});

	    
	    
	  website.setOnClickListener(new Button.OnClickListener() 
	  {  
	      public void onClick(View v) 
	      {	      	
	        Intent myIntent = new Intent(TimelineActivity.this, WebsiteSetupActivity.class);
	        TimelineActivity.this.startActivity(myIntent);
	      }
	  });
	    	      
	  pitch_business.setOnClickListener(new Button.OnClickListener() 
	  {  
	      public void onClick(View v) 
	      {	      	
	        Intent myIntent = new Intent(TimelineActivity.this, PitchBusinessActivity.class);
	        TimelineActivity.this.startActivity(myIntent);
	      }
	  });	    
    
	  button_validate_idea_cheaply.setOnClickListener(new Button.OnClickListener() 
	  {  
	      public void onClick(View v) 
	      {	      	
	        Intent myIntent = new Intent(TimelineActivity.this, MarketIdeaValidationActivity.class);
	        TimelineActivity.this.startActivity(myIntent);
	      }
	  });

		legal_training.setOnClickListener(new Button.OnClickListener()
	  {
	      public void onClick(View v)
	      {
			  //sendEmail ("Timeline-->LawCourse" , "");

			  Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					  Uri.parse("https://www.udemy.com/startup-small-business-law-business-registration/?couponCode=androidapp"));

			  boolean isChromeInstalled = isPackageInstalled("com.android.chrome", TimelineActivity.this);
			  if (isChromeInstalled) {
				  browserIntent.setPackage("com.android.chrome");
				  startActivity(browserIntent);
			  }else{
				  Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
				  startActivity(chooserIntent);
			  }

			  startActivity(browserIntent);
	      }
	  });
	  
	  button_early_stages.setOnClickListener(new Button.OnClickListener() 
	  {  
	      public void onClick(View v) 
	      {	      	
	        Intent myIntent = new Intent(TimelineActivity.this, StageTacticsActivity.class);
	        TimelineActivity.this.startActivity(myIntent);
	      }
	  });	  
	  
	  
	  
	  
	  

	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.problemio"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.problemio");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.problemio");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }
            }
        });
        
        Button planBusiness = (Button)findViewById(R.id.plan_business);   
        //Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);  
        
//        Button online_course = (Button)findViewById(R.id.online_course); 
//        online_course.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	 	   public void onClick(View v) 
//	 	   { 
//	 	        sendEmail("Timeline ~ Emily 2", "" );   	        		       
//	 	    
//	 	          Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	 	        		  Uri.parse("http://emilychasesmith.leadpages.net/problemiocfo/"));
//	 	          startActivity(browserIntent);	 		       
//	 	   }
//	    });	
                
        
        

        
//        Button business_starting_mistakes = (Button)findViewById(R.id.business_starting_mistakes);
//
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//   		business_starting_mistakes.setOnClickListener(new Button.OnClickListener()
//	    {
//	 	   public void onClick(View v)
//	 	   {
//	 		   // Send me an email that a comment was submitted on a question.
//
//	 	     builder.setMessage("We made premium features simple to unlock. " +
//	 	     		"All we ask is your good will in giving back to the app in a show of thanks and help keep the app free.")
//             .setCancelable(false)
//             .setPositiveButton("Show Thanks", new DialogInterface.OnClickListener() {
//                 public void onClick(DialogInterface dialog, int id) {
//
//                     Intent myIntent = new Intent(TimelineActivity.this, GiveBackActivity.class);
//                     TimelineActivity.this.startActivity(myIntent);
//                 }
//             })
//             .setNegativeButton("No", new DialogInterface.OnClickListener()
//             {
//                 public void onClick(DialogInterface dialog, int id)
//                 {
//                      dialog.cancel();
//	              }
//          	 });
//       		   AlertDialog alert = builder.create();
//               alert.show();
//	 	   }
//	    });
        
   
        

        planBusiness.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	
              Intent myIntent = new Intent(TimelineActivity.this, AddProblemActivity.class);
              TimelineActivity.this.startActivity(myIntent);
            }
        });     
        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(TimelineActivity.this, AskQuestionActivity.class);
//                TimelineActivity.this.startActivity(myIntent);
//            }
//        });	      
	    
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);

//        Button share_button = (Button)findViewById(R.id.timeline_share_button);   
//        share_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//            	
//              builder.setMessage("You will be taken to our home page from which you can Like or Tweet which will share the app to your friends?")
//              .setCancelable(false)
//              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                  public void onClick(DialogInterface dialog, int id) {
//                       
//                  
//                      Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//                    		  Uri.parse("http://www.problemio.com"));
//                      startActivity(browserIntent);
//                  }
//              })
//              .setNegativeButton("No", new DialogInterface.OnClickListener() 
//              {
//                  public void onClick(DialogInterface dialog, int id) 
//                  {
//                       dialog.cancel();
//                  }
//              });
//       AlertDialog alert = builder.create();
//       alert.show();
//            }
//        });  

	
        //TextView squarespace_text = (TextView)findViewById(R.id.squarespace_text);           
//        Button squarespace_button = (Button)findViewById(R.id.squarespace_button_timeline);          
//        squarespace_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	
//              Intent myIntent = new Intent(TimelineActivity.this, WebSetupActivity.class);
//              TimelineActivity.this.startActivity(myIntent);
//              
////              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
////            		  Uri.parse("http://squarespace.7eer.net/c/35378/38409/1291"));
////              
////              startActivity(browserIntent);
//            }
//        });                    
        
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }       
    
//    @Override
//    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    public boolean onCreateOptionsMenu(Menu menu)
//    {
//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        getMenuInflater().inflate(R.menu.menu_main, menu);
//		        MenuItem item = menu.findItem(R.id.menu_item_share);
//		        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//		        myShareActionProvider.setShareHistoryFileName(
//		          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//		        myShareActionProvider.setShareIntent(createShareIntent());
//		        return true;
//	        }
//        }
//        catch ( Exception e )
//        {
//
//        }
//
//        return false;
//    }
//
//    private Intent createShareIntent()
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT,
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//
//    // Somewhere in the application.
//    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    public void doShare(Intent shareIntent)
//    {
//        try
//        {
//	        if ( android.os.Build.VERSION.SDK_INT >= 14 )
//	        {
//		        // When you want to share set the share intent.
//		        myShareActionProvider.setShareIntent(shareIntent);
//	        }
//        }
//        catch ( Exception e )
//        {
//
//        }
//    }

	private boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}
}
