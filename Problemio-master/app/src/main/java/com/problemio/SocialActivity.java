package com.problemio;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

public class SocialActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    setContentView(R.layout.social);
	    
	    
        Button fb = (Button)findViewById(R.id.fb);          
        fb.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
            	 // 
            	
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                		  Uri.parse("https://www.facebook.com/groups/problemio/"));
                  startActivity(browserIntent);

            }
        });
        
        Button twitter = (Button)findViewById(R.id.twitter);          
        twitter.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                		  Uri.parse("http://www.twitter.com/problemio"));
                  startActivity(browserIntent);

            }
        });
        
        Button newsletter = (Button)findViewById(R.id.newsletter);          
        newsletter.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                		  Uri.parse("http://glowingstart.com/email-subscribe/"));
                  startActivity(browserIntent);

            }
        });
	    
        Button youtube = (Button)findViewById(R.id.youtube);          
        youtube.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
                		  Uri.parse("http://www.youtube.com/user/Okudjavavich"));
                  startActivity(browserIntent);

            }
        });    
	   
	


	    
	    
	}
}
