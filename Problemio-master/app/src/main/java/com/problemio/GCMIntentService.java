package com.problemio;

import static com.google.android.gcm.GCMConstants.ERROR_SERVICE_NOT_AVAILABLE;
import static com.google.android.gcm.GCMConstants.EXTRA_ERROR;
import static com.google.android.gcm.GCMConstants.EXTRA_REGISTRATION_ID;
import static com.google.android.gcm.GCMConstants.EXTRA_SPECIAL_MESSAGE;
import static com.google.android.gcm.GCMConstants.EXTRA_TOTAL_DELETED;
import static com.google.android.gcm.GCMConstants.EXTRA_UNREGISTERED;
import static com.google.android.gcm.GCMConstants.INTENT_FROM_GCM_LIBRARY_RETRY;
import static com.google.android.gcm.GCMConstants.INTENT_FROM_GCM_MESSAGE;
import static com.google.android.gcm.GCMConstants.INTENT_FROM_GCM_REGISTRATION_CALLBACK;
import static com.google.android.gcm.GCMConstants.VALUE_DELETED_MESSAGES;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.android.gcm.GCMBaseIntentService;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
//import android.widget.Toast;

import utils.GCMConstants;
import utils.SendEmail;

public class GCMIntentService extends GCMBaseIntentService 
{
	public GCMIntentService() 
	{
		    super(ProblemioActivity.SENDER_ID);
	}
	
	@Override
	protected void onRegistered(Context ctxt, String regId) 
	{

	}

	  @Override
	  protected void onUnregistered(Context ctxt, String regId) 
	  {
	    //Log.d(getClass().getSimpleName(), "onUnregistered: " + regId);
	    //sendEmail("onUnRegistered method", "onUnRegistered: " + regId);
	  }

	  @Override
	  protected void onMessage(Context ctxt, Intent message) 
	  {
		  // sendEmail("onMessage! method", "Should be sending something");
		  
	    Bundle extras=message.getExtras();
	    
	    try
	    {
	    	if ( extras != null )
	    	{
		    	String notification_type = extras.getString("notification_type");
		    	if ( notification_type != null && notification_type.equals("question"))
		    	{
			    	String question_id = extras.getString("question_id");
			    	String recent_question = extras.getString("question");
	
			    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( this );
				    
				    prefs.edit()        
			        .putString("recent_question_id", question_id)
			        .putString("recent_question", recent_question)
			        .commit();		    		
		    	}
		    	else
		    	if ( notification_type != null && notification_type.equals("business"))
		    	{
			    	String recent_problem_id = extras.getString("recent_problem_id");
			    	String recent_topic_id = extras.getString("recent_topic_id");
			    	String recent_topic_name = extras.getString("recent_topic_name");
			    	
			    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( this );
	
				    prefs.edit()        
			        .putString("recent_problem_id", recent_problem_id)
			        .putString("recent_topic_id", recent_topic_id)
			        .putString("recent_topic_name", recent_topic_name)
			        .commit();		    	
		    	}
		    	else
		    	if ( notification_type != null && notification_type.equals("daily_discussion"))
		    	{
			    	//sendEmail("onMessage! method", "right");

			    	String discussion_id = extras.getString("discussion_id");
			    	String discussion = extras.getString("discussion");
			    	
			    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( this );
	
				    prefs.edit()        
			        .putString("recent_discussion_id", discussion_id)
			        .putString("recent_discussion", discussion)
			        .commit();		    	
		    	}		    	
				 	
		    	generateNotification(ctxt, extras.getString("message"), "New Message" , extras);
	    	}
	    	else
	    	{
		    	//sendEmail("onMessage! method", "extras null");

	    	}
	    	
	    	//sendEmail( "Creating Notification" , "SDK: " + Build.VERSION.SDK_INT );
	    }
	    catch ( Exception e )
	    {
	    	//sendEmail( "Creating Notification Exception " , "SDK: " + Build.VERSION.SDK_INT + " and Exception: " + e.getMessage() );
	    }
	  }
	  
	    // Subject , body
//	    public void sendEmail( String subject , String body )
//	    {
//	        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//	        SendEmail task = new SendEmail();
//	        task.execute(params);
//	    }
	  
	  @Override
	  protected void onError(Context ctxt, String errorMsg) 
	  {
	    //Log.d(getClass().getSimpleName(), "onError: " + errorMsg);
	    //sendEmail("sendEmail method" , "onError: " + errorMsg);
	  }

	  @Override
	  protected boolean onRecoverableError(Context ctxt, String errorMsg) 
	  {
	    
	    return(true);
	  }	
	  
	  private static void generateNotification(Context context, String message, String title , Bundle extras) 
	  {
		    String notification_type = "default";
		    if ( extras != null )
		    {
		    	notification_type = extras.getString("notification_type");	
		    }
		  
	        int icon = R.drawable.ic_launcher;
	        long when = System.currentTimeMillis(); // can change this to a future time if desired
	        
	        NotificationManager notificationManager = 
	        		(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

	        
	        
	        Intent notificationIntent = new Intent(context, ProblemioActivity.class);
	        if ( notification_type != null && notification_type.equals("question"))
	        {
	        	notificationIntent = new Intent(context, QuestionActivity.class);
	        }
	        else
		    if ( notification_type != null && notification_type.equals("business"))
		    {
	        	notificationIntent = new Intent(context, TopicActivity.class);		    	
		    }
	        else
		    if ( notification_type != null && notification_type.equals("daily_discussion"))
		    {
	        	notificationIntent = new Intent(context, DiscussionActivity.class);		    	
		    }
		    else
			if ( notification_type != null && notification_type.equals("default"))
			{
	        	notificationIntent = new Intent(context, ProblemioActivity.class);		    					
			}	        
	        

	        // set intent so it does not start a new activity
	        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);        
	        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);            

	         Notification notification = new NotificationCompat.Builder(context)
	         .setContentTitle(title)
	         .setContentText(message)
	         .setContentIntent(intent)
	         .setSmallIcon(icon)
	         .setLights(Color.YELLOW, 1, 2)
	         .setAutoCancel(true)
	         .setSound(defaultSound)
	         .build();

	         try
	         {
	        	 notificationManager.notify(0, notification);
	         }
	         catch ( Exception e )
	         {
	        	 
	         }
	}	  
}
