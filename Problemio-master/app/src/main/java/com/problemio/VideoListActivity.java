package com.problemio;

import utils.SendEmail;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;


// Business pitch video
//

public class VideoListActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");        

	    setContentView(R.layout.video_list);
	    
	      Button sub_button = (Button)findViewById(R.id.sub_button);
	      sub_button.setOnClickListener(new Button.OnClickListener() {
			  public void onClick(View v) {
//	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	  	          		  Uri.parse("http://www.youtube.com/user/Okudjavavich"));
//	  	            startActivity(browserIntent);

				  SharedPreferences prefs =
						  PreferenceManager.getDefaultSharedPreferences(VideoListActivity.this);

				  prefs.edit()
						  .putString("url_to_watch", "https://www.youtube.com/user/Okudjavavich")
						  .commit();

				  Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
				  VideoListActivity.this.startActivity(myIntent);
			  }
		  });

	      Button startbusiness = (Button)findViewById(R.id.start_business);
	      startbusiness.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://www.youtube.com/watch?v=h_kR40r5V0c")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });	       
	      
	      Button businessplan = (Button)findViewById(R.id.businessplan);
	      businessplan.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://youtu.be/waZojpDfedU")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);

	          }
	      });


		Button freedomain = (Button)findViewById(R.id.freedomain);
		freedomain.setOnClickListener(new Button.OnClickListener()
		{
			public void onClick(View v)
			{
				// Add url_to_watch in local storage and go to video screen
				SharedPreferences prefs =
						PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

				prefs.edit()
						.putString("url_to_watch", "https://youtu.be/SyQvYNVPG0U")
						.commit();

				Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
				VideoListActivity.this.startActivity(myIntent);

			}
		});


	      
	      Button fundraising = (Button)findViewById(R.id.fundraising);
	      fundraising.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://youtu.be/KGVgamPNPNY")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);

	          }
	      });

	      Button entrep = (Button)findViewById(R.id.entrep);
		  entrep.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://youtu.be/FbdRBubr2QI")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });
	      
	      Button social = (Button)findViewById(R.id.social);
	      social.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://youtu.be/nVh3ZAutEQg")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });
	      
	      Button pitch = (Button)findViewById(R.id.pitch);
	      pitch.setOnClickListener(new Button.OnClickListener()
	      {  
	          public void onClick(View v) 
	          {	            	          	
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs = 
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()        
		        .putString("url_to_watch", "https://www.youtube.com/watch?v=m8F6kVEUqIE")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });
	      
	      Button make_money = (Button)findViewById(R.id.make_money);
	      make_money.setOnClickListener(new Button.OnClickListener()
	      {
	          public void onClick(View v)
	          {
	        	  // Add url_to_watch in local storage and go to video screen
	      	    SharedPreferences prefs =
	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);

		        prefs.edit()
		        .putString("url_to_watch", "https://www.youtube.com/watch?v=XcIleEkezBY")
		        .commit();

	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });

	      Button premium = (Button)findViewById(R.id.premium);
		premium.setOnClickListener(new Button.OnClickListener()
	      {
	          public void onClick(View v)
	          {
	              Intent myIntent = new Intent(VideoListActivity.this, ExtraHelpActivity.class);
	              VideoListActivity.this.startActivity(myIntent);
	          }
	      });
//
//	      Button unit = (Button)findViewById(R.id.unit);
//	      unit.setOnClickListener(new Button.OnClickListener()
//	      {
//	          public void onClick(View v)
//	          {
//	        	  // Add url_to_watch in local storage and go to video screen
//	      	    SharedPreferences prefs =
//	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);
//
//		        prefs.edit()
//		        .putString("url_to_watch", "http://youtu.be/Ehkg3_UwzpY")
//		        .commit();
//
//	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
//	              VideoListActivity.this.startActivity(myIntent);
//	          }
//	      });
//
//	      Button identify = (Button)findViewById(R.id.identify);
//	      identify.setOnClickListener(new Button.OnClickListener()
//	      {
//	          public void onClick(View v)
//	          {
//	        	  // Add url_to_watch in local storage and go to video screen
//	      	    SharedPreferences prefs =
//	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);
//
//		        prefs.edit()
//		        .putString("url_to_watch", "http://youtu.be/loHvzCFnAvg")
//		        .commit();
//
//	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
//	              VideoListActivity.this.startActivity(myIntent);
//	          }
//	      });
//
//	      Button bizrev = (Button)findViewById(R.id.bizrev);
//	      bizrev.setOnClickListener(new Button.OnClickListener()
//	      {
//	          public void onClick(View v)
//	          {
//	        	  // Add url_to_watch in local storage and go to video screen
//	      	    SharedPreferences prefs =
//	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);
//
//		        prefs.edit()
//		        .putString("url_to_watch", "http://youtu.be/gxeXrasXy6Q")
//		        .commit();
//
//	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
//	              VideoListActivity.this.startActivity(myIntent);
//	          }
//	      });
//
//	      Button freemium = (Button)findViewById(R.id.freemium);
//	      freemium.setOnClickListener(new Button.OnClickListener()
//	      {
//	          public void onClick(View v)
//	          {
//	        	  // Add url_to_watch in local storage and go to video screen
//	      	    SharedPreferences prefs =
//	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);
//
//		        prefs.edit()
//		        .putString("url_to_watch", "http://youtu.be/8IqWwNLHIsU")
//		        .commit();
//
//	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
//	              VideoListActivity.this.startActivity(myIntent);
//	          }
//	      });
//
	      
//	      Button freemium = (Button)findViewById(R.id.freemium);
//	      freemium.setOnClickListener(new Button.OnClickListener() 
//	      {  
//	          public void onClick(View v) 
//	          {	            	          	
//	        	  // Add url_to_watch in local storage and go to video screen
//	      	    SharedPreferences prefs = 
//	      	    		PreferenceManager.getDefaultSharedPreferences( VideoListActivity.this);
//
//		        prefs.edit()        
//		        .putString("url_to_watch", "")
//		        .commit();
//
//	              Intent myIntent = new Intent(VideoListActivity.this, VideosActivity.class);
//	              VideoListActivity.this.startActivity(myIntent);
//	          }
//	      });		      
	      
      Button back = (Button)findViewById(R.id.back);
      back.setOnClickListener(new Button.OnClickListener() 
      {  
          public void onClick(View v) 
          {	            	          	
        	  // Go to home screen
              Intent myIntent = new Intent(VideoListActivity.this, ProblemioActivity.class);
              VideoListActivity.this.startActivity(myIntent);
          }
      });	       
	        
	            
	    
	    
//        Button problemio_button = (Button)findViewById(R.id.problemio_button);
//        problemio_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	          	
//  	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//  	          		  Uri.parse("http://www.twitter.com/problemio"));
//  	            startActivity(browserIntent);
//            }
//        });
//
//        Button youtube_button = (Button)findViewById(R.id.youtube_button);
//        youtube_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	          	
//  	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//  	          		  Uri.parse("http://www.youtube.com/user/Okudjavavich"));
//  	            startActivity(browserIntent);
//            }
//        });	    	        
//
//        Button facebook_button = (Button)findViewById(R.id.facebook_button);
//        facebook_button.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	          	
//  	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//  	          		  Uri.parse("https://www.facebook.com/Problemio.apps"));
//  	            startActivity(browserIntent);
//            }
//        });
        
//        Button back = (Button)findViewById(R.id.back);
//        back.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	          	
//                Intent myIntent = new Intent(KeepInTouchActivity.this, MainActivity.class);
//                KeepInTouchActivity.this.startActivity(myIntent);
//            }
//        });
	     
	     

	     
	       	    
	    
	    
	    
	    
	    
//	    
//	    Button give_review = (Button)findViewById(R.id.give_review);   
//	    give_review.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=business.ideas"));
//            	//startActivity(intent);
//            	            	 
//            	 try 
//            	 {
//            	        startActivity(intent);
//            	 } 
//            	 catch (ActivityNotFoundException anfe) 
//            	 {            		
//                     try
//                     {
//                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketing");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);  
//                     }
//                     catch ( Exception e)
//                     {
//                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//
//                         }
//                     }
//            	 }           	            	
//            }
//        });

	    
	    
//      Button godaddy = (Button)findViewById(R.id.godaddy);          
//      godaddy.setOnClickListener(new Button.OnClickListener() 
//      {  
//          public void onClick(View v) 
//          {	            	
//               //sendEmail("Free Ideas -> GoDaddy Check domain", "" );   	
//        	
//	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	          		  Uri.parse("http://www.jdoqocy.com/click-7252177-10985823"));
//	            startActivity(browserIntent);
//          }
//      });
	    
	    
//        Button support_app = (Button)findViewById(R.id.support_app);          
//        support_app.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(ChooseDomainNameActivity.this, SupportAppActivity.class);
//                ChooseDomainNameActivity.this.startActivity(myIntent);
//            }
//        });	 
	    

	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    	FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
    }
     
    @Override
    protected void onStop()
    {
    	super.onStop();		
    	FlurryAgent.onEndSession(this);
    }    	
}
