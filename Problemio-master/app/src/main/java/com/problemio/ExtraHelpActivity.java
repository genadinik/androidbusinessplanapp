package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.problemio.UpdateProfileActivity.DownloadPageTask;
import com.problemio.content.PremiumWebAdvertisingActivity;
import com.problemio.content.PsychologyActivity;
import com.problemio.content.TopMistakesActivity;
import com.problemio.content.WebsiteServiceActivity;

import utils.Consts;
import utils.Consts.PurchaseState;
import utils.Consts.ResponseCode;
import utils.SendEmail;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ExtraHelpActivity extends BaseActivity 
{
//	EditText email;
//	EditText person_name;
//	EditText person_phone;

    /**
     * Called when this activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }
    
    // TODO: start here
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	        super.onCreate(savedInstanceState);
		    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	        
	        setContentView(R.layout.extra_help);

	        Button books = (Button)findViewById(R.id.books);
			books.setOnClickListener(new Button.OnClickListener() {
				public void onClick(View v) {
					Intent myIntent = new Intent(ExtraHelpActivity.this, BooksActivity.class);
					ExtraHelpActivity.this.startActivity(myIntent);
				}
			});


			Button courses = (Button)findViewById(R.id.courses);
			courses.setOnClickListener(new Button.OnClickListener()
			{
				public void onClick(View v)
				{
					Intent myIntent = new Intent(ExtraHelpActivity.this, CoursesActivity.class);
					ExtraHelpActivity.this.startActivity(myIntent);
				}
			});

			Button coaching = (Button)findViewById(R.id.coaching);
		coaching.setOnClickListener(new Button.OnClickListener()
			{
				public void onClick(View v)
				{
					Intent myIntent = new Intent(ExtraHelpActivity.this, CoachingActivity.class);
					ExtraHelpActivity.this.startActivity(myIntent);
				}
			});








	        
	        
//	        Button community = (Button)findViewById(R.id.community);          
//	        community.setOnClickListener(new Button.OnClickListener() 
//	        {  
//	            public void onClick(View v) 
//	            {        	
//	                Intent myIntent = new Intent(ExtraHelpActivity.this, CommunityActivity.class);
//	                ExtraHelpActivity.this.startActivity(myIntent);
//	            }
//	        });	        
	        
	        
	        

	        

	        
//	        Button fund = (Button)findViewById(R.id.fund);          
//	        fund.setOnClickListener(new Button.OnClickListener() 
//	        {  
//	            public void onClick(View v) 
//	            {        	
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	            		  Uri.parse("https://www.udemy.com/10-fundraising-strategies-to-raise-money-for-your-business/?couponCode=ap"));
//	              
//	              startActivity(browserIntent);
//	            }
//	        });


//	        Button bplan = (Button)findViewById(R.id.bplan);
//	        bplan.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/how-to-write-a-business-plan/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button apps = (Button)findViewById(R.id.apps);
//	        apps.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/how-to-create-grow-a-mobile-app-iphone-android-business/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button fullmarkting = (Button)findViewById(R.id.fullmarkting);
//	        fullmarkting.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/marketing-plan-strategy-become-a-great-marketer/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button seo = (Button)findViewById(R.id.seo);
//	        seo.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/seo-with-google-other-large-platforms-to-get-great-scale/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//
//	        Button affiliate = (Button)findViewById(R.id.affiliate);
//	        affiliate.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/make-money-with-affiliate-marketing-earn-passive-income/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button social = (Button)findViewById(R.id.social);
//	        social.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/advanced-social-media-marketing-with-practical-strategies/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button real = (Button)findViewById(R.id.real);
//	        real.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/how-to-start-a-career-in-real-estate/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button publicity = (Button)findViewById(R.id.publicity);
//	        publicity.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/how-to-get-publicity-and-press-coverage-for-your-business/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button money = (Button)findViewById(R.id.money);
//	        money.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/how-to-make-money/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button events = (Button)findViewById(R.id.events);
//	        events.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/event-marketing-how-to-create-a-successful-event-series/?couponCode=freeforappusers"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button fb = (Button)findViewById(R.id.fb);
//	        fb.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/facebook-marketing-course/?couponCode=ap"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button twitter = (Button)findViewById(R.id.twitter);
//	        twitter.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("https://www.udemy.com/twitter-marketing-course/?couponCode=ap"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//
//
//	        Button fullmarketing = (Button)findViewById(R.id.fullmarketing);
//	        fullmarketing.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("http://www.amazon.com/Marketing-Strategies-People-Problemio-business-ebook/dp/B00IG83T7E"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button startbusiness = (Button)findViewById(R.id.startbusiness);
//	        startbusiness.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("http://www.amazon.com/Get-Business-Ideas-Start-ebook/dp/B00HZUVAUM"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button appbook = (Button)findViewById(R.id.appbook);
//	        appbook.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("http://www.amazon.com/Mobile-App-Marketing-Monetization-thousands-ebook/dp/B00N14RSNY"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button fundraisingbook = (Button)findViewById(R.id.fundraisingbook);
//	        fundraisingbook.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("http://www.amazon.com/10-Fundraising-Ideas-Strategies-strategies-ebook/dp/B00KADT0Q2"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
//
//	        Button facebookbook = (Button)findViewById(R.id.facebookbook);
//	        facebookbook.setOnClickListener(new Button.OnClickListener()
//	        {
//	            public void onClick(View v)
//	            {
//	              Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//	            		  Uri.parse("http://www.amazon.com/Facebook-Marketing-Business-Expect-Promoting-ebook/dp/B00LMGPDIK"));
//
//	              startActivity(browserIntent);
//	            }
//	        });
	        
	        
	        
   		    
   		    
   		    
   		    
   		    
   		    
   		    
   		    
      
	        

		        
	        
	        
//	        final TextView fun = (TextView) findViewById(R.id.fun);
//	        final TextView fun_text = (TextView) findViewById(R.id.fun_text);
//
//	        Button number_of_businesses = (Button)findViewById(R.id.number_of_businesses); 
//	        number_of_businesses.setOnClickListener(new Button.OnClickListener() 
//		    {  
//		 	   public void onClick(View v) 
//		 	   {   
//		 		   // Send me an email that a comment was submitted on a question. 
//		 	      sendEmail("Number of Businesses Clicked", "Someone clicked on number of businesses." );   
//		 		
//		 	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
//		            		ExtraHelpActivity.this);
//		 	      
//			 	  String num_of_businesses = prefs.getString( "num_of_businesses" , null );
//			 	    
//			 	  // If already bought this item, send them to the page.
//			 	  if ( num_of_businesses != null )
//			 	  {
//	                    sendEmail("Already purchased num_businesses", 
//	                    		"In the validation, redirecting to bought page.");
//			 	    	
//			 	    	Intent myIntent = new Intent(ExtraHelpActivity.this, NumberOfBusinessesActivity.class);
//				        ExtraHelpActivity.this.startActivity(myIntent);	
//			 	  }  
//			 	  else
//			 	  {
//	                    sendEmail("Else of check if already purchased", 
//	                    		"Num of businesses: " + num_of_businesses);
//		   		 	     
//	                    if(mBillingService.checkBillingSupported(Consts.ITEM_TYPE_INAPP))
//				        { 		                
//	                    	try
//		      				{  
//		      					  //if (mBillingService.requestPurchase("android.test.purchased", 
//		      					  //Consts.ITEM_TYPE_INAPP ,  null))	                    		
//	                    		
//		      					  if (mBillingService.requestPurchase(issueProductIdNumberofBusinesses, 
//		      					  Consts.ITEM_TYPE_INAPP ,  null)) 					   
//		      					  {
//		      				          sendEmail("NumOfBusinesses After Tried Billing", "Seemed to have worked fine" );   					          
//		      			          } 
//		      					  else 
//		      					  {
//		      			                //showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
//		      					        sendEmail("Num of Businesses NOT ok", "Broke somewhere along the way :)" );   	
//		      			          }
//		      				}
//		      				catch ( Exception e )
//		      				{
//		      					  String error_message = e.getMessage();  
//		      					  sendEmail( "Excepion in NumOfBusinesses" , "Here is the exception: " + error_message );
//		      				}				        
//				        } 
//				        else 
//				        {
//				        	  // If billing is not supported, email me that, and give them the item for free
//				        	 sendEmail("Billing Not supported", "Giving num of businesses article for free." );    
//		             
//					          // Send them to the screen of the article.
//					          Intent myIntent = new Intent(ExtraHelpActivity.this, NumberOfBusinessesActivity.class);
//					          ExtraHelpActivity.this.startActivity(myIntent);	
//				        }
//			 	  }				  
//		 	   }
//		    });	        	        
	
	 	     	    	        
       	        
	}

	
	
	
    
    
	
    // Subject , body
    public void sendEmail( String subject , String body )
    {
        String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };

        SendEmail task = new SendEmail();
        task.execute(params);            	
    }    

    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }    
	
    public void sendFeedback( String name , String email , String phone ) 
    {  
        String[] params = new String[] { "https://www.problemio.com/problems/professional_help.php",
        		name , email , phone };

        DownloadPageTask task = new DownloadPageTask();
        task.execute(params);        
    }       
    
    
    private class DownloadPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        final String name = theParams[1];
	        final String email = theParams[2];
	        final String phone = theParams[3];
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("platform=android&name=%s&email=%s&phone=%s", 
		        	     URLEncoder.encode(name, charset) ,
		        	     URLEncoder.encode(email + "", charset) ,
		        	     URLEncoder.encode(phone + "", charset) 
		        		);

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					e.printStackTrace();
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	        
	        if ( result == null )
	        {		        
                //sendEmail("SendLead Error Null", "Result came back as empty ...?" );             	        	
	        }
	        else
	        {                
  		        Toast.makeText(getApplicationContext(), "Success. You will be contacted in the next 48 business hours. ", 
		    		  Toast.LENGTH_LONG).show();                		        		        		        

	        } // End of if/else    
		}
	}        	
	
}
