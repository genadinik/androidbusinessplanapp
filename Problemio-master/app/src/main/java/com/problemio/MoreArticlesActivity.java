package com.problemio;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.problemio.content.AdvertisingActivity;
import com.problemio.content.TargetMarketActivity;

public class MoreArticlesActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    
	    setContentView(R.layout.more_articles);
	    	    		
//		Button marketing_app = (Button)findViewById(R.id.marketing_button);
//		marketing_app.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	            	
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//            	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//            	//startActivity(intent);
//            	            	 
//            	 try 
//            	 {
//            	        startActivity(intent);
//            	 } 
//            	 catch (ActivityNotFoundException anfe) 
//            	 {            		
//                     try
//                     {
//                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
//                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                    	 startActivity(next_intent);  
//                     }
//                     catch ( Exception e)
//                     {
//                         // Now try to redirect them to the web version:
//                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
//                         try
//                         {
//                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                        	 startActivity(webintent);
//                         }
//                         catch ( Exception except )
//                         {
//
//                         }
//                     }
//            	 }           
//            }
//        });
        
//        Button more_button = (Button)findViewById(R.id.more_button); 
//        more_button.setOnClickListener(new Button.OnClickListener() 
//	      {  
//	          public void onClick(View v) 
//	          {		              	        	  
//	        	  
//	            Intent intent = new Intent(Intent.ACTION_VIEW);
//	        	intent.setData(Uri.parse("market://details?id=business.premium"));
//	        	            	 
//	        	 try 
//	        	 {
//	        	        startActivity(intent);
//	        	 } 
//	        	 catch (ActivityNotFoundException anfe) 
//	        	 {
//	                 try
//	                 {
//	                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
//	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	                	 startActivity(next_intent);  
//	                 }
//	                 catch ( Exception e)
//	                 {
//	                     // Now try to redirect them to the web version:
//	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
//	                     try
//	                     {
//	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                    	 startActivity(webintent);
//	                     }
//	                     catch ( Exception except )
//	                     {
//
//	                     }
//	                 }
//	        	 }           	            	
//	        }
//	      });	 	        		
		
		
		
              Button business_ideas = (Button)findViewById(R.id.ideas_button); 
		      business_ideas.setOnClickListener(new Button.OnClickListener() 
		      {  
		          public void onClick(View v) 
		          {			             
		              Intent myIntent = new Intent(MoreArticlesActivity.this, ExtraHelpActivity.class);
		              MoreArticlesActivity.this.startActivity(myIntent);
		        	  
//		            Intent intent = new Intent(Intent.ACTION_VIEW);
//		        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
//		        	            	 
//		        	 try 
//		        	 {
//		        	        startActivity(intent);
//		        	 } 
//		        	 catch (ActivityNotFoundException anfe) 
//		        	 {
//		                 try
//		                 {
//		                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
//		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//		                	 startActivity(next_intent);  
//		                 }
//		                 catch ( Exception e)
//		                 {
//		                     // Now try to redirect them to the web version:
//		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
//		                     try
//		                     {
//		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//		                    	 startActivity(webintent);
//		                     }
//		                     catch ( Exception except )
//		                     {
//
//		                     }
//		                 }
//		        	 }           	            	
		        }
		      });	 	        
        
//            Button fundraising = (Button)findViewById(R.id.fundraising_button);          
//		    fundraising.setOnClickListener(new Button.OnClickListener() 
// 		      {  
// 		          public void onClick(View v) 
// 		          {	 		              
// 		            Intent intent = new Intent(Intent.ACTION_VIEW);
// 		        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
// 		        	            	 
// 		        	 try 
// 		        	 {
// 		        	        startActivity(intent);
// 		        	 } 
// 		        	 catch (ActivityNotFoundException anfe) 
// 		        	 {
// 		                 try
// 		                 {
// 		                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
// 		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
// 		                	 startActivity(next_intent);  
// 		                 }
// 		                 catch ( Exception e)
// 		                 {
// 		                     // Now try to redirect them to the web version:
// 		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
// 		                     try
// 		                     {
// 		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
// 		                    	 startActivity(webintent);
// 		                     }
// 		                     catch ( Exception except )
// 		                     {
//
// 		                     }
// 		                 }
// 		        	 }           	            	
// 		        }
// 		      });	 	     	    	    

			Button marketing_app_1 = (Button)findViewById(R.id.marketing_button_1);
			marketing_app_1.setOnClickListener(new Button.OnClickListener() 
	        {  
	            public void onClick(View v) 
	            {            		            	
	                Intent intent = new Intent(Intent.ACTION_VIEW);
	            	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
	            	//startActivity(intent);
	            	            	 
	            	 try 
	            	 {
	            	        startActivity(intent);
	            	 } 
	            	 catch (ActivityNotFoundException anfe) 
	            	 {            		
	                     try
	                     {
	                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
	                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	                    	 startActivity(next_intent);  
	                     }
	                     catch ( Exception e)
	                     {
	                         // Now try to redirect them to the web version:
	                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
	                         try
	                         {
	                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	                        	 startActivity(webintent);
	                         }
	                         catch ( Exception except )
	                         {

	                         }
	                     }
	            	 }           
	            }
	        });
	        
	        Button more_button_1 = (Button)findViewById(R.id.more_button_1); 
	        more_button_1.setOnClickListener(new Button.OnClickListener() 
		      {  
		          public void onClick(View v) 
		          {		              		        	  
		        	  
		            Intent intent = new Intent(Intent.ACTION_VIEW);
		        	intent.setData(Uri.parse("market://details?id=business.premium"));
		        	            	 
		        	 try 
		        	 {
		        	        startActivity(intent);
		        	 } 
		        	 catch (ActivityNotFoundException anfe) 
		        	 {
		                 try
		                 {
		                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
		                	 startActivity(next_intent);  
		                 }
		                 catch ( Exception e)
		                 {
		                     // Now try to redirect them to the web version:
		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
		                     try
		                     {
		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
		                    	 startActivity(webintent);
		                     }
		                     catch ( Exception except )
		                     {

		                     }
		                 }
		        	 }           	            	
		        }
		      });	 	        		
			
			
			
	              Button business_ideas_1 = (Button)findViewById(R.id.ideas_button_1); 
			      business_ideas_1.setOnClickListener(new Button.OnClickListener() 
			      {  
			          public void onClick(View v) 
			          {				        
			              Intent myIntent = new Intent(MoreArticlesActivity.this, ExtraHelpActivity.class);
			              MoreArticlesActivity.this.startActivity(myIntent);
			        	  
			        	  
//			            Intent intent = new Intent(Intent.ACTION_VIEW);
//			        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
//			        	            	 
//			        	 try 
//			        	 {
//			        	        startActivity(intent);
//			        	 } 
//			        	 catch (ActivityNotFoundException anfe) 
//			        	 {
//			                 try
//			                 {
//			                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
//			                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//			                	 startActivity(next_intent);  
//			                 }
//			                 catch ( Exception e)
//			                 {
//			                     // Now try to redirect them to the web version:
//			                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
//			                     try
//			                     {
//			                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//			                    	 startActivity(webintent);
//			                     }
//			                     catch ( Exception except )
//			                     {
//
//			                     }
//			                 }
//			        	 }           	            	
			        }
			      });	 	        
	        
	            Button fundraising_1 = (Button)findViewById(R.id.fundraising_button_1);          
			    fundraising_1.setOnClickListener(new Button.OnClickListener() 
	 		      {  
	 		          public void onClick(View v) 
	 		          {		 		              
	 		            Intent intent = new Intent(Intent.ACTION_VIEW);
	 		        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
	 		        	            	 
	 		        	 try 
	 		        	 {
	 		        	        startActivity(intent);
	 		        	 } 
	 		        	 catch (ActivityNotFoundException anfe) 
	 		        	 {
	 		                 try
	 		                 {
	 		                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
	 		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
	 		                	 startActivity(next_intent);  
	 		                 }
	 		                 catch ( Exception e)
	 		                 {
	 		                     // Now try to redirect them to the web version:
	 		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
	 		                     try
	 		                     {
	 		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
	 		                    	 startActivity(webintent);
	 		                     }
	 		                     catch ( Exception except )
	 		                     {

	 		                     }
	 		                 }
	 		        	 }           	            	
	 		        }
	 		      });	 	     	    	    

	
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }        
}
