package com.problemio;

import com.problemio.content.PlanRestaurantActivity;
import utils.SendEmail;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

//import com.flurry.android.FlurryAgent;
import com.problemio.content.PlanLocalActivity;
import com.problemio.content.PlanFashionActivity;
import com.problemio.content.PlanHomeBusinessActivity;
import com.problemio.content.PlanAppActivity;

public class PlanExamplesActivity extends BaseActivity  
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.business_plan_examples);
 
        // Get Shared Preferences.
//	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PlanExamplesActivity.this);
//	    String user_id = prefs.getString("user_id", null);
//	    String first_name = prefs.getString( "first_name" , null );
//	    String email = prefs.getString( "email" , null );

        Button business_restaurant = (Button)findViewById(R.id.business_restaurant);
        Button home_based_business = (Button)findViewById(R.id.home_based_business);
        Button community_business_fashion = (Button)findViewById(R.id.community_business_fashion); 
        Button community_business_tech = (Button)findViewById(R.id.community_business_tech);  
        Button community_business_local = (Button)findViewById(R.id.community_business_local);

        home_based_business.setOnClickListener(new Button.OnClickListener()
        {  
     	   public void onClick(View v) 
     	   {
  	 	    	 Intent myIntent = new Intent(PlanExamplesActivity.this, PlanHomeBusinessActivity.class);
  	 	    	 PlanExamplesActivity.this.startActivity(myIntent);	 	    	    	           	 	       
     	   }
        });  
        
        business_restaurant.setOnClickListener(new Button.OnClickListener()
        {
     	   public void onClick(View v)
     	   {
	 	    	 Intent myIntent = new Intent(PlanExamplesActivity.this, PlanRestaurantActivity.class);
	 	    	 PlanExamplesActivity.this.startActivity(myIntent);
     	   }
        });
        
        community_business_fashion.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
   	 	      
     		    Intent myIntent = new Intent(PlanExamplesActivity.this, PlanFashionActivity.class);
  	 	    	 PlanExamplesActivity.this.startActivity(myIntent);
     	   }
        }); 
        
        community_business_tech.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
    	 	      
     	 	    	 Intent myIntent = new Intent(PlanExamplesActivity.this, PlanAppActivity.class);
     	 	    	 PlanExamplesActivity.this.startActivity(myIntent);
     	   }
        }); 
        
        community_business_local.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {   

 	 	      Intent myIntent = new Intent(PlanExamplesActivity.this, PlanLocalActivity.class);
  	 	    	 PlanExamplesActivity.this.startActivity(myIntent);	

     	   }
        });         
    }
    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    }          
}
