package com.problemio;

import utils.Consts;
import utils.SendEmail;
import utils.Consts.PurchaseState;
import utils.Consts.ResponseCode;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;
import com.problemio.content.PremiumWebAdvertisingActivity;
import com.problemio.content.TopMistakesActivity;

public class CommunitySignupActivity extends BaseActivity
{	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.community_signup);
 
        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( CommunitySignupActivity.this);
	    String user_id = prefs.getString( "user_id" , null );		
	    String first_name = prefs.getString( "first_name" , null );			    
	    String last_name = prefs.getString( "last_name" , null );			    
	    String email = prefs.getString( "email" , null );			    
	    String community_subscription = prefs.getString( "community_subscription" , null );			    

		if ( user_id == null )
		{
			// TODO: redirect user and message them to log in.
		}
		
		if ( community_subscription != null )
		{			
	    	 Intent myIntent = new Intent(CommunitySignupActivity.this, CommunityActivity.class);
	    	 CommunitySignupActivity.this.startActivity(myIntent);				

		}
		else
		{
			
		}
		


        Button community_subscribe = (Button)findViewById(R.id.community_subscribe);   
        community_subscribe.setOnClickListener(new Button.OnClickListener() 
	    {  
	 	   public void onClick(View v) 
	 	   {   
	 	     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
	            		CommunitySignupActivity.this);
	 	     
	 	     String community_subscription = prefs.getString( "community_subscription" , null );
	 	    
	 
	 	    	 Intent myIntent = new Intent(CommunitySignupActivity.this, CommunityActivity.class);
	 	    	 CommunitySignupActivity.this.startActivity(myIntent);	
	 	    
	 	   }
	    });	           		
   		
   		
   		
        
	    
	    
//        Button community_to_home = (Button)findViewById(R.id.community_to_home);   
//        community_to_home.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {
//                Intent myIntent = new Intent(CommunitySignupActivity.this, ProblemioActivity.class);
//                CommunitySignupActivity.this.startActivity(myIntent);
//            }
//        });
    }    
    
     
   
        
       
        
    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }       		
}