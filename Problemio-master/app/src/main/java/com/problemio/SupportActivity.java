package com.problemio;


import utils.SendEmail;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class SupportActivity extends BaseActivity
{    
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support);
                
        // compute your public key and store it in base64EncodedPublicKey
        //mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        //mHelper.enableDebugLogging(false);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() 
//        {
//            public void onIabSetupFinished(IabResult result) 
//            
//                if (!result.isSuccess()) 
//                {
//                    // Oh noes, there was a problem.
//                    complain("Problem setting up in-app billing: " + result);
//                    return;
//                }
//
//                // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
//                sendEmail(TAG, "Setup successful. Querying inventory.");
//                mHelper.queryInventoryAsync(mGotInventoryListener);
//            }
//        });
        
        // CREATE THE SUBSCRIBE BUTTON
        Button subscribe = (Button)findViewById(R.id.subscribe);
        subscribe.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {            	
        	  onUpgradeAppButtonClicked ( );
          }
        });                   
        
        
    }
 
    
    
    // Listener that's called when we finish querying the items we own
//    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = 
//    		new IabHelper.QueryInventoryFinishedListener() 
//    {
//        public void onQueryInventoryFinished(IabResult result, Inventory inventory) 
//        {
//            if (result.isFailure()) 
//            {
//                complain("Failed to query inventory: " + result);
//                return;
//            }
//
//            sendEmail(TAG, "Query inventory was successful.");
//
//            // Do we have the premium upgrade?
//            mIsPremium = inventory.hasPurchase(SKU_SUBSCRIPTION);
//            sendEmail(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
//
//            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
////            if (inventory.hasPurchase(SKU_GAS)) {
////                Log.d(TAG, "We have gas. Consuming it.");
////                mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
////                return;
////            }
//
//            updateUi( mIsPremium );
//            setWaitScreen(false);
//            sendEmail(TAG, "Initial inventory query finished; enabling main UI.");
//        }
//    };
    
    
    
    
    
    void complain(String message) 
    {
    	//sendEmail(TAG, "**** TrivialDrive Error: " + message);
    }

    void alert(String message) 
    {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);

        bld.create().show();
    }

    // updates UI to reflect model
    public void updateUi( boolean hasSubscription ) 
    {
    	if ( hasSubscription )
    	{
    		// CHANGE THINGS TO SAY UNSUBSCRIBE
            Button subscribe = (Button)findViewById(R.id.subscribe);
            TextView summary_text = (TextView)findViewById(R.id.summary_text);

            summary_text.setText("You are subscribed.  But you can unsubscribe at any time");
            subscribe.setText( "Unsubscribe donation" );
            
    	}
    	
//        // update the car color to reflect premium status or lack thereof
//        ((ImageView)findViewById(R.id.free_or_premium)).setImageResource(mIsPremium ? R.drawable.premium : R.drawable.free);
//
//        // "Upgrade" button is only visible if the user is not premium
//        findViewById(R.id.upgrade_button).setVisibility(mIsPremium ? View.GONE : View.VISIBLE);
//
//        // update gas gauge to reflect tank status
//        int index = mTank >= TANK_RES_IDS.length ? TANK_RES_IDS.length - 1 : mTank;
//        ((ImageView)findViewById(R.id.gas_gauge)).setImageResource(TANK_RES_IDS[index]);
    }    
    
    // Enables or disables the "please wait" screen.
    void setWaitScreen(boolean set) 
    {
        //findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
        //findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    }
    
    
    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() 
    {
    	super.onDestroy();
    	
        // very important:
        //if (mHelper != null) mHelper.dispose();
        //mHelper = null;
    }    
    
    // Callback for when a purchase is finished
//    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() 
//    {
//        public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
//        {
//            sendEmail(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
//            if (result.isFailure()) 
//            {
//                // Oh noes!
//                complain("Error purchasing: " + result);
//                //setWaitScreen(false);
//                return;
//            }
//
//            sendEmail(TAG, "Purchase successful.");
//
//            if (purchase.getSku().equals(SKU_SUBSCRIPTION)) 
//            {
//                // bought 1/4 tank of gas. So consume it.
//                mIsPremium = true;
//                updateUi( mIsPremium );
//                setWaitScreen(false);
//            }
//        }
//    };
    
    // User clicked the "Upgrade to Premium" button.
    public void onUpgradeAppButtonClicked( ) 
    {
        //sendEmail(TAG, "Buy button clicked; launching purchase flow for upgrade. SKU: " + SKU_SUBSCRIPTION + " and RC_REQUEST: " + RC_REQUEST );
        //setWaitScreen(true);
        //mHelper.launchPurchaseFlow(this, SKU_SUBSCRIPTION , RC_REQUEST, mPurchaseFinishedListener );
        
//        mHelper.launchPurchaseFlow(this,
//                SKU_SUBSCRIPTION, IabHelper.ITEM_TYPE_INAPP, 
//                RC_REQUEST, mPurchaseFinishedListener, ""); 
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        //sendEmail(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
//        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) 
//        {
//        	// not handled, so handle it ourselves (here's where you'd
//            // perform any handling of activity results not related to in-app
//            // billing...
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//        else 
//        {
//            Log.d(TAG, "onActivityResult handled by IABUtil.");
//        }
    }    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}

