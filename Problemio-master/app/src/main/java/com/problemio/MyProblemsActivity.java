package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import utils.SendEmail;

import com.flurry.android.FlurryAgent;
import com.problemio.content.HelpInstructionsActivity;
import com.problemio.data.Problem;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyProblemsActivity extends BaseListActivity 
{
	ArrayAdapter<Problem> adapter;
	TextView problem_loading_prompt = null;
	
	ArrayList<Problem> problems = new ArrayList <Problem>( );
	
	private Dialog dialog;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.my_problems_page);
        
        // Setup the text to inform the person that their businesses are retrieved.
        problem_loading_prompt = (TextView) findViewById(R.id.problem_loading_prompt);
                
        adapter = new ArrayAdapter<Problem>(this, R.layout.my_problems, problems);

        setListAdapter(adapter);

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);
        
        lv.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) 
            {
              // When clicked, show a toast with the TextView text
              Toast.makeText(getApplicationContext(), (( TextView ) view).getText(),
                  Toast.LENGTH_SHORT).show();

              String problem_name = problems.get( position ).getProblemName();
              String problem_id = problems.get( position ).getProblemId();
              
              // Now put this stuff into the SharedPreferences
		      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( MyProblemsActivity.this);
		      prefs.edit().putString("recent_problem_id", problem_id ).commit();              
              
              // And go to problem intent
	          Intent myIntent = new Intent(MyProblemsActivity.this, ProblemActivity.class);
	          myIntent.putExtra("RecentProblemId", problem_id);
	          MyProblemsActivity.this.startActivity(myIntent);
            }
          });        
        
        // Check if person is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( MyProblemsActivity.this);
        final String user_id = prefs.getString( "user_id" , null ); 
        //final String session_name = prefs.getString( "first_name" , null );
        //final String session_email = prefs.getString( "email" , null );
        
        //sendEmail("My Businesses Loading", "Name: " + session_name + ", User_id: " + user_id + " , email: " + session_email );   	        

        
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {
            //sendEmail("My Businesses NULL ERROR Loading", "Name: " + session_name + ", User_id: " + user_id + " , email: " + session_email );   	        

			Log.d( "JSON ERRORS: " , "......NOT LOGGED IN"  );


			Toast.makeText(getApplicationContext(), "Not logged in.  Please login or create an account.", Toast.LENGTH_LONG).show();
            
	        Intent loginIntent = new Intent( MyProblemsActivity.this, LoginActivity.class);
	        MyProblemsActivity.this.startActivity(loginIntent);        	
        }        
        
        
//	    Button no_answer = (Button)findViewById(R.id.no_answer);              
//	    no_answer.setOnClickListener(new Button.OnClickListener() 
//	    {  
//	 	   public void onClick(View v) 
//	 	   {	 		   
//	 		   // Send me an email that a comment was submitted on a question. 
//	 	      //sendEmail("My Questions -> Add Question", "Someone clicked on add-question from my questions.  User id: " + user_id  );   
//	 		   	 		  
//              Intent myIntent = new Intent(MyProblemsActivity.this, ExtraHelpActivity.class);
//              MyProblemsActivity.this.startActivity(myIntent);    	 	  
//	 	   }
//	    });   
        
//        Button get_marketing_help = (Button)findViewById(R.id.get_marketing_help);  
//        get_marketing_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//           public void onClick(View v) 
//            {
//                //sendEmail("Add Problem From MyProblems", "From my problems page, user clicked on " );               		
//                
//                Intent myIntent = new Intent(MyProblemsActivity.this, WePromoteActivity.class);
//                MyProblemsActivity.this.startActivity(myIntent);
//            }
//        });        
        
        Button add_problem = (Button)findViewById(R.id.add_business);  
        add_problem.setOnClickListener(new Button.OnClickListener() 
        {  
           public void onClick(View v) 
            {
                //sendEmail("Add Problem From MyProblems", "From my problems page, user clicked on " );               		
                
                Intent myIntent = new Intent(MyProblemsActivity.this, AddProblemActivity.class);
                MyProblemsActivity.this.startActivity(myIntent);
            }
        });                

        Button enter_invite_code = (Button)findViewById(R.id.enter_invite_code);  
        enter_invite_code.setOnClickListener(new Button.OnClickListener() 
        {  
           public void onClick(View v) 
            {            	
                Intent myIntent = new Intent(MyProblemsActivity.this, EnterInviteCodeActivity.class);
                MyProblemsActivity.this.startActivity(myIntent);
            }
        });         
        
        // Go to the database and display a list of problems, all clickable.
        if ( user_id != null )
        {
			//Log.d( "JSON ERRORS: " , "............User id: " + user_id );
        	sendFeedback( user_id );   
        }
    }	
    
    public void sendFeedback(String user_id) 
    {  
        String[] params = new String[] { "https://www.problemio.com/problems/get_users_problems_mobile.php", user_id };

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }              

    
    public void onItemClick(AdapterView<?> items, View v, int x, long y)
    {
        Log.d( "onItemClick: " , "In the onItemClick method of MyProblemsActivity" );
    }
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(MyProblemsActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		        dialog.setTitle("Loading Businesses You Planned");

		      TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
		        text.setText("Please wait while your businesses load...");
		      dialog.show();
		 }     	
    	
		@Override
		protected String doInBackground(String... theParams) 
		{

			//Log.d( "JSON ERRORS: " , "......IN DO IN BACKGROUND"  );
	        String myUrl = theParams[0];
	        final String user_id = theParams[1];
	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("user_id=%s", 
		        	     URLEncoder.encode(user_id, charset));

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");

		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
//				Log.d( "JSON ERRORS: " , "some crap happened ****" + e.getMessage() );

				//sendEmail ( "MyProblemsActivity Network Error" , "Error: " + e.getMessage() );
		
			     connectionError = true;
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	      
            try 
            {
                dialog.dismiss();
            } catch (Exception e) {
                // nothing
            }
            
			if ( connectionError == true )
			{
			     Toast.makeText(getApplicationContext(), "Please try again. Possible Internet connection error.", Toast.LENGTH_LONG).show();	 				
			}
			else
	        if ( result != null && result.equals( "no_problems_for_user" ))
	        {
		        // Clear current page messaging
	        	problems.clear();
	        	
	            Problem p = new Problem ();
	            p.setProblemName( "You have not planned any businesses." );
	            
	            problems.add(p);
	            
	        	adapter.notifyDataSetChanged();		        			        	
	        	
//	           	Button add_problem_ok = (Button)findViewById(R.id.add_problem_ok);	           	
//	           	add_problem_ok.setOnClickListener(new Button.OnClickListener()
//	        	{  
//	        	   public void onClick(View v) 
//	        	   {
//	    		      Intent intent = new Intent( MyProblemsActivity.this, AddProblemActivity.class);
//	    		      MyProblemsActivity.this.startActivity(intent);        	
//	        	   }
//	        	});	        	
	        		        	
		        // No problems.  Display message that there are no problems.
	            //final TextView solution_title  = (TextView) findViewById(R.id.no_problems);
	        }
	        else
	        if ( result != null && result.equals("no_such_user" ))
	        {	            
		        // Show the user a message that they did not enter the right login
		        
		        Toast.makeText(getApplicationContext(), "We could not get your problems.", Toast.LENGTH_LONG).show();	
		     		        
	        }
	        else
	        {
		        String problem_title = null;
		        String problem_id = null;
		        
		        try
		        {
					//Log.d( "JSON ERRORS: " , "some crap happened **** 1"  );
		        	JSONArray obj = new JSONArray(result);
					//Log.d( "JSON ERRORS: " , "some crap happened **** 2"  );
		        	if ( obj != null )
		        	{
				        problems.clear();
		        		
		        		for ( int i = 0; i < obj.length(); i++ )
		        		{
		        			JSONObject o = obj.getJSONObject(i);

				            //Log.d( "Title: " , "" + o.getString("problem_title") );
				            //Log.d( "id: " , "" + o.getString("problem_id") );
		        	
				            problem_title = o.getString("problem_title");
				            problem_id = o.getString("problem_id");
				            String has_new_comments = o.getString("has_new_comments");
				            String is_private = o.getString("is_private");

				            Problem p = new Problem ( );
				            p.setProblemId(problem_id);				            
				            p.setProblemName(problem_title);
				            p.setIsPrivate(is_private);
				            p.setHasNewComments(has_new_comments);
				            
				            problems.add( p );
				            
//					        Log.d( "MyProblemsActivity" , "problem title: " + problem_title );	      
					        //Log.d( "MyProblemsActivity" , "problem id: " + problem_id );
					        					        
		        		}
		        		
		        		problem_loading_prompt.setText("");
		        		
				        adapter.notifyDataSetChanged();		        		
		        	}			        
		        }
		        catch ( Exception e )
		        {
					//Log.d( "JSON ERRORS: " , "some crap happened ****" + e.getMessage() );

		            //sendEmail( "MyProblemsError" , "Result: " + result + " and exception: " + e.getMessage() );
		        }
		    }
		}        
    }
    
    // Subject , body
    public void sendEmail( String subject , String body )
    {
        String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };

        SendEmail task = new SendEmail();
        task.execute(params);            	
    }      
}
