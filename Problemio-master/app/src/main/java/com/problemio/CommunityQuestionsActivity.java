package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.problemio.data.Question;

import utils.SendEmail;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CommunityQuestionsActivity extends BaseListActivity
{
	ArrayAdapter<Question> adapter;			
	ArrayList<Question> questions = new ArrayList <Question>( );		

	//private Dialog dialog;
	
    @Override
	public void onCreate(Bundle savedInstanceState) 
    {   
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.public_questions);
         
//        dialog = new Dialog(this);
//        dialog.setTitle("Loading Your Questions");
//        dialog.show();
//        TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
//        text.setText("Please wait while your questions load...");
        
        
	    // Make sure the user is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( CommunityQuestionsActivity.this);
        final String user_id = prefs.getString( "user_id" , null );	    
        
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {
            //sendEmail("MyQuestions USER_ID NULL, REDIRECTING", "Redirecting them to be logged in before they see this button. ");   	
        	
		    Toast.makeText(getApplicationContext(), "Not logged in.  Please login or create an account.", Toast.LENGTH_LONG).show();	
            
	        Intent loginIntent = new Intent( CommunityQuestionsActivity.this, LoginActivity.class);
	        CommunityQuestionsActivity.this.startActivity(loginIntent);        	
        }          

        adapter = new ArrayAdapter<Question>(this, R.layout.user_question_list, questions);
        
        setListAdapter ( adapter );
                
        ListView lv = getListView();
        lv.setTextFilterEnabled(true);
        
        lv.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) 
            {
              // When clicked, show a toast with the TextView text
              Toast.makeText(getApplicationContext(), (( TextView ) view).getText(),
                  Toast.LENGTH_SHORT).show();
              
              String question_name = questions.get( position ).getQuestion() + "";
              String question_id = questions.get( position ).getQuestionId() + "";
              
              // Now put this stuff into the SharedPreferences
		      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
		    		  CommunityQuestionsActivity.this);
		      		      
		      prefs.edit().putString( "recent_question_id" , question_id ).commit();
		      prefs.edit().putString( "recent_question" , question_name ).commit();
		      		      
              // And go to problem intent
	          Intent myIntent = new Intent(CommunityQuestionsActivity.this, QuestionActivity.class);
	          CommunityQuestionsActivity.this.startActivity(myIntent);
            }
          });                
               
        
	    
	    // Now make a call to server to get the questions for user.
	    
	    if ( user_id == null )
	    {
	    	//sendEmail ( "MyQuestionsError NULL ID" ,  "User id is null.  here is the email: " + email + " and name: + first_name.  Lets see what happens next....migth crash :)");
	    }
	    else
	    {
	    	sendFeedback( user_id );
	    }
    }

    public void sendFeedback( String user_id ) 
    {  
        String[] params = new String[] 
        		{ "https://www.problemio.com/problems/get_public_questions.php",
        		user_id };
        
        DownloadWebPageTask task = new DownloadWebPageTask();
        
        task.execute(params);        
    }  
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {	
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];
	        final String user_id = theParams[1];
	        	        
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        	        
		        final URL url = new URL( myUrl );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{

			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	        
//		    try 
//		    {
//		        dialog.dismiss();
//		    } catch (Exception ee) {
//		        // nothing
//		    }
			
			if ( result == null )
	        {
		        Toast.makeText(getApplicationContext(), "Could not get your questions. We are working to fix this. ", Toast.LENGTH_LONG).show();			     		        	        
	        }
	        else
	        if ( result.trim().equals( "database_error" ) )
	        {
//			    try {
//			        dialog.dismiss();
//			    } catch (Exception ee) {
//			        // nothing
//			    }
		        
		        Toast.makeText(getApplicationContext(), "Could not get your questions. We are working to fix this. ", Toast.LENGTH_LONG).show();			     		        
	        }
	        else
	        if ( result.trim().equals( "no_questions_by_user" ) )
	        {	        
//			    try {
//			        dialog.dismiss();
//			    } catch (Exception ee) {
//			        // nothing
//			    }
		        
	        	        
                TextView question_label = (TextView) findViewById(R.id.question_label);
                question_label.setText("There are no public questions at this time." );
	        }
	        else
	        {
//			    try {
//			        dialog.dismiss();
//			    } catch (Exception ee) {
//			        // nothing
//			    }
	        	
		            try
			        {	
			        	JSONArray obj = new JSONArray(result);
				        			        
			        	{
					        questions.clear();
			        
			        		for ( int i = 0; i < obj.length(); i++ )
			        		{
			        			JSONObject o = obj.getJSONObject(i);
	
					            String question_id = o.getString("question_id");
					            String question = o.getString("question");
					            String questioner_id = o.getString("member_id");
					            String first_name = o.getString("first_name");
					            			            
					            Question q = new Question ( );
					            q.setQuestion(question);
					            q.setQuestionId(question_id);
					            q.setQuestionByMemberId( questioner_id );
					            
					            q.setAuthorName(first_name );
					            
					            questions.add( q );
					        }
		        		}
					        
					    adapter.notifyDataSetChanged();
					    
				        TextView question_label = (TextView) findViewById(R.id.question_label);
				        question_label.setText("The public questions are below. Try to answer and help people.");
			        }			        
			        catch ( Exception e )
			        {
			        	//sendEmail ("Exeption in getting qestions" , "Exception: " + e.getMessage());

				        e.printStackTrace();
			        }
		        }
		        
		        //adapter.notifyDataSetChanged();		        		
		    }
		}        
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
}
