package com.problemio;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 12/17/15.
 */

public class BooksActivity extends BaseActivity
{
    /**
     * Called when this activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.books);

//        Button niche = (Button)findViewById(R.id.niche);
//        niche.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//
////                SharedPreferences prefs =
////                        PreferenceManager.getDefaultSharedPreferences(BooksActivity.this);
////
////                String user_id = prefs.getString( "user_id", null ); // First arg is name and second is if not found.
////                String first_name = prefs.getString( "first_name" , null );
//
//                //sendEmail ("Books-->Amazon" , "User_id: " + user_id + " and name: " + first_name + "-->FacebookMarketing" );
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.amazon.com/How-find-business-niche-Ideas-ebook/dp/B0197T77CS"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });

//        Button startbusiness = (Button)findViewById(R.id.startbusiness);
//        startbusiness.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Business-Start-up-Ideas-Comprehensive-successful/dp/1495261840/"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });

//        Button appbook = (Button)findViewById(R.id.appbook);
//        appbook.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Mobile-App-Marketing-Monetization-downloads/dp/1502383829"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//
//                startActivity(browserIntent);
//            }
//        });

//        Button card = (Button)findViewById(R.id.card);
//        card.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Business-Card-Marketing-Networking-ebook/dp/B018BD6T02"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });

//        Button flier = (Button)findViewById(R.id.flier);
//        flier.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Flier-Marketing-Flyers-Promote-Business-ebook/dp/B018HU86SM"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });

//        Button twitter = (Button)findViewById(R.id.twitter);
//        twitter.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Advanced-Twitter-Marketing-Automation-strategies-ebook/dp/B017T1T4FO"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });







//        Button facebook = (Button)findViewById(R.id.facebook);
//        facebook.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Facebook-Marketing-Business-Expect-Promoting-ebook/dp/B00LMGPDIK"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });
//
//        Button strategiesbook = (Button)findViewById(R.id.strategiesbook);
//        strategiesbook.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Marketing-Advertising-Strategy-Reach-People/dp/1495453588"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });



//
        Button businessplan = (Button)findViewById(R.id.businessplan);
        businessplan.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.amazon.com/Business-plan-template-example-business-ebook/dp/B0193L53IU"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });

        Button marketingplan = (Button)findViewById(R.id.marketingplan);
        marketingplan.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.amazon.com/Marketing-Plan-Template-Example-marketing/dp/1519712952"));

                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
                if (isChromeInstalled) {
                    browserIntent.setPackage("com.android.chrome");
                    startActivity(browserIntent);
                }else{
                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
                    startActivity(chooserIntent);
                }

                startActivity(browserIntent);
            }
        });

//        Button amznauthor = (Button)findViewById(R.id.amznauthor);
//        amznauthor.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://www.amazon.com/Alex-Genadinik/e/B00I114WEU"));
//
//                boolean isChromeInstalled = isPackageInstalled("com.android.chrome", BooksActivity.this);
//                if (isChromeInstalled) {
//                    browserIntent.setPackage("com.android.chrome");
//                    startActivity(browserIntent);
//                }else{
//                    Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
//                    startActivity(chooserIntent);
//                }
//
//                startActivity(browserIntent);
//            }
//        });


    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
        // your code
    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
