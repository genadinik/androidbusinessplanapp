package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.SendEmail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.flurry.android.FlurryAgent;

public class EditBusinessActivity extends BaseActivity
{
	private RadioGroup radioPrivacyGroup;
	RadioButton open;
	RadioButton closed;	
	
	EditText email;
	TextView email_ask;
	EditText name;
	TextView name_ask;	

	EditText business;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.edit_business);
                	      
        // Email me that this user has the edit loaded.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
		    		  EditBusinessActivity.this);              
		        
		final String user_id = prefs.getString( "user_id" , null ); 
		final String business_id = prefs.getString( "recent_problem_id" , null ); 
		final String business_name = prefs.getString( "recent_problem_name" , null ); 
		final String recent_problem_is_private = prefs.getString( "recent_problem_is_private" , null ); 
		final String session_email = prefs.getString( "email" , null ); 
		final String session_name = prefs.getString( "first_name" , null ); 
		
//	      open = (RadioButton)findViewById(R.id.radioOpen);
//	      closed = (RadioButton)findViewById(R.id.radioPrivate);		
		
		if ( recent_problem_is_private == null )
		{
//			   email.setVisibility(View.GONE);
//			   email_ask.setVisibility(View.GONE);
//			   name.setVisibility ( View.GONE );
//			   name_ask.setVisibility ( View.GONE );
			   
//	    	   closed.setChecked(true);			   
//	    	   open.setChecked(false);
		}
		else
		if ( recent_problem_is_private.equals( "1" ) )
		{
//			   email.setVisibility(View.VISIBLE);
//			   email_ask.setVisibility(View.VISIBLE);
//			   name.setVisibility ( View.VISIBLE );
//			   name_ask.setVisibility ( View.VISIBLE );			

//	    	   closed.setChecked(true);
//	    	   open.setChecked(false);
		}
		else
		{
//			   email.setVisibility(View.GONE);
//			   email_ask.setVisibility(View.GONE);
//			   name.setVisibility ( View.GONE );
//			   name_ask.setVisibility ( View.GONE );
			   
//	    	   open.setChecked(true);			   
//	    	   closed.setChecked(false);
		}
		
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {
           // sendEmail("Problem user_id Error", "User id is empty when browsing a problem. Here is " +
           // 		"their user_id: " + user_id );   	        	
        	
	        //Intent loginIntent = new Intent( ProblemActivity.this, LoginActivity.class);
	        //ProblemActivity.this.startActivity(loginIntent);        	
        }
        
        // Display the items.
        //final TextView prompt_to_expain_biz = (TextView) findViewById(R.id.prompt_to_expain_biz);

        business = (EditText) findViewById(R.id.problem_explanation);
        business.setText(business_name);   
        
    	radioPrivacyGroup = (RadioGroup) findViewById(R.id.radioPrivacy);
    	radioPrivacyGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
    	{
            public void onCheckedChanged(RadioGroup group, int checkedId) 
            { 
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                String temp = radioButton.getText() + "";
                
                if ( temp.equals("I want help planning"))
                {
       			   email.setVisibility(View.VISIBLE);
       			   email_ask.setVisibility(View.VISIBLE);
       			   name.setVisibility ( View.VISIBLE );
       			   name_ask.setVisibility ( View.VISIBLE );       			   
                }
                else
                {               
       			   email.setVisibility(View.GONE);
       			   email_ask.setVisibility(View.GONE);
       			   name.setVisibility ( View.GONE );
       			   name_ask.setVisibility ( View.GONE );
                }
            }
    	});    	    
    
	    email_ask = (TextView) findViewById(R.id.email_ask);
	    email = (EditText) findViewById(R.id.email);
	    name_ask = (TextView) findViewById(R.id.name_ask);
	    name = (EditText) findViewById(R.id.name);
	    
	    email.setVisibility(View.GONE);
	    email_ask.setVisibility(View.GONE);
	    name.setVisibility(View.GONE);
	    name_ask.setVisibility(View.GONE); 
	    
	    // Now pre-fill the entries
	    if ( session_email != null )
	    {
	    	email.setText(session_email);
	    	email.setEnabled(false);

	    	email_ask.setText("This is the email associated with your account.  If you wish to use a different email, please update your profile.");
	    }
	    
	    if ( session_name != null )
	    {
	    	name.setText(session_name);
	    }  	    	    
	    
	    // Enable the button
    	Button submit = (Button)findViewById(R.id.submit);
    	submit.setOnClickListener(new Button.OnClickListener()
    	{  
    	   public void onClick(View v) 
    	   {
		      Toast.makeText(getApplicationContext(), "Editing the business. Please wait...", Toast.LENGTH_LONG).show();	

    	      String business_name = business.getText().toString();
    	      String person_email = email.getText().toString();
    	      String person_name = name.getText().toString();
    	      
    	      // Put the persons SHarepPrefs email in there.
    	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
    	    		  EditBusinessActivity.this);
    	      
    	      prefs.edit()        
		        .putString("email", person_email)
		        .putString("first_name", person_name)
		        .commit();	 

    	      
    	 	  //Set the email pattern string
    		  Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
    		  //Match the given string with the pattern
    		  Matcher m = pattern.matcher(person_email);
    		  //check whether match is found
    		  boolean matchFound = m.matches();    	      

    	      open = (RadioButton)findViewById(R.id.radioOpen);
    	      closed = (RadioButton)findViewById(R.id.radioPrivate);
    	  
    	      if ( user_id == null )
    	      {    	    	
    		      Toast.makeText(getApplicationContext(), "You do not have an active session.  Please Log in or create an account.", Toast.LENGTH_LONG).show();	
    		     
    		      Intent loginIntent = new Intent( EditBusinessActivity.this, LoginActivity.class);
    		      EditBusinessActivity.this.startActivity(loginIntent);  
    	      }
    	      
    	      if(!open.isChecked() && !closed.isChecked())
    	      {
    		      Toast.makeText(getApplicationContext(), "Please choose whether you want help or privacy.", Toast.LENGTH_LONG).show();	
    	      }              
    	      else 	  
    	      if ( business_name == null || business_name.length() < 1 )
    	      {
    		      Toast.makeText(getApplicationContext(), "The business name can not be empty. Please try again.", 
    		    		  Toast.LENGTH_LONG).show();	
    		      
                  //sendEmail("Add Problem Submitted Error", "From add-problem page, submitted a business. " +
                  //		"The problem can not be empty" );       		      
    	      }
    	      else
    	      if ( open.isChecked() )
    	      {
    	    	  // TODO: validate name and email
    	    	  if ( person_name == null || person_name.length() < 1 )
    	    	  {
    	    		  Toast.makeText(getApplicationContext(), "Please enter your name.", Toast.LENGTH_LONG).show();	    	    		  
    	    	  }
    	    	  else
    	          if ( person_email == null || !matchFound )
    	          {
    	    		  Toast.makeText(getApplicationContext(), "Please enter a valid email address.", Toast.LENGTH_LONG).show();	    	    		  
    	          }
    	          else
    	          {
    	        	  sendFeedback( business_name.trim() , user_id , person_name , person_email , "0" , business_id);
    	          }
    	      }    	      
    	      else
    	      {
    	    	  sendFeedback( business_name.trim() , user_id , null , null , "1" , business_id);
    	      }
    	    }
    	});

		Button back_to_planning_button = (Button)findViewById(R.id.back_to_planning);
		back_to_planning_button.setOnClickListener(new Button.OnClickListener()
		{
			public void onClick(View v)
			{
				Intent myIntent = new Intent(EditBusinessActivity.this, ProblemActivity.class);
				EditBusinessActivity.this.startActivity(myIntent);
			}
		});

	}
	
    
    

    
    public void sendFeedback(String business_name , String user_id , String person_name , 
    		String person_email , String privacy , String business_id) 
    {          
        // Now that I have these items lets put them into the database
        
        if ( business_name != null && business_name.length() > 0 )
        {	
	        String[] params = new String[] 
	        		{ "https://www.problemio.com/problems/edit_business_mobile.php",
	        		business_name , user_id , person_name , person_email , privacy , business_id};
	             
	        DownloadWebPageTask task = new DownloadWebPageTask();
	        task.execute(params); 	        	        
        }
        else
        {
	        Log.d( "......EDITProblem" , "Some error, need to display an error." );
        	
        	// Display error message
        	TextView errorMessage = (TextView) findViewById(R.id.add_problem_validation_error);
        	errorMessage.setText(getResources().getString(R.string.add_problem_validation_error));   
        	
        }    	
    }      
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        final String business_name = theParams[1];
	        final String user_id = theParams[2];	        
	        final String person_name = theParams[3];
	        final String person_email = theParams[4];
	        final String privacy = theParams[5];
	        final String business_id = theParams[6];
	        
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("business_name=%s&user_id=%s&person_name=%s&person_email=%s&privacy=%s&business_id=%s", 
		        	     URLEncoder.encode( business_name , charset), 
		        	     URLEncoder.encode(user_id, charset) ,
		        	     URLEncoder.encode(person_name + "", charset) ,
		        	     URLEncoder.encode(person_email + "", charset) ,
		        	     URLEncoder.encode(privacy, charset) , 
		        	     URLEncoder.encode(business_id, charset)
		        		);

		        final URL url = new URL( myUrl + "?" + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
//	 		      sendEmail ( "Edit Business Network Error" , "Error: " + e.getMessage() );
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{
		      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
		    		  EditBusinessActivity.this);              
		        
		      String temp_user_id = prefs.getString( "user_id" , null ); 
		      String temp_business_id = prefs.getString( "recent_problem_id" , null );	
		      String name = prefs.getString( "first_name" , null );	
		      
		      
	        if ( result == null )
	        {
                //sendEmail("Add Problem Submitted Error", "Nothing returned from the server. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        
	        }
	        else
	        if ( result.equals( "no_business_name" ) )
	        {
                //sendEmail("Error editing business", "No business name. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        
                
  		        Toast.makeText(getApplicationContext(), "Error editing the business.  We are aware of this problem and it will be fixed in the next update of the app.", 
		    		  Toast.LENGTH_LONG).show();                
	        }		        
	        else
	        if ( result.equals( "not_logged_in" ) )
	        {
                //sendEmail("Error editing business", "Not logged in. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        
 
  		        Toast.makeText(getApplicationContext(), "Error editing the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                		        
	        }         
	        else
	        if ( result.equals( "no_business_id" ) )
	        {		        
                //sendEmail("Error editing business", "No business id. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        
                
  		        Toast.makeText(getApplicationContext(), "Error editing the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                		        		        
	        }
	        else 	
	        if ( result.equals( "get_business_error") )
	        {
                //sendEmail("DB Error editing business", "Db error getting the business. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        

  		        Toast.makeText(getApplicationContext(), "Error editing the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                    
	        }
	        else
	        if ( result.equals("no_business") )
	        {
                //sendEmail("Error editing business", "No such business. Name: " + name + " and user_id: " + temp_user_id + " and problem_id: " + temp_business_id );	        

  		        Toast.makeText(getApplicationContext(), "Error editing the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show(); 	        	
		    }
	        else
	        if ( result.equals("success") )
	        {              
  		        Toast.makeText(getApplicationContext(), "Successfully edited the business. Please wait to be redirected. ", 
		    		  Toast.LENGTH_LONG).show();                		        		        		        
		        
		        //sendEmail ( "Success editing business, redirecting" , "Cool....Name: " + name + " and user_id: " + temp_user_id + " and business_id: " + temp_business_id );
		        
		        // 2) Make an intent to go to the home screen
	            Intent myIntent = new Intent(EditBusinessActivity.this, ProblemActivity.class);
	            EditBusinessActivity.this.startActivity(myIntent);
	        }
	        else
	        {
		        //sendEmail ( "Strange thing happened editing business" , "Last else got executed :)....Name: " + name + " and user_id: " + temp_user_id + " and business_id: " + temp_business_id );	        	
	        }
		}    
    }    
 
    
		      
		      
    
    
	// Subject , body
//	public void sendEmail( String subject , String body )
//	{
//	    String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//	    SendEmail task = new SendEmail();
//	    task.execute(params);
//	}
	
	@Override
	public void onStop()
	{
	   super.onStop();
	   FlurryAgent.onEndSession(this);
	   // your code
	}    		      
}
