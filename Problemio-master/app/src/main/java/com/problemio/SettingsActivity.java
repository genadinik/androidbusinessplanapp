package com.problemio;

import com.flurry.android.FlurryAgent;
import com.problemio.content.TopMistakesActivity;

import utils.SendEmail;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SettingsActivity extends BaseActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.settings);

        // Get Shared Preferences.
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( SettingsActivity.this);
	    String user_id = prefs.getString( "user_id" , null );        

     	Button create_profile = (Button)findViewById(R.id.create_profile);  
        Button login = (Button)findViewById(R.id.login);  
        Button fill_out_info = (Button)findViewById(R.id.fill_out_info);  
        Button different_user = (Button)findViewById(R.id.different_user);  

        //Button help_the_app = (Button)findViewById(R.id.help_the_app);
        
        
        different_user.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	        	   
    			   //sendEmail("Settings To Sign In", "Sign in as a diff user chosen from settings page." );   
            
                Intent myIntent = new Intent( SettingsActivity.this, LoginActivity.class);
                SettingsActivity.this.startActivity(myIntent);              
            }
        }); 	         
        
        
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        help_the_app.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Settings TO Give Back", "" );  
//            	
//   	 	     builder.setMessage("To unlock premium features: See ads or download premium app")
//             .setCancelable(false)
//             .setPositiveButton("See Ads", new DialogInterface.OnClickListener() {
//                 public void onClick(DialogInterface dialog, int id) {
//      
//                     Intent myIntent = new Intent(SettingsActivity.this, GiveBackActivity.class);
//                     SettingsActivity.this.startActivity(myIntent);
//                     
//                     sendEmail( "See ads YES" , ":)");
//                 }
//             })
//             .setNegativeButton("Premium App", new DialogInterface.OnClickListener() 
//             {
//                 public void onClick(DialogInterface dialog, int id) 
//                 {
//                      dialog.cancel();
//                      
//	                      sendEmail( "Look at Premium App" , "");
//	                      
//	                      Intent intent = new Intent(Intent.ACTION_VIEW);
//	                  	intent.setData(Uri.parse("market://details?id=business.premium"));
//	                  	//startActivity(intent);
//	                  	            	 
//	                  	 try 
//	                  	 {
//	                  	        startActivity(intent);
//	                  	 } 
//	                  	 catch (ActivityNotFoundException anfe) 
//	                  	 {
//	                           sendEmail("Settings to Premium App ERROR", "From settings page, user clicked on premium app and this is the exception: " + anfe.getMessage() );   	
//	                  		
//	                           try
//	                           {
//	                          	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
//	                          	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	                          	 startActivity(next_intent);  
//	                           }
//	                           catch ( Exception e)
//	                           {
//	                               // Now try to redirect them to the web version:
//	                               Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
//	                               try
//	                               {
//	                              	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                              	 startActivity(webintent);
//	                               }
//	                               catch ( Exception except )
//	                               {
//	                                   sendEmail("Settings to premium app (2) ERROR", "From settings page, user clicked on web version of give review and this is the exception: " + except.getMessage() );   	                    	 
//	                               }
//	                           }
//	                  	 }	                      
//                 }
//             });
//      AlertDialog alert = builder.create();
//      alert.show();		       		         	 	                  	
//          }
//        });	 	     	          

        

             
        
                
	    
        if ( user_id == null )
        {
        	// Not logged in, put in
	       	 create_profile.setOnClickListener(new Button.OnClickListener() 
	         {  
	               public void onClick(View v) 
	               {       
	       	         
	                 Intent myIntent = new Intent(SettingsActivity.this, CreateProfileActivity.class);
	                 SettingsActivity.this.startActivity(myIntent);
	               }
	           });      

	           login.setOnClickListener(new Button.OnClickListener() 
	           {  
	               public void onClick(View v) 
	               {            			       	        
	                 Intent myIntent = new Intent(SettingsActivity.this, LoginActivity.class);
	                 SettingsActivity.this.startActivity(myIntent);
	               }
	           });   	       	 


	           fill_out_info.setVisibility(View.GONE);        
	           different_user.setVisibility(View.GONE);      
	           
	           
	            
        }
        else
        {
        	// Not logged in
	       	create_profile.setVisibility(View.GONE);        
 	        login.setVisibility(View.GONE);        

 	        fill_out_info.setOnClickListener(new Button.OnClickListener() 
            {  
               public void onClick(View v) 
               {                 	
                    Intent myIntent = new Intent(SettingsActivity.this, UpdateProfileActivity.class);
                    SettingsActivity.this.startActivity(myIntent);
               }
            });  	        


        }

        
         
//        Button give_feedback = (Button)findViewById(R.id.give_feedback);  
//        give_feedback.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {            	        	               
//                Intent myIntent = new Intent( SettingsActivity.this, FeedbackActivity.class);
//                SettingsActivity.this.startActivity(myIntent);              
//            }
//        });           
        
        Button home = (Button)findViewById(R.id.home);  
        home.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {            	        	   
    			   //sendEmail("Settings To Home", "Home page chosen from settings page." );   
            
                Intent myIntent = new Intent( SettingsActivity.this, ProblemioActivity.class);
                SettingsActivity.this.startActivity(myIntent);              
            }
        });   
        
        
    }
    
    

    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }    
}
