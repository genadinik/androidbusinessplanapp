package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.google.android.gcm.GCMRegistrar;
import com.problemio.content.HelpInstructionsActivity;
import com.problemio.content.PremiumWebAdvertisingActivity;
import com.problemio.content.TopMistakesActivity;

import utils.Consts;
import utils.SendEmail;
import utils.Consts.PurchaseState;
import utils.Consts.ResponseCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class AddProblemActivity extends BaseActivity 
{    
	String business_name = null;
	
	EditText email;
	TextView email_ask;
	EditText name;
	TextView name_ask;
	
	private RadioGroup radioPrivacyGroup;
	RadioButton open;
	RadioButton closed;
	
	Button addBusinessButton;
	Button premiumAppButton;

	TextView premium_text;
	
    Button submit;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.add_problem);
        
        // Check if person is logged in
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AddProblemActivity.this);
        final String user_id = prefs.getString( "user_id" , null ); // First arg is name and second is if not found.
        final String session_name = prefs.getString( "first_name" , null );
        final String session_email = prefs.getString( "email" , null );        
                
        // If the user is not logged in, send them to log in
        if ( user_id == null )
        {
        	  Toast.makeText(getApplicationContext(), "Please log in.", Toast.LENGTH_LONG).show();	            	              
            
	        Intent loginIntent = new Intent( AddProblemActivity.this, LoginActivity.class);
	        AddProblemActivity.this.startActivity(loginIntent);        	
        }
           
        
	    email_ask = (TextView) findViewById(R.id.email_ask);
	    email = (EditText) findViewById(R.id.email);
	    name_ask = (TextView) findViewById(R.id.name_ask);
	    name = (EditText) findViewById(R.id.name);
	    
	    email.setVisibility(View.GONE);
	    email_ask.setVisibility(View.GONE);
	    name.setVisibility(View.GONE);
	    name_ask.setVisibility(View.GONE);
	    
    	addBusinessButton = (Button)findViewById(R.id.button_send_feedback);
    	
    	premiumAppButton = (Button)findViewById(R.id.premium_button);

    	premiumAppButton.setVisibility(View.GONE);
    	//subscribe.setVisibility(View.GONE);
    	
    	premium_text = (TextView) findViewById(R.id.premium_text);
    	premium_text.setVisibility(View.GONE);
    	
	    // Now pre-fill the entries
	    if ( session_email != null && session_email.trim().length() > 2)
	    {
	    	email.setText(session_email);
	    	email.setEnabled(false);

	    	email_ask.setText("This is the email associated with your account.  If you wish to use a different email, please update your profile.");
	    }
	    
	    if ( session_name != null )
	    {
	    	name.setText(session_name);
	    }  	    
	    
	    final TextView question_label = (TextView) findViewById(R.id.prompt_to_expain_biz);     
    	final EditText problemName = (EditText) findViewById(R.id.problem_explanation);  
    	business_name = problemName.getText().toString();  
    	
    	radioPrivacyGroup = (RadioGroup) findViewById(R.id.radioPrivacy);
    	radioPrivacyGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
    	{
            public void onCheckedChanged(RadioGroup group, int checkedId) 
            { 
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                String temp = radioButton.getText() + "";
                
                if ( temp.equals("I want help planning"))
                {       
                   try
                   {
	   	       			   email.setVisibility(View.GONE);
	   	       			   email_ask.setVisibility(View.GONE);
	   	       			   name.setVisibility ( View.GONE );
	   	       			   name_ask.setVisibility ( View.GONE );   
	   	       			   addBusinessButton.setVisibility(View.GONE);
	   	       			   premiumAppButton.setVisibility(View.VISIBLE); 
	   	       			   premium_text.setVisibility(View.VISIBLE);              
	   	       			   //subscribe.setVisibility(View.VISIBLE);   	       			   
                   }
                   catch ( Exception e )
                   {
	       			   email.setVisibility(View.GONE);
	       			   email_ask.setVisibility(View.GONE);
	       			   name.setVisibility ( View.GONE );
	       			   name_ask.setVisibility ( View.GONE );   
	       			   addBusinessButton.setVisibility(View.GONE);
	       			   premiumAppButton.setVisibility(View.VISIBLE); 
	       			   premium_text.setVisibility(View.VISIBLE);   
	       			   //subscribe.setVisibility(View.VISIBLE);
	       		   }

                }
                else
                {               
       			   email.setVisibility(View.GONE);
       			   email_ask.setVisibility(View.GONE);
       			   name.setVisibility ( View.GONE );
       			   name_ask.setVisibility ( View.GONE );
       			   addBusinessButton.setVisibility(View.VISIBLE);       			  
       			   premiumAppButton.setVisibility(View.GONE);
       			   premium_text.setVisibility(View.GONE);       			   
       			   //subscribe.setVisibility(View.GONE);       			   

                }
            }
    	});

    	
	    premiumAppButton.setOnClickListener(new Button.OnClickListener() 
	      {  
	          public void onClick(View v) 
	          {

				  Intent myIntent = new Intent(AddProblemActivity.this, ExtraHelpActivity.class);
				  AddProblemActivity.this.startActivity(myIntent);

//	            Intent intent = new Intent(Intent.ACTION_VIEW);
//	        	intent.setData(Uri.parse("market://details?id=business.premium"));
//
//	        	 try
//	        	 {
//	        	        startActivity(intent);
//	        	 }
//	        	 catch (ActivityNotFoundException anfe)
//	        	 {
//	                 try
//	                 {
//	                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
//	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	                	 startActivity(next_intent);
//	                 }
//	                 catch ( Exception e)
//	                 {
//	                     // Now try to redirect them to the web version:
//	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
//	                     try
//	                     {
//	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                    	 startActivity(webintent);
//	                     }
//	                     catch ( Exception except )
//	                     {
//
//	                     }
//	                 }
//	        	 }
	        }
	      });	 	        		    	
    	
    	
    	addBusinessButton.setOnClickListener(new Button.OnClickListener()
    	{  
    	   public void onClick(View v) 
    	   {
		      Toast.makeText(getApplicationContext(), "Adding the business. Please wait...", Toast.LENGTH_LONG).show();	

    	      String business_name = problemName.getText().toString().trim();
    	      String person_email = email.getText().toString().trim();
    	      String person_name = name.getText().toString().trim();
    	      
    	      // Put the persons SHarepPrefs email in there.
    	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
    	    		  AddProblemActivity.this);
    	      
    	      prefs.edit()        
		        .putString("email", person_email)
		        .putString("first_name", person_name)
		        .putString("temp_business_name", business_name )
		        .commit();	 
    	      
    	 	  //Set the email pattern string
    		  Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
    		  //Match the given string with the pattern
    		  Matcher m = pattern.matcher(person_email);
    		  //check whether match is found
    		  boolean matchFound = m.matches();    	      

    	      open = (RadioButton)findViewById(R.id.radioOpen);
    	      closed = (RadioButton)findViewById(R.id.radioPrivate);
    	  
    	      if ( user_id == null )
    	      {
    		      Toast.makeText(getApplicationContext(), "You do not have an active session.  Please Log in or create an account.", Toast.LENGTH_LONG).show();	
    		     
    		      Intent loginIntent = new Intent( AddProblemActivity.this, LoginActivity.class);
    		      AddProblemActivity.this.startActivity(loginIntent);  
    	      }
    	      
    	      if(!open.isChecked() && !closed.isChecked())
    	      {  
    		      Toast.makeText(getApplicationContext(), "Please choose whether you want help or privacy.", Toast.LENGTH_LONG).show();	
    	      }              
    	      else 	  
    	      if ( business_name == null || business_name.length() < 1 )
    	      {  
    		      Toast.makeText(getApplicationContext(), "The business field can not be empty. Please try again.", 
    		    		  Toast.LENGTH_LONG).show();	      		      
    	      }
    	      else
    	      if ( open.isChecked() )
    	      {
    	    	  if ( person_name == null || person_name.length() < 1 )
    	    	  {
    	    		  Toast.makeText(getApplicationContext(), "Please enter your name.", Toast.LENGTH_LONG).show();	    	    		  
    	    	  }
    	    	  else
    	          if ( person_email == null || !matchFound )
    	          {
    	    		  Toast.makeText(getApplicationContext(), "Please enter a valid email address.", Toast.LENGTH_LONG).show();	    	    		  
    	          }
    	          else
    	          {    		 	     
//    		 	     String business = prefs.getString( "business" , null );
    		 	          	             
    			     sendFeedback( business_name.trim() , user_id , person_name , person_email , "0");    	        	  
    			  }    		      
    	      }    	      
    	      else
    	      {    	    	  
    	    	  sendFeedback( business_name.trim() , user_id , null , null , "1");
    	      }
    	    }
    	});
    }

    public void sendFeedback(String business_name , String user_id , String person_name , 
    		String person_email , String privacy) 
    {          
        // Now that I have these items lets put them into the database
        
        if ( business_name != null && business_name.length() > 0 )
        {	
	        String[] params = new String[] 
	        		{ "https://www.problemio.com/problems/add_problem_ajax_mobile.php?platform=android_probemio&",
	        		business_name , user_id , person_name , person_email , privacy};
	             
	        DownloadWebPageTask task = new DownloadWebPageTask();
	        task.execute(params); 	        	        
        }
        else
        {
        	// Display error message
        	TextView errorMessage = (TextView) findViewById(R.id.add_problem_validation_error);
        	errorMessage.setText(getResources().getString(R.string.add_problem_validation_error));          	
        }    	
    }      
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		private boolean connectionError = false;
		 
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        final String business_name = theParams[1];
	        final String user_id = theParams[2];
	        final String person_name = theParams[3];
	        final String person_email = theParams[4];
	        final String privacy = theParams[5];
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        String query = String.format("name=%s&user_id=%s&person_name=%s&person_email=%s&privacy=%s", 
		        	     URLEncoder.encode( business_name , charset), 
		        	     URLEncoder.encode(user_id, charset) ,
		        	     URLEncoder.encode(person_name + "", charset) ,
		        	     URLEncoder.encode(person_email + "", charset) ,
		        	     URLEncoder.encode(privacy, charset)  
		        		);

		        final URL url = new URL( myUrl + query );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
					//.printStackTrace();
					connectionError = true;
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{
			//Log.d( "CONNECTION*** ERRORS: " , ".....4, result: " + result  );
			//Log.d( "CONNECTION*** ERRORS: " , ".....45, result: ");

			if ( connectionError )
			{
		        Toast.makeText(getApplicationContext(), "Error: Internet conection problem. Please try again and make sure your phone is connected to the Internet." , Toast.LENGTH_LONG).show();
			}
			else
	        if ( result != null && result.equals( "no_problem_name" ) )
	        {		                        
  		        Toast.makeText(getApplicationContext(), "Error submitting the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                
	        }        
	        else
	        if ( result != null && result.equals( "error_adding_problem" ) )
	        {		        
//                sendEmail("Add Problem Submitted Error", "From add-problem page, after submitted a business. " +
//                		"There was a database error adding the problem" );
                
  		        Toast.makeText(getApplicationContext(), "Error submitting the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                		        		        
	        }
	        else
	        if ( result != null && result == "error_duplicate_problem")
	        {
		        //Log.d( "AddProblemActivity" , "Error - duplicate problem" );		        
            }
	        else
	        if ( result != null && result == "not_logged_in")
	        {	        	
               // sendEmail("Add Problem Submitted Error", "From add-problem page, after submitted a business. " +
                //		"There was an error adding the problem. User id was not passed in right." );
                
  		        Toast.makeText(getApplicationContext(), "Error submitting the business.  We are aware of this problem and it will be fixed in the next update of the app.  We sincerely apologize.", 
		    		  Toast.LENGTH_LONG).show();                		        		        	        	
		        
	        	// TODO take this user to the login screen.
     	        Intent myIntent = new Intent(AddProblemActivity.this, LoginActivity.class);
     	        AddProblemActivity.this.startActivity(myIntent);	        	
		    }
	        else
	        {                
  		        Toast.makeText(getApplicationContext(), "Successfully submitted the business.  " +
  		        		"Please wait to be redirected. ", 
		    		  Toast.LENGTH_LONG).show();                		        		        		        
		       
		        // 1) First, write to whatever local session file that the person is logged in
		        // - I just really need user id and name and email. And store that.
		        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( AddProblemActivity.this);
		        prefs.edit().putString("recent_problem_id", result ).commit();
		        
		        // 2) Make an intent to go to the home screen
	            Intent myIntent = new Intent(AddProblemActivity.this, ProblemActivity.class);
	            //myIntent.putExtra("RecentProblemId", result);
	            AddProblemActivity.this.startActivity(myIntent);
	        }
		}    
    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }       
}
