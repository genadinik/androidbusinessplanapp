package com.problemio;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

import utils.SendEmail;

/**
 * Created by alexgenadinik on 12/18/15.
 */
public class CoachingActivity extends BaseActivity
{
    /**
     * Called when this activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    // TODO: start here
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");

        setContentView(R.layout.coaching);

//        Button coachingask = (Button)findViewById(R.id.coachingask);
//        coachingask.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//                Intent myIntent = new Intent(CoachingActivity.this, MyQuestionsActivity.class);
//                CoachingActivity.this.startActivity(myIntent);
//            }
//        });

//        Button coaching = (Button)findViewById(R.id.coachingskype);
//        coaching.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//
//
//
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("https://www.fiverr.com/genadinik/give-you-a-business-coaching-and-business-plan-skype-session-for-30-minutes"));
//
//                startActivity(browserIntent);
//            }
//        });

        Button me = (Button)findViewById(R.id.me);
        me.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(CoachingActivity.this, AboutActivity.class);
                CoachingActivity.this.startActivity(myIntent);
            }
        });
    }

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
        // your code
    }
}
