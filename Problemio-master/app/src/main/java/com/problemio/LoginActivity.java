package com.problemio;

import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.InputStreamReader;
import java.io.BufferedReader;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;

import utils.SendEmail;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends BaseActivity 
{
	//private TextView textView;
	private Dialog dialog;

	public static final String REQUEST_METHOD = "GET";
	public static final int READ_TIMEOUT = 15000;
	public static final int CONNECTION_TIMEOUT = 15000;
	
    @Override
	public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.login);        
            
        //final TextView emailask = (TextView) findViewById(R.id.email_ask);
        
        // Show form for login_email
    	final EditText loginEmail = (EditText) findViewById(R.id.login_email);  
    	//String name = loginEmail.getText().toString();

    	// Show field for password  
    	final EditText password = (EditText) findViewById(R.id.password);
    	//String text = password.getText().toString();
		//Log.d( "First parameters: " , "Login email: " + loginEmail + " AND login password: " +  text);


		// Show button for submit
        Button submit = (Button)findViewById(R.id.submit);   
                
        submit.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
     	      String email = loginEmail.getText().toString();
     	      String pass = password.getText().toString();

			   //Set the email pattern string
//    		  Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
//    		  //Match the given string with the pattern
//    		  Matcher m = pattern.matcher(email);
//    		  //check whether match is found
//    		  boolean matchFound = m.matches();        	      
     	      
     		  //sendEmail("Login attempted", "This is the login email: " + email);   

     		  // TODO: VALIDATE!!!
     		  if ( email == null || email.trim().length() < 2 )
     		  {
	    		  Toast.makeText(getApplicationContext(), "Please enter a valid email address.", Toast.LENGTH_LONG).show();	    	    		  
     		  }
     		  else
     		  if ( pass == null || pass.trim().length() < 2 )
     		  {
     			  Toast.makeText(getApplicationContext(), "Please enter a correct password.", Toast.LENGTH_LONG).show();	    	    		  
     		  }
     		  else
     		  {
     			 sendFeedback(pass, email); 
     		  }  
     	   }
        });        

        // Show button for submit
        Button forgot_password = (Button)findViewById(R.id.forgot_password);   
                
        forgot_password.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
			  Toast.makeText(getApplicationContext(), "Please wait...", Toast.LENGTH_LONG).show();	
     				  
	          Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
	          LoginActivity.this.startActivity(intent);			  
     	   }
        });                
        
        // Now add messaging for creating a profile
        final TextView create_profile_message = (TextView) findViewById(R.id.create_profile_message);
        
        Button create_profile = (Button)findViewById(R.id.create_profile);   

        create_profile.setOnClickListener(new Button.OnClickListener() 
        {  
     	   public void onClick(View v) 
     	   {
      		    //sendEmail("Create Profile Clicked", "From Login screen, someone clicked on the create profile button" );   
     		   
     	        Intent myIntent = new Intent(LoginActivity.this, CreateProfileActivity.class);
     	        LoginActivity.this.startActivity(myIntent);
     	   }
        });        
        
    }
    
    public void sendFeedback(String pass , String email) 
    {  
        String[] params = new String[] { "https://www.problemio.com/auth/mobile_login.php", email, pass };

        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute(params);        
    }          
    
    // Subject , body
    public void sendEmail( String subject , String body )
    {
        String[] params = new String[] { "https://www.problemio.com/problems/send_email_mobile.php", subject, body };

        SendEmail task = new SendEmail();
        task.execute(params);            	
    }
    
    public class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		 private boolean connectionError = false;
	    	
		 @Override
		 protected void onPreExecute( ) 
		 {
			  dialog = new Dialog(LoginActivity.this);

		      dialog.setContentView(R.layout.please_wait);
		        dialog.setTitle("Logging You In");

		      TextView text = (TextView) dialog.findViewById(R.id.please_wait_text);
		        text.setText("Please wait while you are being logged in...");
		      dialog.show();
		 }


//		@Override
//		protected String doInBackground(String... theParams)
//		{
//			String myUrl = theParams[0];
//			final String myEmail = theParams[1];
//			final String myPassword = theParams[2];
//
//			String charset = "UTF-8";
//
//			Authenticator.setDefault(new Authenticator()
//			{
//				@Override
//				protected PasswordAuthentication getPasswordAuthentication()
//				{
//					return new PasswordAuthentication( myEmail, myPassword.toCharArray());
//				}
//			});
//
//			String response = null;
//
//			String stringUrl = "https://www.problemio.com/auth/mobile_login.php?login=test.name@gmail.com&password=130989";
//			String result = "";
//			String inputLine;
//
//			try
//			{
//				String query = String.format("login=%s&password=%s",
//						URLEncoder.encode(myEmail, charset),
//						URLEncoder.encode(myPassword, charset));
//
//				stringUrl = myUrl + "?" + query;
//
//				//final URL url = new URL( myUrl + "?" + query );
//
//				Log.d( "JSON ERRORS XYZ: " , "This is the query being sent: " + stringUrl );
//
//				//Create a URL object holding our url
//				URL MyURL = new URL(stringUrl);
//
//				//Create a connection
//				HttpURLConnection connection = (HttpURLConnection)
//						MyURL.openConnection();
//				//Set methods and timeouts
//				connection.setRequestMethod(REQUEST_METHOD);
//				connection.setReadTimeout(READ_TIMEOUT);
//				connection.setConnectTimeout(CONNECTION_TIMEOUT);
//
//				//Connect to our url
//				connection.connect();
//				//Create a new InputStreamReader
//				InputStreamReader streamReader = new
//						InputStreamReader(connection.getInputStream());
//				//Create a new buffered reader and String Builder
//				BufferedReader reader = new BufferedReader(streamReader);
//				StringBuilder stringBuilder = new StringBuilder();
//				//Check if the line we are reading is not null
//				while ((inputLine = reader.readLine()) != null) {
//					stringBuilder.append(inputLine);
//				}
//				//Close our InputStream and Buffered reader
//				reader.close();
//				streamReader.close();
//				//Set our result equal to our stringBuilder
//				result = stringBuilder.toString();
//
//			}
//			catch (Exception e)
//			{
//				sendEmail ( "Login Activity 1 Network Error" , "Error: " + e.getMessage() );
//			}
//
//			return result;
//		}


		 // orig
		@Override
		protected String doInBackground(String... theParams)
		{
	        String myUrl = theParams[0];
	        final String myEmail = theParams[1];
	        final String myPassword = theParams[2];

	        String charset = "UTF-8";

	        Authenticator.setDefault(new Authenticator()
	        {
	            @Override
				protected PasswordAuthentication getPasswordAuthentication()
	            {
	                return new PasswordAuthentication( myEmail, myPassword.toCharArray());
	            }
	        });

	        String response = null;

			String stringUrl = "https://www.problemio.com/auth/mobile_login.php?login=test.name@gmail.com&password=130989";
			String result = "";
			String inputLine;

			try
			{
		        String query = String.format("login=%s&password=%s",
		        	     URLEncoder.encode(myEmail, charset),
		        	     URLEncoder.encode(myPassword, charset));

		        final URL url = new URL( myUrl + "?" + query );

				//Log.d( "JSON ERRORS XYZ: " , "This is the query being sent: " + url.toString() );

		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		        conn.setDoOutput(false);
		        conn.setRequestMethod("GET");

//		        conn.setRequestProperty("login", myEmail);
//		        conn.setRequestProperty("password", myPassword);
//		        conn.setDoOutput(true);

		        conn.setUseCaches(false);

		        conn.connect();

		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1)
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();

				//Log.d( "JSON ERRORS: " , "This is the response: " + response );

			}
			catch (Exception e)
			{
	 		      sendEmail ( "Login Activity 1 Network Error" , "Error: " + e.getMessage() );
			}

			return response;
		}

		
		
		@Override
		protected void onPostExecute(String result) 
		{
			super.onPostExecute(result);

			try {
		        dialog.dismiss();
		    } catch (Exception ee) {
		        // nothing
		    }			
			
			if ( connectionError == true )
			{
				Toast.makeText(getApplicationContext(), "Please try again. Possible Internet connection error.", Toast.LENGTH_LONG).show();	 
			}			
			
	        if ( result != null && result.equals( "no_such_user") )
	        {
				//Log.d( "CONNECTION*** ERRORS: " , "no such user 1"  );
		        Toast.makeText(getApplicationContext(), "Your email and password do not match out records. Please try again or create and account.", Toast.LENGTH_LONG).show();	
		        
		        //final TextView login_error = (TextView) findViewById(R.id.login_error);
	        }
	        else
	        {
				//Log.d( "CONNECTION*** ERRORS: " , "ok 3 and result length: " + result.length()  );

		        String firstName = null;
		        String lastName = null;
		        String email = null;
		        String user_id = null;
		        
		        try
		        {
					//Log.d( "CONNECTION*** ERRORS: " , ".....4, result: " + result  );

					JSONArray obj = new JSONArray(result);

					JSONObject o = obj.getJSONObject(0);
					firstName = o.getString("first_name");
					//Log.d( "CONNECTION*** ERRORS: " , ".....7 and userId: " +  firstName  );
					//lastName = o.getString("last_name");
					//Log.d( "CONNECTION*** ERRORS: " , ".....8 and userId: " +  lastName );
					email = o.getString("email");
					//Log.d( "CONNECTION*** ERRORS: " , ".....9 and userId: " +  email  );
					user_id = o.getString("user_id");
					//Log.d( "CONNECTION*** ERRORS: " , ".....10 and userId: " +  user_id );
		        }
		        catch ( Exception e )
		        {
			        Log.d( "JSON ERRORS: " , "some crap happened ****" + e.getMessage() );
		        }
		        
		        
		        // 1) First, write to whatever local session file that the person is logged in
		        // - I just really need user id and name and email. And store that.
		        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( 
		        		LoginActivity.this);
		        
		        if ( user_id != null && user_id.trim().length() > 0 && !user_id.trim().equals("null") )
		        {
			        prefs.edit()
			        .putString("first_name", firstName)
			        .putString("last_name", lastName)
			        .putString("email", email)		        
			        .putString("user_id", user_id)
			        .putBoolean("working", true)
			        .commit();
			        
//			        if ( user_id.equals("1"))
//			        {
//				        prefs.edit()
//				        .putString("community_subscription", "1")
//				        .commit();
//			        }
		        }

		        // TODO: Create success message
		        // 2) Make an intent to go to the home screen
	            //Intent myIntent = new Intent(LoginActivity.this, ProblemioActivity.class);
	            //LoginActivity.this.startActivity(myIntent);

//				AlertDialog alertDialog = new AlertDialog.Builder(this)
////set title
//						.setTitle("Are you sure to Exit")
////set message
//						.setMessage("Exiting will call finish() method")
////set positive button
//						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialogInterface, int i) {
//								//set what would happen when positive button is clicked
//								finish();
//							}
//						})
////set negative button
//
//						.show();
	        }
		}    
    }

    // TODO: see if I can get rid of this
	public void readWebpage(View view) 
	{


		DownloadWebPageTask task = new DownloadWebPageTask();
		task.execute(new String[] { "https://www.problemio.com/auth/mobile_login.php" });
	}     	
	
    @Override
	public void onStop()
    {
       super.onStop();
       FlurryAgent.onEndSession(this);
       // your code
    }    	
}
