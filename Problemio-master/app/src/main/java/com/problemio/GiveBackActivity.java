package com.problemio;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import utils.SendEmail;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
//import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.problemio.R;
import com.problemio.content.FundraisingActivity;
import com.problemio.content.PsychologyActivity;
import com.problemio.content.TopMistakesActivity;


// Articles:
// Mobile app
// Restaurant
//      Clothing product
//        Local service business
//        Store
//        Home based business


public class GiveBackActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
	    setContentView(R.layout.content_give_back);
        
        Button asking = (Button)findViewById(R.id.asking);
		asking.setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(GiveBackActivity.this, ExtraHelpActivity.class);
                GiveBackActivity.this.startActivity(myIntent);

            }
        });
	    
        Button allbooks = (Button)findViewById(R.id.allbooks);
        allbooks.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {	            	          	
  	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
  	          		  Uri.parse("https://www.amazon.com/Business-plan-template-example-business/dp/1519741782/"));
  	            startActivity(browserIntent);
            }
        });      
        
        Button courses = (Button)findViewById(R.id.courses);          
        courses.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {        	
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
            		  Uri.parse("https://www.udemy.com/u/alexgenadinik/"));
              
              startActivity(browserIntent);
            }
        });  
        
        Button book = (Button)findViewById(R.id.book); 
        book.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {       	
                Intent myIntent = new Intent(GiveBackActivity.this, BookActivity.class);
                GiveBackActivity.this.startActivity(myIntent);

            }
        });

        // Business plan course
	    Button tube = (Button)findViewById(R.id.tube);
		tube.setOnClickListener(new Button.OnClickListener()
 		{
 		        public void onClick(View v)
 		        {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW,
							Uri.parse("https://www.udemy.com/course/how-to-write-a-business-plan/?referralCode=D66A2AFD0CC9CFC4278F"));

					boolean isChromeInstalled = isPackageInstalled("com.android.chrome", GiveBackActivity.this);
					if (isChromeInstalled) {
						browserIntent.setPackage("com.android.chrome");
						startActivity(browserIntent);
					}else{
						Intent chooserIntent = Intent.createChooser(browserIntent, "Select Application");
						startActivity(chooserIntent);
					}

					startActivity(browserIntent);
 		        }
 		});
	    
	    
//	    Button marketing_app = (Button)findViewById(R.id.marketing_app);
//	    Button fundraising = (Button)findViewById(R.id.fundraising_app);

		Button businessplan = (Button)findViewById(R.id.business_plan_vide_examples);
		businessplan.setOnClickListener(new Button.OnClickListener()
		{
			public void onClick(View v)
			{
				// Add url_to_watch in local storage and go to video screen
				SharedPreferences prefs =
						PreferenceManager.getDefaultSharedPreferences( GiveBackActivity.this);

//				prefs.edit()
//						.putString("url_to_watch", "https://youtu.be/waZojpDfedU")
//						.commit();

				Intent myIntent = new Intent(GiveBackActivity.this, VideoListActivity.class);
				GiveBackActivity.this.startActivity(myIntent);

			}
		});
	    
//		fundraising.setOnClickListener(new Button.OnClickListener()
// 		{
// 		          public void onClick(View v)
// 		          {
// 		            Intent intent = new Intent(Intent.ACTION_VIEW);
// 		        	intent.setData(Uri.parse("market://details?id=com.fundraising"));
//
// 		        	 try
// 		        	 {
// 		        	        startActivity(intent);
// 		        	 }
// 		        	 catch (ActivityNotFoundException anfe)
// 		        	 {
// 		                 try
// 		                 {
// 		                	 Uri uri = Uri.parse("market://search?q=pname:com.fundraising");
// 		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
// 		                	 startActivity(next_intent);
// 		                 }
// 		                 catch ( Exception e)
// 		                 {
// 		                     // Now try to redirect them to the web version:
// 		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.fundraising");
// 		                     try
// 		                     {
// 		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
// 		                    	 startActivity(webintent);
// 		                     }
// 		                     catch ( Exception except )
// 		                     {
//
// 		                     }
// 		                 }
// 		        	 }
// 		        }
// 		      });
//
//
//
// 		      marketing_app.setOnClickListener(new Button.OnClickListener()
// 		      {
// 		          public void onClick(View v)
// 		          {
// 		            Intent intent = new Intent(Intent.ACTION_VIEW);
// 		        	intent.setData(Uri.parse("market://details?id=com.marketingpremium"));
//
// 		        	 try
// 		        	 {
// 		        	        startActivity(intent);
// 		        	 }
// 		        	 catch (ActivityNotFoundException anfe)
// 		        	 {
// 		                 try
// 		                 {
// 		                	 Uri uri = Uri.parse("market://search?q=pname:com.marketingpremium");
// 		                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
// 		                	 startActivity(next_intent);
// 		                 }
// 		                 catch ( Exception e)
// 		                 {
// 		                     // Now try to redirect them to the web version:
// 		                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketingpremium");
// 		                     try
// 		                     {
// 		                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
// 		                    	 startActivity(webintent);
// 		                     }
// 		                     catch ( Exception except )
// 		                     {
//
// 		                     }
// 		                 }
// 		        	 }
// 		        }
// 		      });
	    	      
//	    Button business_idea_app = (Button)findViewById(R.id.business_idea_app);
//	    business_idea_app.setOnClickListener(new Button.OnClickListener()
//	      {
//	          public void onClick(View v)
//	          {
//	            Intent intent = new Intent(Intent.ACTION_VIEW);
//	        	intent.setData(Uri.parse("market://details?id=com.businessideas"));
//
//	        	 try
//	        	 {
//	        	        startActivity(intent);
//	        	 }
//	        	 catch (ActivityNotFoundException anfe)
//	        	 {
//	                 try
//	                 {
//	                	 Uri uri = Uri.parse("market://search?q=pname:com.businessideas");
//	                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//	                	 startActivity(next_intent);
//	                 }
//	                 catch ( Exception e)
//	                 {
//	                     // Now try to redirect them to the web version:
//	                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.businessideas");
//	                     try
//	                     {
//	                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//	                    	 startActivity(webintent);
//	                     }
//	                     catch ( Exception except )
//	                     {
//
//	                     }
//	                 }
//	        	 }
//	        }
//	      });
	    
	    //Button paid_app = (Button)findViewById(R.id.paid_app);
	    
	    Button starting_mistakes = (Button)findViewById(R.id.starting_mistakes);   
	    Button psychology_of_entrepreneurship = (Button)findViewById(R.id.psychology_of_entrepreneurship);   
	    Button business_plan_examples = (Button)findViewById(R.id.business_plan_examples);
	    
	    business_plan_examples.setOnClickListener(new Button.OnClickListener()
        {
          public void onClick(View v)
          {
              Intent myIntent = new Intent(GiveBackActivity.this, PlanExamplesActivity.class);
              GiveBackActivity.this.startActivity(myIntent);
          }
        });
	    
	    psychology_of_entrepreneurship.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {	              
              Intent myIntent = new Intent(GiveBackActivity.this, PsychologyActivity.class);
              GiveBackActivity.this.startActivity(myIntent);
          }
        });	    

	    starting_mistakes.setOnClickListener(new Button.OnClickListener() 
        {  
          public void onClick(View v) 
          {	              
              Intent myIntent = new Intent(GiveBackActivity.this, TopMistakesActivity.class);
              GiveBackActivity.this.startActivity(myIntent);
          }
        });		    
	    
	    
	    
	    
//     	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	  see_ads.setOnClickListener(new Button.OnClickListener() 
//      {  
//          public void onClick(View v) 
//          {	
//          	
//          	// Here show instructions.
//		       builder.setMessage("Instructions: a screen with ads will appear. Please consider signing up for one of their interesting free offerings.  To get out of the takeover screen, just press the back button. Try it?")
//	              .setCancelable(false)
//	              .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//	                  public void onClick(DialogInterface dialog, int id) {
//	       
//	                      Intent myIntent = new Intent(GiveBackActivity.this, AdsActivity.class);
//	                      GiveBackActivity.this.startActivity(myIntent);
//	                      
//	                      sendEmail( "See ads YES" , ":)");
//	                  }
//	              })
//	              .setNegativeButton("No", new DialogInterface.OnClickListener() 
//	              {
//	                  public void onClick(DialogInterface dialog, int id) 
//	                  {
//	                       dialog.cancel();
//	                       
//		                      sendEmail( "See ads NO" , ":)");
//	                  }
//	              });
//	       AlertDialog alert = builder.create();
//	       alert.show();		       		         
//          }
//      });	 	     
	    
	    
//      paid_app.setOnClickListener(new Button.OnClickListener()
//      {
//          public void onClick(View v)
//          {
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//        	intent.setData(Uri.parse("market://details?id=business.premium"));
//
//        	 try
//        	 {
//        	        startActivity(intent);
//        	 }
//        	 catch (ActivityNotFoundException anfe)
//        	 {
//                 try
//                 {
//                	 Uri uri = Uri.parse("market://search?q=pname:business.premium");
//                	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
//                	 startActivity(next_intent);
//                 }
//                 catch ( Exception e)
//                 {
//                     // Now try to redirect them to the web version:
//                     Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=business.premium");
//                     try
//                     {
//                    	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
//                    	 startActivity(webintent);
//                     }
//                     catch ( Exception except )
//                     {
//                        // sendEmail("Give Review (2) ERROR", "From advertising page, user clicked on web version of give review and this is the exception: " + except.getMessage() );
//                     }
//                 }
//        	 }
//        }
//      });


   
	    
	    
        //final TextView explanation_two = (TextView) findViewById(R.id.explanation_two);

		
//        Button extra_help = (Button)findViewById(R.id.extra_help);           
//        extra_help.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	
//            	sendEmail("Advertising Activity -> Extra Help", "From marketing page, " +
//            			"user clicked on extra help" );   	
//                
//                Intent myIntent = new Intent(AdvertisingActivity.this, ExtraHelpActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });	 	     
//        
//        Button ask_direct_question = (Button)findViewById(R.id.ask_direct_question);          
//     
//        
//	    ask_direct_question.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(AdvertisingActivity.this, AskQuestionActivity.class);
//                AdvertisingActivity.this.startActivity(myIntent);
//            }
//        });	        		
	}

	
	
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//       if (requestCode == 1001)
//       {
//    	  if ( data != null )
//    	  {
//	          int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
//	          String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
//	          String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
//
//	          if (resultCode == RESULT_OK)
//	          {
//	             try
//	             {
//	                JSONObject jo = new JSONObject(purchaseData);
//	                //String sku = jo.getString("basicsubscription");
//	              }
//	              catch (JSONException e)
//	              {
//	                 e.printStackTrace();
//	              }
//	          }
//    	  }
//       }
//    }

    
    @Override
    public void onDestroy() 
    {
        super.onDestroy();
//        if (mServiceConn != null) 
//        {
//            unbindService(mServiceConn);
//        }   
    }	
	
	
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       // your code
    }

	private boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) 
//    {
//        getMenuInflater().inflate(R.layout.menu, menu);
//        MenuItem item = menu.findItem(R.id.menu_item_share);
//        myShareActionProvider = (ShareActionProvider)item.getActionProvider();
//        myShareActionProvider.setShareHistoryFileName(
//          ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
//        myShareActionProvider.setShareIntent(createShareIntent());
//        return true;
//    }
//    
//    private Intent createShareIntent() 
//    {
//           Intent shareIntent = new Intent(Intent.ACTION_SEND);
//           shareIntent.setType("text/plain");
//           shareIntent.putExtra(Intent.EXTRA_TEXT, 
//             "I am using mobile apps for starting a business from http://www.problemio.com");
//           return shareIntent;
//    }
//    
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) 
//    {
//        // When you want to share set the share intent.
//        myShareActionProvider.setShareIntent(shareIntent);
//    }            
}
