package com.problemio;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import utils.SendEmail;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

//import com.flurry.android.FlurryAgent;

public class NumberOfBusinessesActivity extends BaseActivity  
{
	TextView explanation = null;
	TextView answer = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
	    //FlurryAgent.onStartSession(this, "8CA5LTZ5M73EG8R35SXG");
        
        setContentView(R.layout.number_of_businesses);
        
       // sendEmail("NumOfBusinesses Loading", "Thats it :) ");   	        
           
        explanation = (TextView) findViewById(R.id.explanation);
        answer = (TextView) findViewById(R.id.answer);
	
  	  sendFeedback( );
    }
    


    public void sendFeedback( ) 
    {          
        // Now that I have these items lets put them into the database
        String[] params = new String[] 
	        		{ "https://www.problemio.com/problems/number_of_problems_mobile.php"};
	             
	        DownloadWebPageTask task = new DownloadWebPageTask();
	        task.execute(params); 	        	        
    }      
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> 
    {
		@Override
		protected String doInBackground(String... theParams) 
		{
	        String myUrl = theParams[0];	        
	        	     
	        String charset = "UTF-8";	        
	        String response = null;
	        
			try 
			{		        
		        final URL url = new URL( myUrl );
		        		        
		        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        
		        conn.setDoOutput(true); 
		        conn.setRequestMethod("GET");
		        
		        conn.setUseCaches(false);
		        conn.connect();
		        
		        final InputStream is = conn.getInputStream();
		        final byte[] buffer = new byte[8196];
		        int readCount;
		        final StringBuilder builder = new StringBuilder();
		        while ((readCount = is.read(buffer)) > -1) 
		        {
		            builder.append(new String(buffer, 0, readCount));
		        }

		        response = builder.toString();		
			} 
			catch (Exception e) 
			{
			     Toast.makeText(getApplicationContext(), "Please try again. Possible network error.", Toast.LENGTH_LONG).show();	 
			}
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) 
		{	        
	        if ( result == null )  
	        {		                        
  		        Toast.makeText(getApplicationContext(), "Error getting the businesses.  Please try to back out of this screen and try it again. Also, please make sure your Internet connection is working. If the problem persists, please let us know about it.", 
		    		  Toast.LENGTH_LONG).show();                
	        }		        
	        else
	        if ( result.equals( "error" ) )
	        {		        
                //sendEmail("Number of Businesses Activity Error", "Error from server" );
                
  		        Toast.makeText(getApplicationContext(), "Error getting the business.  Please try to back out of this screen and try it again. Also, please make sure your Internet connection is working. If the problem persists, please let us know about it.", 
		    		  Toast.LENGTH_LONG).show();                		        
	        }	        
	        else
	        {               
		        // NOW SET THE TEXTVIW
		        answer.setText("There are " + result + " businesses planned on the app so far.");
		        explanation.setText("");		        
	        }
		}    
    }    
    
    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
	public void onStop()
    {
       super.onStop();
       //FlurryAgent.onEndSession(this);
       // your code
    } 
}
