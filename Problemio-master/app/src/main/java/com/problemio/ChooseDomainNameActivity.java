package com.problemio;

import utils.SendEmail;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.flurry.android.FlurryAgent;

public class ChooseDomainNameActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.content_domain_name);
	    
	    Button give_review = (Button)findViewById(R.id.give_review);   
	    give_review.setOnClickListener(new Button.OnClickListener() 
        {  
            public void onClick(View v) 
            {
            	
                Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.setData(Uri.parse("market://details?id=com.marketing"));
            	//startActivity(intent);
            	            	 
            	 try 
            	 {
            	        startActivity(intent);
            	 } 
            	 catch (ActivityNotFoundException anfe) 
            	 {            		
                     try
                     {
                    	 Uri uri = Uri.parse("market://search?q=pname:com.marketing");
                    	 Intent next_intent = new Intent(Intent.ACTION_VIEW, uri);
                    	 startActivity(next_intent);  
                     }
                     catch ( Exception e)
                     {
                         // Now try to redirect them to the web version:
                         Uri weburi = Uri.parse("https://play.google.com/store/apps/details?id=com.marketing");
                         try
                         {
                        	 Intent webintent = new Intent(Intent.ACTION_VIEW, weburi);
                        	 startActivity(webintent);
                         }
                         catch ( Exception except )
                         {

                         }
                     }
            	 }           	            	
            }
        });

	    
	    
//      Button godaddy = (Button)findViewById(R.id.godaddy);          
//      godaddy.setOnClickListener(new Button.OnClickListener() 
//      {  
//          public void onClick(View v) 
//          {	            	
//               //sendEmail("Problemio -> GoDaddy Check domain", "" );   	
//        	
//	            Intent browserIntent = new Intent(Intent.ACTION_VIEW, 
//	          		  Uri.parse("http://www.dpbolvw.net/click-7252177-11168704"));
//	            startActivity(browserIntent);
//          }
//      });
	    
	    
//        Button support_app = (Button)findViewById(R.id.support_app);          
//        support_app.setOnClickListener(new Button.OnClickListener() 
//        {  
//            public void onClick(View v) 
//            {	            	
//                Intent myIntent = new Intent(ChooseDomainNameActivity.this, SupportAppActivity.class);
//                ChooseDomainNameActivity.this.startActivity(myIntent);
//            }
//        });	 
	    
	    
	    // http://www.jdoqocy.com/click-7252177-10985823
	    
	}

    // Subject , body
//    public void sendEmail( String subject , String body )
//    {
//        String[] params = new String[] { "http://www.problemio.com/problems/send_email_mobile.php", subject, body };
//
//        SendEmail task = new SendEmail();
//        task.execute(params);
//    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    	//FlurryAgent.onStartSession(this, "4VYNFK3V6RCZ53CZ3J32");
    }
     
    @Override
    protected void onStop()
    {
    	super.onStop();		
    	//FlurryAgent.onEndSession(this);
    }    
}
