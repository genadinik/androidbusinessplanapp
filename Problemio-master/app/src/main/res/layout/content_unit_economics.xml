<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="fill_parent"
        android:layout_height="fill_parent"
        >
	<ScrollView
	        android:layout_width="fill_parent"
	        android:layout_height="fill_parent"
	        >
	    <LinearLayout
            android:layout_width="fill_parent"
            android:layout_height="fill_parent"
            android:orientation="vertical">
	    
<include android:id="@+id/header"
         layout="@layout/header"/>     

<TextView
	android:id="@+id/explanation"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text" 
    android:text="Understanding and correctly forecasting the unit economics of your business is extremely important. It is a large part of a successful business plan, and the business itself.  The term might sound complicated, but it is surprisingly simple. At least we will try to make it so with an example.  
    
\n\nIn an effort to make this material easy and fun to understand, we will actually go over the unit economics of a real business. Our example business: A single-location exercise gym. We will call it Bob's Fitness." />
    
<TextView
	android:id="@+id/explanation_heading1"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_heading"   
        android:text="1) BEFORE THE BUSINESS OPENS, WHAT ARE THE UPFRONT COSTS?"
    />
     
  
<TextView
	android:id="@+id/explanation1"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text"   
    android:text="This kind of a business is going to have some heavy costs before you start.  Here are some:
\na) Leasing the facility
\nb) Remodeling
\nc) Buying exercise equipment
\nd) Buying liability and other types of insurance
\ne) Hiring and training staff
\nf) Getting various city and state permits and licences to open the business
\ng) Legal costs that come with opening such a business 
\nh) Optionally creating a website 
\ni) Software for gym membership management and on-site computers and card scanners

\n\nThis list goes on and on and depending on your state and geographic location can cost far over $100,000 and take 3-9 months to complete." />			


<TextView
	android:id="@+id/explanation_heading2"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_heading"   
        android:text="2) AFTER THE BUSINESS OPENS, WHAT ARE THE ONGOING COSTS?"
    />

<TextView
	android:id="@+id/explanation2"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text" 
    android:text="If the fixed costs to start the business were not high enough, the ongoing costs can be just as high.  Here are some examples:
\na) Ongoing staff salary
\nb) Equipment replacement
\nc) Building maintenance
\nd) Possibly being sued and the costs for legal fees
\ne) Electric bills
\nf) Ongoing insurance payments
\ng) Ongoing facility rent payments  
\nh) Marketing costs
\ni) Add a little extra because every month some extra cost is bound to come out of the blue
\nj) Here is the sad news for many people: after all those costs, you will have to pay taxes from your revenues

\n\nThis list also goes on and on. The point of it is not to get depressed or discouraged, but to understand the difficulties and plan for them. If you plan for them, you will be more ready to meet the challenge.  Now we will get to some of the more encouraging parts of the business." />			

<TextView
	android:id="@+id/explanation_heading3"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_heading"   
    android:text="3) NOW THE UNIT ECONOMICS"
    />

<TextView
	android:id="@+id/explanation3"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text"  
    android:text="This is the moment we have been waiting for. Let's count how many customers we need to break even, then turn a profit. It is precisely here that you get to understand whether all the risk we outlined above will be worth the reward. Sometimes this is also useful to tell whether this business can never be profitable, but sometimes the financial opportunity really becomes clear right here.  Let's get right into the calculations:
    
\n\nLet's say, just as an example, that you charge each user $50/month. You also have a $50 activation fee. On average, a user remains a member for 12 months.

\n\nLet's say your costs to open were $100,000 which you need to eventually recoup, and your ongoing costs are $50,000.

\n\nTo break even on a month-to-month basis, you will need 1000 members paying $50/month. If you keep signing up new members, you will need fewer registered members, but the number of $50 transactions has to be 1000 to get to the break-even point of $50,000 per month. 

\n\nNote: You will be taxed on your revenue, so remember to adjust for taxes and tax write-offs.

\n\nNow think about how long it will take to get to a point where you are having that many regular and new customers.  Think about the first month.  Maybe you will have 200 customers. The next month you might have 300 customers.  Different businesses grow at very different rates. In the summer, people might want to exercise outside so you might actually have a decrease in customers.  The important thing here is to understand that for the first year or so, this business will very possibly lose more and more money as you improve your marketing initiatives and start to grow your customer base.

\n\nIf the entrepreneur keeps working and, the business will become mature and get to the 1000 customers per month and grow from there. Note that this is the good-case consideration.  Many gyms never get to that many members.

\n\nUnfortunately, there will be some space constraints so the single gym cannot be home to some number of members just because it is a physical facility. If it gets too crowded, and the gym will feel less like the small community place that it once was, that will turn off some members and they will switch gyms to a competitor gym a few blocks away. So there is a maximum number of members a gym can have.

\n\nAll this means that after a pretty long time, a gym can become profitable.  But you would have to put about $300,000 or more into it before it becomes profitable. And it can never become infinitely profitable because it has a space capacity so membership growth will eventually level off.  To make more money, you would have to open another gym and go through a number of the pains you went through with the first gym before it can become profitable."/>			



<TextView
	android:id="@+id/explanation_heading4"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_heading"   
        android:text="4) CONCLUSION AND SUMMING UP"
    />

<TextView
	android:id="@+id/explanation4"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text"
    android:text="Now you should see the value of thoroughly going through such unit-economics forecasting.  We just went through how much money it would cost to open and run this kind of a business. We also went over an estimate of how long it would take to break even, and what the maximum opportunity might be.
    
\n\nWhat if you wanted to open a gym, but did not go through this section thoroughly? You might be kicking yourself two years later and asking yourself why you did not thoroughly go through the early cost, revenue and profit estimates.

\n\nSo as part of your planning, we really encourage you to consider the unit economics. " />


<TextView
	android:id="@+id/explanation_heading5"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_heading"    
        android:text="5) WAS THIS USEFUL?"
    />

<TextView
	android:id="@+id/explanation5"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text"   
    android:text="If you feel this was useful, please take a second to give the app a nice testimonial so other entrepreneurs can learn this valuable material as well. " />	
	
	<Button  
	    android:id="@+id/give_review"  
	    android:layout_height="wrap_content"
	    android:layout_marginTop ="10dp"    
	    android:text="Please Consider Leaving a Testimonial" 
	    android:layout_width="fill_parent">  
	</Button> 	


<!-- 
<Button
	android:id="@+id/ask_direct_question"        
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    android:text="Ask a Business Question"
    android:layout_marginTop ="10dp"
    style="@style/home_page_buttons"         
    />
 -->
 
    <Button
	android:id="@+id/plan_business"
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    android:layout_marginTop ="10dp"    
    android:text="Plan a Business"
    style="@style/home_page_buttons" 
    />

    
    
    <!-- 
<TextView
	android:id="@+id/explanation10"    
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    style="@style/instruction_text"   
    android:text="For your convenience, if you would like to access this article online or share it with friends, it is available on our website. Please share the article with friends as it helps us spread the word about our apps.  " />	    
    
    <Button
	android:id="@+id/share_button"
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    android:layout_marginTop ="10dp"    
    android:text="View And Share On The Web"
    style="@style/home_page_buttons" 
    /> 
 -->
    
</LinearLayout>
    </ScrollView>
</RelativeLayout>
    